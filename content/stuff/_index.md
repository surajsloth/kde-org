---
title: "Miscellaneous KDE Resources"
description: "Miscellaneous KDE resources"
---

- [Books on KDE](https://community.kde.org/Books) You'd like to learn more about KDE, a specific application, or how to develop an application for this project? Then this page will lead you to the best books about those topics.

- [KDE Clipart](/stuff/clipart) Here you can find an almost complete list of major artwork used in the KDE project, from the official KDE and Plasma logo to the KDE mascots.</div>

- [KDE MetaStore](/stuff/metastore) Here you can find links to various stores that provide products and merchandising related to the KDE project.

- [KDE Code of Conduct](/code-of-conduct/) Principles for interacting within our community.

- [Thank Yous](/thanks) Those who have helped KDE.
