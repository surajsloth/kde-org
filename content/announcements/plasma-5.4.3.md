---
title: KDE Ships Plasma 5.4.3, bugfix Release for November
description: KDE Ships Plasma 5.4.3
date: 2015-11-10
release: plasma-5.4.3
layout: plasma
changelog: plasma-5.4.2-5.4.3-changelog
---

{{<figure src="/announcements/plasma-5.4/plasma-screen-desktop-2-shadow.png" alt="Plasma 5.4" class="text-center" width="600px" caption="Plasma 5.4">}}

Tuesday, 10 November 2015.

Today KDE releases a bugfix update to Plasma 5, versioned 5.4.3. <a href='https://www.kde.org/announcements/plasma-5.4.0.php'>Plasma 5.4</a> was released in August with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Update the KSplash background to the 5.4 wallpaper. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=467d997d8ad534d42b779719cec03a8cbfb66162">Commit.</a>
- Muon fixes PackageKit details display. <a href="http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=f110bb31d0599fda5478d035bdaf5ce325419ca6">Commit.</a>
- Fix crash when exiting kscreen kcm in systemsettings. <a href="http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=4653c287f844f2cb19379ff001ca76d7d9e3a2a1">Commit.</a> Fixes bug <a href="https://bugs.kde.org/344651">#344651</a>. Code review <a href="https://git.reviewboard.kde.org/r/125734">#125734</a>
- <a href="http://blog.martin-graesslin.com/blog/2015/10/looking-at-some-crashers-fixed-this-week/">Several crashes fixed in KWin</a>
