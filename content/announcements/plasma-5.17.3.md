---
title: KDE Plasma 5.17.3, bugfix Release for November
release: 'plasma-5.17.3'
version: "5.17.3"
description: KDE Ships Plasma 5.17.3
date: 2019-11-12
layout: plasma
changelog: plasma-5.17.2-5.17.3-changelog
---

{{< peertube "https://peertube.mastodon.host/videos/embed/5a315252-2790-42b4-8177-94680a1c78fc" >}}

{{<figure src="/announcements/plasma-5.17/plasma-5.17.png" alt="Plasma 5.17" class="text-center" width="600px" caption="KDE Plasma 5.17">}}

Tuesday, 12 November 2019.

{{% i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.17.3." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in October 2019 with many feature refinements and new modules to complete the desktop experience." "5.17" %}}

This release adds a fortnight's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix binding loop in lockscreen media controls. <a href="https://commits.kde.org/plasma-workspace/419b9708ee9e36c4d5825ff6f58ca71f61d32d83">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413087">#413087</a>. Phabricator Code review <a href="https://phabricator.kde.org/D25252">D25252</a>
- [GTK3/Firefox] Fix scrollbar click region. <a href="https://commits.kde.org/breeze-gtk/be9775281fa9018e69588174856da34c96fab82e">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413118">#413118</a>. Phabricator Code review <a href="https://phabricator.kde.org/D25180">D25180</a>
- [effects/startupfeedback] Fallback to small icon size when no cursor size is configured. <a href="https://commits.kde.org/kwin/87f36f53b3e64012bce2edc59f553341fc04cfc2">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413605">#413605</a>. Phabricator Code review <a href="https://phabricator.kde.org/D25065">D25065</a>
