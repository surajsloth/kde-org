---
title: KDE Plasma 5.10.3, Bugfix Release for June
release: plasma-5.10.3
version: 5.10.3
description: KDE Ships 5.10.3
date: 2017-06-27
layout: plasma
changelog: plasma-5.10.2-5.10.3-changelog
---

{{%youtube id="VtdTC2Mh070"%}}

{{<figure src="/announcements/plasma-5.10/plasma-5.10.png" alt="KDE Plasma 5.10 " class="text-center" width="600px" caption="KDE Plasma 5.10">}}

Tuesday, 27 June 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.10.3." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.10" "May" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "two week's" %}}

- Fix KWin draws 1px overlay on the left screen border: properly block the edge also for touch screen edges. <a href="https://commits.kde.org/kwin/6267d597311ccea26a8e70d57bd730ad13d146c2">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380476">#380476</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6304">D6304</a>

- Fix Buffer objects (VBO, FBO) need remapping after suspend/vt switch with NVIDIA. [platforms/x11] Add support for GLX_NV_robustness_video_memory_purge. <a href="https://commits.kde.org/kwin/97fa72ee48b7525e722822e7d7d41bb08343e337">Commit.</a> Fixes bug <a href="https://bugs.kde.org/344326">#344326</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6344">D6344</a>

- Make shadows work for windows 100% width or height. <a href="https://commits.kde.org/kwin/b7cb301deb3b191c7ff0bd04d87d6c1b93d90407">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380825">#380825</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6164">D6164</a>

- Introduce KDE_NO_GLOBAL_MENU env variable to disable global menu per-app. <a href="https://commits.kde.org/plasma-integration/1ba4bca8342ac3d55bf29bdd8f622cd304e11816">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D6310">D6310</a>

- Workaround Qt regression of no longer delivering events for the root window. <a href="https://commits.kde.org/kwin/a6dee74ee455d1da47dd5c9d55a84adbb5e1426a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/360841">#360841</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6258">D6258</a>

- Fix can't control brightness. Revert 'skip the disabled backlight device'. <a href="https://commits.kde.org/powerdevil/5c57cf64b5e5c880b1a5f3a0177293f6958e1b9a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/381114">#381114</a>. Fixes bug <a href="https://bugs.kde.org/381199">#381199</a>
