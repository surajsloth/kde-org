---
title: KDE 4.0 Released
date: "2008-01-11"
layout: single
---

<h3 align="center">
   Projekt KDE dostarcza czwartą wersję przełomowego środowiska graficznego
</h3>
<p align="justify">
  <strong>
    Wraz z czwartą główną wersją społeczność KDE rozpoczyna nową erę KDE 4.
  </strong>
</p>

<p>
Projekt KDE z prawdziwą radością ogłasza dostępność nowej wersji KDE 4.0.0. Oznacza ona pomyślne zakończenie długiego okresu intensywnych prac nad <a href="http://www.kde.org/announcements/4.0/">KDE 4.0</a>, a tym samem początek nowej ery KDE 4.
</p>

<div class="text-center">
<a href="/announcements/announce-4.0/desktop.png">
<img src="/announcements/announce-4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>Pulpit KDE 4.0</em>
</div>
<br/>


<p><b>Biblioteki programistyczne</b> KDE 4 zostały usprawnione pod niemal każdym względem. Biblioteka do obsługi multimediów <i>Phonon</i>, dostarcza ich niezależną od platformy obsługę wszystkim aplikacjom KDE, a biblioteka <i>Solid</i> znacznie ułatwia obsługę urządzeń przenośnych oraz zapewnia lepsze zarządzanie energią.
</p>
<p><b>Pulpit</b> KDE wzbogacił się o nowe możliwości. Plasma prezentuje nowy interfejs pulpitu zawierający panel, menu oraz widżety pulpitu. Menedżer okien KDE, KWin obsługuje teraz zaawansowane efekty graficzne usprawniające pracę z oknami.
</p>
<p>Zostało usprawnionych również wiele <b>aplikacji</b> KDE. Obsługa grafiki wektorowej, ulepszenia bibliotek graficznych oraz interfejsów użytkownika, nowe funkcje, a nawet nowe aplikacje -- co byśmy nie wymienili w tym zakresie, KDE 4.0 już to wszystko ma. Nowa przeglądarka dokumentów Okular i nowy menedżer plików Dolphin to tylko dwa przykłady aplikacji wykorzystujących nowe techniki dostępne w KDE 4.0.
</p>
<p><img src="/announcements/announce-4.0/images/oxybann.png" align="right" hspace="10"/>Graficy z grupy Oxygen wnieśli dużo świeżości do <b>wyglądu</b> środowiska. Prawie wszystkie widoczne elementy KDE i jego aplikacji posiadają nową grafikę. Piękno i przejrzystość to podstawowe cechy Oxygen.
</p>

<h3>Pulpit</h3>
<ul>
	<li>Nowa powłoka pulpitu, Plasma, dostarcza niezbędne elementy pozwalające na pracę z pulpitem i aplikacjami.</li>
	<li>KWin, sprawdzony menedżer okien KDE, udostępnia teraz także zaawansowane efekty graficzne. Wykorzystując akcelerację sprzętową, pozwala na płynniejszą i bardziej intuicyjną pracę z oknami.</li>
	<li>Oxygen odpowiedzialny za motyw graficzny KDE 4.0 zapewnia spójny i przyjemny dla oka wygląd.</li>
</ul>

Więcej na temat nowego interfejsu KDE możesz dowiedzieć się a <a href="./guide">Wizualnego Przewodnika po KDE 4.0</a>.

<h3>Aplikacje</h3>
<ul>
	<li>Konqueror to sprawdzona przeglądarka internetowa KDE. Jest lekka, dobrze zintegrowana z systemem oraz obsługuje najnowsze standardy takie jak CSS 3.
</li>
	<li>Dolphin - nowy menedżer plików KDE. Aplikacja była przygotowywana z myślą o użyteczności, jest prosta w użyciu, a jednocześnie stanowi potężne narzędzie.
</li>
	<li>Ustawienia systemowe posiadają teraz nowy interfejs. Monitor systemowy KSysGuard pozwala na łatwe obserwowanie zasobów systemu oraz na ich kontrolę.
</li>
	<li>Okular to przeglądarka dokumentów, która obsługuje dużą ilość formatów dokumentów. Podobnie jak wiele aplikacji KDE 4, została ona usprawniona przy współpracy z projektem <a href="http://openusability.org/" class="external text" title="http://openusability.org/">OpenUsability</a>.
</li>
	<li>Aplikacje edukacyjne to jedne z pierwszych programów, które zostały przeniesione na platformę KDE 4. Kalzium, który jest graficzną tabelą okresowego układu pierwiastków oraz mapa geograficzna świata Marble to tylko dwa przykłady spośród wielu aplikacji edukacyjnych.
</li>
	<li>Wiele gier dostępnych z KDE zostało odświeżonych. Dzięki możliwościom jakie daje grafika wektorowa, gry wyglądają teraz ładnie niezależnie od rozmiaru ekranu.
</li>
</ul>

Niektóre aplikacje zostały bardziej szczegółowo opisane na <a href="./applications">Wizualnym Przewodniku po KDE 4.0</a>.

<div class="text-center">
<a href="/announcements/announce-4.0/dolphin-systemsettings-kickoff.png">
<img src="/announcements/announce-4.0/dolphin-systemsettings-kickoff_thumb.png" class="img-fluid">
</a> <br/>
<em>Menedżer plików, Ustawienia systemowe oraz Menu w akcji</em>
</div>
<br/>


<h3>Biblioteki programistyczne</h3>
<p>
<ul>
	<li>Phonon oferuje funkcje multimedialne takie jak odtwarzanie muzyki i plików wideo. Obsługuje różne silniki, które można zmieniać "w locie". Podstawowym silnikiem multimedialnym w KDE 4.0 będzie Xine, oferujący potężny zakres obsługiwanych formatów. Biblioteka Phonon pozwala również użytkownikowi wybrać urządzenia wyjściowe zależnie od rodzaju odtwarzanych multimediów. 
</li>
	<li>Biblioteka Solid pozwala na łatwą obsługę urządzeń przenośnych oraz tych zamontowanych na stałe w komputerze. Solid współgra też z systemem do zarządzania energią, obsługuje połączenia sieciowe i urządzenia Bluetooth. Biblioteka działa w oparciu o HAL <span title="Po angielsku: Hardware Abstraction Layer" style="cursor:help;font-style:italic;border:lowercase;text-transform:none;"><abbr title="Po angielsku: Hardware Abstraction Layer" style="border:none;color:#606060;">(ang. Hardware Abstraction Layer)</abbr></span>, NetworkManager oraz Bluez bluetooth. Komponenty te można zastępować innymi nie tracąc przy dostępu do aplikacji.
</li>
	<li>KHTML to silnik renderujący strony internetowe używany przez Konquerora - przeglądarkę KDE. KHTML jest bardzo lekki i obsługuje najnowsze standardy, takie jak CSS 3. Był to również pierwszy silnik, który przechodził słynny test zgodności ze standardami Acid 2.
</li>
	<li>Biblioteka ThreadWeaver wchodząca w skład głównych bibliotek KDE, zapewnia wsparcie dla współczesnych procesorów wielordzeniowych. Wykorzystując efektywniej zasoby systemu, czyni aplikacje KDE płynniejsze w działaniu.
</li>
	<li>KDE 4.0 zostało napisane w oparciu o bibliotekę Qt 4 firmy Trolltech. Pozwala to na wykorzystanie zaawansowanych możliwości graficznych przy zmniejszonym zużyciu pamięci. Główne biblioteki programistyczne <span title="Po angielsku: kdelibs" style="cursor:help;font-style:italic;border:lowercase;text-transform:none;"><abbr title="Po angielsku: kdelibs" style="border:none;color:#606060;">(ang. kdelibs)</abbr></span> stanowią wspaniałe rozszerzenie biblioteki Qt, dodając nowe funkcje i udogodnienia dla programistów.
</li>
</ul>
</p>
<p>Szczegółowe informacje na temat bibliotek programistycznych zawiera baza wiedzy KDE <a href="http://techbase.kde.org">TechBase</a>.</p>

<h4>Take a guided tour...</h4>
<p>
The <a href="./guide">KDE 4.0 Visual Guide</a> provides a quick overview of various new
and improved KDE 4.0 technologies. Illustrated with many screenshots, it walks you
through the different parts of KDE 4.0 and shows some of the exciting new technologies and
improvements for the user. New features of the <a href="./desktop">desktop</a> get
you started, <a href="./applications">applications</a> such as System Settings, Okular the
document viewer and Dolphin the filemanager are introduced. 
<a href="./education">Educational applications</a> are shown as well as 
<a href="./games">Games</a>.
</p>


<h4>Wypróbuj najnowsze KDE...</h4>
<p>
Aktualizowana lista znajduje się na 
<a href="http://www.kde.org/info/4.0">stronie informacyjnej KDE 4.0</a>, gdzie możesz znaleźć odnośniki do kodu źródłowego, informacji o kompilacji, bezpieczeństwie i inne.
</p>
<p>
Następujące dystrybucje poinformowały nas o dostępności pakietów lub Live CD z KDE 4.0:

<ul><li>Wersja alpha <b>Arklinux 2008.1</b> opartego na KDE 4 powinna się pojawić krótko po wydaniu KDE 4.0, a pełna wersja systemu 3-4 tygodnie później.
</li><li>Pakiety KDE 4.0 dla <b>Debiana</b> dostępne są w gałęzi eksperymentalnej. Platforma programistyczna KDE będzie dostępna w Lenny'm. Czekamy na ogłoszenie <a href="http://pkg-kde.alioth.debian.org/" class="external text" title="http://pkg-kde.alioth.debian.org/">grupy Debian KDE</a>. Plotki głoszą, że planowana jest również wersja Live CD.

</li><li><b>Fedora</b> udostępni KDE 4.0 w 9 wersji swojego systemu, która <a href="http://fedoraproject.org/wiki/Releases/9" class="external text" title="http://fedoraproject.org/wiki/Releases/9">powinna się ukazać</a> w kwietniu. Wersja alfa będzie dostępna od 24 stycznia. Pakiety KDE 4.0 są dostępne w respozytorium <a href="http://fedoraproject.org/wiki/Releases/Rawhide" class="external text" title="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> pre-alpha.
</li><li><b>Gentoo Linux</b> udostępni ebuildy w swoim drzewie portage
</li><li>Pakiety <b>Kubuntu</b> i Ubuntu są dostępne w nadchodzącej wersji "Hardy Heron" (8.04), jak również będą dostępne jako aktualizacja w stabilnym "Gutsy Gibbon" (7.10). Dostępne jest również Live CD z KDE 4.0. Więcej informacji można znaleźć w <a href="http://kubuntu.org/announcements/kde-4.0" class="external text" title="http://kubuntu.org/announcements/kde-4.0">ogłoszeniu na stronach kubuntu.org</a>.

</li><li><b>Mandriva</b> udostępni pakiety w wersji 2008.0 i planuje wypuścić Live CD z najnowszą dostępną wersją 2008.1.
</li><li>Pakiety <b>openSUSE</b> są <a href="http://en.opensuse.org/KDE4" class="external text" title="http://en.opensuse.org/KDE4">dostępne</a> dla openSUSE 10.3 (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp" class="external text" title="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">zainstaluj jednym kliknięciem</a>) i openSUSE 10.2. Dostępne jest również <a href="http://home.kde.org/~binner/kde-four-live/" class="external text" title="http://home.kde.org/~binner/kde-four-live/">KDE Four Live CD</a> z tymi pakietami. KDE 4.0 będzie częścią nadchodzącego wydania openSUSE 11.0.
</li></ul>

</p>

<h2>Informacje o KDE 4</h2>
<p>
KDE 4.0 to innowacyjne środowisko graficzne będące Wolnym Oprogramowaniem. Zawiera dużą liczbą aplikacji, przeznaczonych do codziennego typowego jak i specjalistycznego użytku. Plasma, nowa powłoka przygotowana dla KDE 4, w celu ułatwiania pracy z pulpitem i aplikacjami udostępnia intuicyjny interfejs graficzny. Przeglądarka Konqueror integruje pulpit z funkcjami internetowymi. Menedżer plików Dolphin, przeglądarka dokumentów Okular oraz Centrum Sterowania dopełniają podstawowy zestaw podręcznych narzędzi. 
<br />
KDE zostało zbudowane przy pomocy bibliotek programistycznych, w przystępny sposób udostępniających zasoby sieciowe poprzez rozszerzenie KIO oraz zaawansowane możliwości graficzne dzięki Qt 4. Inne biblioteki KDE, takie jak Phonon i Solid, wnoszą obsługę multimediów oraz usprawniają współpracę z aplikacji KDE z poszczególnymi urządzeniami komputera.
</p>

<h2>Informacje o KDE</h2>
<p>
KDE to międzynarodowy zespół techniczny tworzący zintegrowane środowisko graficzne będące Wolnym/Otwartym oprogramowaniem dla komputerów stacjonarnych oraz urządzeń przenośnych. Wśród produktów KDE znajduje się system graficzny dla platform klasy Linux i UNIX, wszechstronny pakiet biurowy oraz pakiet do pracy grupowej, setki aplikacji należących do wielu kategorii, włącznie programami do obsługi Internetu, multimedialnych, rozrywkowych, edukacyjnych, graficznych oraz programistycznych. Wysoce funkcjonalne aplikacje KDE 4, zbudowane na bazie bibliotek Qt® firmy Trolltech®, zapewniają efektywne działanie na platformach Linux, BSD, Solaris, Windows i Mac OS X.
</p>

<hr />
<p><i>Informacje o znakach towarowych.</i> KDE® i logo K Desktop Environment® są zarejestrowanymi znakami towarowymi KDE e.V. (szczegóły). Linux® jest zarejestrowanym znakiem towarowym Linusa Torvaldsa. UNIX jest zarejestrowanym znakiem towarowym Open Group w USA i innych krajach. Wszystkie inne znaki towarowe i prawa autorskie są własnością ich prawowitych właścicieli. 
</p>
<hr />

<h4>Kontakt dla prasy</h4>
<p>
<b>Europa</b><br />
Sebastian Kügler<br />
Wolfstraat 154<br />
6531 LR Nijmegen<br />
Holandia<br />
tel.: +31-6-48370928<br />
info-europe@kde.org<br />
</p>

