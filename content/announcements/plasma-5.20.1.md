---
title: "KDE Plasma 5.20.1, bugfix Release for October"
version: 5.20.1
layout: plasma
changelog: plasma-5.20.0-5.20.1-changelog
---

{{% plasma-5-20-video %}}

Tuesday, 20 October 2020.

{{% i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.20.1." %}}

[Plasma 5.20](/announcements/plasma-5.20.0) was released in October 2020 with many feature refinements and new modules to complete the desktop experience.

+ KSysGuard: Divide network speeds by 2 to match reality. <a href='https://commits.kde.org/ksysguard/33694eafa0178ecb1e33d7d26bb15dc97defbf3c'>Commit.</a>
+ Powerdevil: Ignore players from KDE Connect when suspending. <a href='https://commits.kde.org/powerdevil/4ac78e7118238414d3f2d603c21975413eea8bb0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427209'>#427209</a>
+ Bluedevil kcm: Set sane default size. <a href='https://commits.kde.org/bluedevil/fdc622af5f49b6ccfc83641c9ce8e8d77a99e82a'>Commit.</a>
