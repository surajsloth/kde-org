---
title: KDE Plasma 5.14.3, Bugfix Release for November
release: plasma-5.14.3
version: 5.14.3
description: KDE Ships 5.14.3
date: 2018-11-06
changelog: plasma-5.14.2-5.14.3-changelog
---

{{<figure src="/announcements/plasma-5.14/plasma-5.14.png" alt="Plasma 5.14" class="text-center" width="600px" caption="KDE Plasma 5.14">}}

Tuesday, 6 November 2018.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.14.3." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.14" "October" %}}

{{% i18n_var "This release adds %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "two weeks" %}}

- Fix custom window rules being applied in the window manager
- Dynamically switching between trash and delete in the plasma desktop context menu
- More visible icons for network manager and volume control in the system tray
