---
title: KDE Ships KDE Applications 15.12.3
description: KDE Ships KDE Applications 15.12.3
date: 2016-03-16
version: 15.12.3
layout: application
changelog: fulllog_applications-15.12.3
---

{{% i18n_var "March 15, 2016. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 15.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-15.12.0" %}}

More than 15 recorded bugfixes include improvements to kdepim, akonadi, ark, kblocks, kcalc, ktouch and umbrello, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.18" %}}
