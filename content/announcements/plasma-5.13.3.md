---
title: KDE Plasma 5.13.3, Bugfix Release for July
release: plasma-5.13.3
version: 5.13.3
description: KDE Ships 5.13.3
date: 2018-07-10
changelog: plasma-5.13.2-5.13.3-changelog
---

{{%youtube id="C2kR1_n_d-g"%}}

{{<figure src="/announcements/plasma-5.13/plasma-5.13.png" alt="Plasma 5.13" class="text-center" width="600px" caption="KDE Plasma 5.13">}}

Tuesday, 10 July 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.13.3." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.13" "June" %}}

{{% i18n_var "This release adds %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "2 week's" %}}

- Fix for QtCurve settings crash when using global menu
- Fix places runner to open search, timeline and devices properly
- Fixes for stability and usability in Plasma Discover
- Correct Folder View sizing and representation switch behavior
