---
title: Plasma 5.10.4 Complete Changelog
version: 5.10.4
hidden: true
plasma: true
type: fulllog
---

### <a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a>

- Applet: Rename MediaPlayer to MediaPlayerItem. <a href='https://commits.kde.org/bluedevil/94ff9f296b6e9105e2deb07abaca3fc4ecd2f8cd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6487'>D6487</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Fix launching of resources for PackageKit and Flatpak. <a href='https://commits.kde.org/discover/33805b2deda1401b5cbf7a45b79d03c58a1c687d'>Commit.</a>
- Make the flatpak backend more flexible to issues. <a href='https://commits.kde.org/discover/e062f0e946489d8c10c656379b229de1c117e21b'>Commit.</a>
- KNS: Don't connect the stream until the search is in place. <a href='https://commits.kde.org/discover/0cd0b6223171e3856ef5f5d66b7f1a2e6f9e8f30'>Commit.</a>
- Improve logic for fetching installed. <a href='https://commits.kde.org/discover/fcd53ef5e2a4cd8b90d1c2525d0ba05d40a577e1'>Commit.</a>
- Don't continue fetching when looking for installed resources. <a href='https://commits.kde.org/discover/61323583ff620506ef8c725d947caefb66c0964c'>Commit.</a>
- Stop keeping track of the remaining time. <a href='https://commits.kde.org/discover/12146bc5d6b5955efe073396ef181d94ad2884b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381979'>#381979</a>
- Don't search on KNS when in a category the backend doesn't belong to. <a href='https://commits.kde.org/discover/4991511d77d5bfda0097fe730bfa656b1ab37bb1'>Commit.</a>
- Slightly cleaner code. <a href='https://commits.kde.org/discover/9c06d81713320dc42ff81545d90a2b0d1aa9afec'>Commit.</a>
- Make sure the osName isn't "quoted". <a href='https://commits.kde.org/discover/55ab4adf8706b1a060ad7af87a4203d6f8525efa'>Commit.</a>
- Allow opening links in the update changelog. <a href='https://commits.kde.org/discover/c2f83caea58df8a33cd595a498ede06f6e7811b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381768'>#381768</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Fix check for missing engine in comic applet. <a href='https://commits.kde.org/kdeplasma-addons/8aa47be2abe657fb0c9c65172a23a9593e7f1a11'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372313'>#372313</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6746'>D6746</a>
- [Color Picker] Fix picking colors in multi-screen. <a href='https://commits.kde.org/kdeplasma-addons/0ed491a1b0f1013bfd239ba1dc41210144c23e25'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382261'>#382261</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6639'>D6639</a>
- Replace XML parsing in National Geographic dataengine. <a href='https://commits.kde.org/kdeplasma-addons/4ed05974c6a150677ad3a91578879092486124e2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379003'>#379003</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5729'>D5729</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [aurorae] Add support for shaded windows. <a href='https://commits.kde.org/kwin/bf0e0f071cebf9238669ce4352f29cc3f061edcc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373319'>#373319</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6719'>D6719</a>
- Properly swap the quick tile side when pressing the shortcut again. <a href='https://commits.kde.org/kwin/139b4dc82a9423e42285a469ab7f7cc4139d7054'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382313'>#382313</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6708'>D6708</a>
- [aurorae] Mark the render QQuickWindow as frameless. <a href='https://commits.kde.org/kwin/5cb91762be43b4303a1864accc545a6fe26aa9ed'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6726'>D6726</a>
- [platforms/x11] Quit the OpenGL Freeze protection thread on shutdown. <a href='https://commits.kde.org/kwin/06a558e3de658f300b295beac7c4adc4f08227f5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382283'>#382283</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6735'>D6735</a>
- [qpa] Prevent crash due to Surface getting null. <a href='https://commits.kde.org/kwin/962a2e39ee7a476e416f067dd31f10e3a4a63784'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382063'>#382063</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6533'>D6533</a>
- [platforms/x11] Fix incorrect screen edge approaching with switch desktop on window move. <a href='https://commits.kde.org/kwin/4e9456a857234fe7884cae6aa1cec93093fdf525'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381849'>#381849</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6467'>D6467</a>
- [effects/slideback] Ignore windows which are not in visible area. <a href='https://commits.kde.org/kwin/24ff93854ddc78990bf111e7bab4bce58a75d0bc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381402'>#381402</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6468'>D6468</a>
- Restore active client after ending showing desktop. <a href='https://commits.kde.org/kwin/113be5fac81a4b546e2a1b272451f50e777bdcb5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375993'>#375993</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6420'>D6420</a>
- Skip Shader self test for Mesa >= 17. <a href='https://commits.kde.org/kwin/43816119e9ec74df228b42163b087e51b7541884'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376801'>#376801</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6426'>D6426</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Don't define behaviors for the same properies in 2 places. <a href='https://commits.kde.org/plasma-desktop/30ebb82b11b2ec696f56ec77b59fb2b489d01b19'>Commit.</a>
- Kimpanel: use visualParent to correctly position the popup menu. <a href='https://commits.kde.org/plasma-desktop/3413d873dd85e8f751121c6b70e5b61eee208db9'>Commit.</a>
- [Task Manager] Fix icon size in launcher tooltips. <a href='https://commits.kde.org/plasma-desktop/d28deb6f4ab2f9e03b90f8e257145ffd793cb15d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380432'>#380432</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6751'>D6751</a>
- Fix crash in KCMKeyboardWidget::populateWithCurrentXkbOptions on Wayland. <a href='https://commits.kde.org/plasma-desktop/5cd48d672782bc11b4f949a3646aa5bc4224ab85'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6581'>D6581</a>
- Backport fix from e5df3ded85c94f0a33afe12b18e6afad96f12639. <a href='https://commits.kde.org/plasma-desktop/6f3eaef11b1dad53ed8e3e1b4ab6bed13447f6e4'>Commit.</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Fix compilation with current cmake (git branch release). <a href='https://commits.kde.org/plasma-workspace/052ab380b6cb7f27da19ba0937bc2563b175a19b'>Commit.</a>
- [Notification Item] Enforce PlainText for summary. <a href='https://commits.kde.org/plasma-workspace/ddc530ff14b61f3a508d53116f2b2cd7cb585b29'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6671'>D6671</a>
- [Notifications] Check for corona to avoid crash. <a href='https://commits.kde.org/plasma-workspace/8a05294e5b3ef1df86f099edde837b8c8d28ccaf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378508'>#378508</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6653'>D6653</a>
- [Windowed Widgets Runner] Fix launching widget. <a href='https://commits.kde.org/plasma-workspace/8c5a75341849a621462c41bb685bb46dfef129e1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6602'>D6602</a>
- [lookandfeel/lockscreen] Fix state handling when clicking the keyboard's own hide button. <a href='https://commits.kde.org/plasma-workspace/5004afe62d165af36d1fc97b138432fb0d6893a4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381833'>#381833</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6456'>D6456</a>

### <a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a>

- Session file parser: Support sections and respect the Hidden property. <a href='https://commits.kde.org/sddm-kcm/65dc9de7c45d5ea4affaa6bf9e6601a000c3e321'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381982'>#381982</a>. Phabricator Code review <a href='https://phabricator.kde.org/D6626'>D6626</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Honour the NoDisplay attribute of KServices. <a href='https://commits.kde.org/systemsettings/85ed16cd422804971345bc492757fa0050b4b61d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6612'>D6612</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Use CMAKE_INSTALL_FULL_LIBEXECDIR. <a href='https://commits.kde.org/xdg-desktop-portal-kde/718029526e32e65ac76206117b8531146c57f616'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D6749'>D6749</a>
