---
title: KDE Plasma 5.17.1, bugfix Release for October
release: "plasma-5.17.1"
version: "5.17.1"
description: KDE Ships Plasma 5.17.1
date: 2019-10-22
layout: plasma
changelog: plasma-5.17.0-5.17.1-changelog
---

{{< peertube "https://peertube.mastodon.host/videos/embed/5a315252-2790-42b4-8177-94680a1c78fc" >}}

{{<figure src="/announcements/plasma-5.17/plasma-5.17.png" alt="Plasma 5.17" class="text-center" width="600px" caption="KDE Plasma 5.17">}}

Tuesday, 22 October 2019.

{{% i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.17.1." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in October 2019 with many feature refinements and new modules to complete the desktop experience." "5.17" %}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- [Mouse KCM] Fix acceleration profile on X11. <a href="https://commits.kde.org/plasma-desktop/829501dd777966091ddcf94e5c5247aaa78ac832">Commit.</a> Fixes bug <a href="https://bugs.kde.org/398713">#398713</a>. Phabricator Code review <a href="https://phabricator.kde.org/D24711">D24711</a>
- Fix slideshow crashing in invalidate(). <a href="https://commits.kde.org/plasma-workspace/a1cf305ffb21b8ae8bbaf4d6ce03bbaa94cff405">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413018">#413018</a>. Phabricator Code review <a href="https://phabricator.kde.org/D24723">D24723</a>
- [Media Controller] Multiple artists support. <a href="https://commits.kde.org/plasma-workspace/1be4bb880fdeea93381eb45846a7d487e58beb93">Commit.</a> Fixes bug <a href="https://bugs.kde.org/405762">#405762</a>. Phabricator Code review <a href="https://phabricator.kde.org/D24740">D24740</a>
