---
title: KDE Plasma 5.18.1, feature Release for February
release: "plasma-5.18.1"
version: "5.18.1"
description: KDE Ships Plasma 5.18.1
date: 2020-02-18
layout: plasma
changelog: plasma-5.18.0-5.18.1-changelog
---

{{< peertube "cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5" >}}

{{<figure src="/announcements/plasma-5.18/plasma-5.18.png" alt="Plasma 5.18" class="text-center" width="600px" caption="KDE Plasma 5.18">}}

Tuesday, 18 February 2020.

{{% i18n_var "Today KDE releases a feature update to KDE Plasma 5, versioned %[1]s" "5.18.1." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in February 2020 with many feature refinements and new modules to complete the desktop experience." "5.18" %}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- <a href="https://bugs.kde.org/show_bug.cgi?id=415155">Closing window from Present Windows effect sometimes causes the new frontmost window to lose focus and be unable to regain it</a>
- <a href="https://bugs.kde.org/show_bug.cgi?id=417424">On upgrade to 5.18, a desktop with locked widgets remains locked, and cannot be unlocked.</a>
- <a href="https://bugs.kde.org/show_bug.cgi?id=416358">kcm_fonts: Cannot apply changes (button remains inactive)</a>
- <a href="https://bugs.kde.org/show_bug.cgi?id=416695">Chord keyboard shortcuts that begin with Alt+D all open the desktop settings window</a>
- <a href="https://bugs.kde.org/show_bug.cgi?id=413915">All electron apps have grey menus when using breeze gtk3 theme</a>
