---
title: KDE Ships Plasma 5.4.1, bugfix Release for September
description: KDE Ships Plasma 5.4.1
date: 2015-09-08
release: plasma-5.4.1
layout: plasma
changelog: plasma-5.4.0-5.4.1-changelog
---

{{<figure src="/announcements/plasma-5.4/plasma-screen-desktop-2-shadow.png" alt="Plasma 5.4 " class="text-center" width="600px" caption="Plasma 5.4">}}

Tuesday, 08 September 2015.

Today KDE releases a bugfix update to Plasma 5, versioned 5.4.1. <a href='https://www.kde.org/announcements/plasma-5.4.php'>Plasma 5.4</a> was released in August with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fixes for compilation with GCC 5
- Autostart desktop files no longer saved to the wrong location
- On Muon Make sure the install button has a size.
