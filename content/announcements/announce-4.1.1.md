---
title: KDE 4.1.1 Release Announcement
date: "2008-09-03"
description: KDE Community Ships First Maintenance Update for KDE 4.1.
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Community Stabilizes Desktop with KDE 4.1.1
</h3>

<p align="justify">
  <strong>
KDE Community Ships First Translation and Service Release of the 4.1
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and
Translation Updates
</strong>
</p>

<p align="justify">
The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of KDE 4.1.1, the first
bugfix and maintenance update for the latest generation of the most advanced and powerful
free desktop. KDE 4.1.1 is a monthly update to <a href="4.1/">KDE 4.1</a>. It
ships with a basic desktop and many other packages; like administration programs, network tools,
educational applications, utilities, multimedia software, games, artwork,
web development tools and more. KDE's award-winning tools and applications are
available in more than 50 languages.
</p>

<div class="text-center">
	<a href="/announcements/announce-4.1/desktop.png">
	<img src="/announcements/announce-4.1/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The KDE 4.1 desktop</em>
</div>
<br/>

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.1.1/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  <a id="changes">Enhancements</a>
</h4>
<p align="justify">
Pretty much all applications have received the developers' attention,
resulting in a <a href="http://www.kde.org/announcements/changelogs/changelog4_1to4_1_1">
long list of bugfixes and improvements</a>. The most significant changes are:

<ul>
	<li>Significant performance, interaction and rendering correctness improvements
	in KHTML and Konqueror, KDE's webbrowser </li>
	<li>User interaction, rendering and stability fixes in Plasma, the KDE4 desktop shell </li>
	<li>PDF backend fixes in the document viewer Okular </li>
	<li>Fixes in Gwenview, the image viewer's thumbnailing, more robust retrieval and
	display of images with broken metadata </li>
	<li>Stability and interaction fixes in KMail </li>
</ul>

To find out more about KDE 4.1, please refer to the
<a href="http://www.kde.org/announcements/4.1/">KDE 4.1.0</a> and
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0.0</a> release
notes. KDE 4.1.1 is a recommended update for everyone running KDE 4.1.0.
It will be followed up by more x.y.z updates over the next months and
ultimately by a new feature release, KDE 4.2.0 this coming January.

<p />

<h4>Extragear</h4>
<p align="justify">
Since KDE 4.0.0, <a href="http://extragear.kde.org">Extragear</a> applications
are also part of regular KDE releases.
Extragear applications are KDE applications that are mature, but not part
of one of the other KDE packages.
</p>

<h4>
  Installing KDE 4.1.1 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.1.1
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.1.1/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.1.1">KDE 4.1.1 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.1.1
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.1.1 may be <a
href="http://download.kde.org/stable/4.1.1/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.1.1
  are available from the <a href="/info/4.1.1#binary">KDE 4.1.1 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
