---
title: KDE Plasma 5.8.3, Bugfix Release for November
release: plasma-5.8.3
description: KDE Ships Plasma 5.8.3.
date: 2016-11-01
layout: plasma
changelog: plasma-5.8.2-5.8.3-changelog
---

{{% youtube id="LgH1Clgr-uE" %}}

{{<figure src="/announcements/plasma-5.8/plasma-5.8.png" alt="KDE Plasma 5.8 " class="text-center" width="600px" caption="KDE Plasma 5.8">}}

Tuesday, 1 November 2016.

Today KDE releases a Bugfix update to KDE Plasma 5, versioned 5.8.3. <a href='https://www.kde.org/announcements/plasma-5.8.0.php'>Plasma 5.8</a> was released in October with many feature refinements and new modules to complete the desktop experience.

This release adds two week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- User Manager: do not ask for root permissions when it's unnecessary. <a href="http://quickgit.kde.org/?p=user-manager.git&amp;a=commit&amp;h=a666712102be7ef4dd48202cc2411921fc4d392b">Commit.</a>
- PowerDevil no longer crashes on logout. <a href="http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=70177b065389db8cc822dbe88b3cdd383cd1d4cc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/371127">#371127</a>
- Mute volume when decreasing volume to zero. <a href="http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=6366791aaa5077e2c553b25c5d10c6029412a95c">Commit.</a>
