---
title: KDE Plasma 5.15.2, Bugfix Release for February
release: 'plasma-5.15.2'
version: "5.15.2"
description: KDE Ships Plasma 5.15.1.
date: 2019-02-26
changelog: plasma-5.15.1-5.15.2-changelog
---

{{%youtube id="C2kR1_n_d-g"%}}

{{<figure src="/announcements/plasma-5.15/plasma-5.15-apps.png" alt="Plasma 5.15" class="text-center" width="600px" caption="KDE Plasma 5.15">}}

Tuesday, 26 February 2019.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.15.2." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in February with many feature refinements and new modules to complete the desktop experience." "5.15" %}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- The 'Module Help' button gets enabled when help is available. <a href="https://commits.kde.org/kinfocenter/585b92af8f0e415ffb8c2cb6a4712a8fe01fbbc4">Commit.</a> Fixes bug <a href="https://bugs.kde.org/392597">#392597</a>. Phabricator Code review <a href="https://phabricator.kde.org/D19187">D19187</a>
- [about-distro] let distributions choose VERSION_ID or VERSION. <a href="https://commits.kde.org/kinfocenter/99f10f1e5580f373600478746016c796ac55e3f9">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D19226">D19226</a>
- xdg-desktop-portal-kde: Fix selection of multiple files. <a href="https://commits.kde.org/xdg-desktop-portal-kde/8ef6eb3f7a950327262881a5f3b21fac1d3064c6">Commit.</a> Fixes bug <a href="https://bugs.kde.org/404739">#404739</a>
