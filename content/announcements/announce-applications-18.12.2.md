---  
title: KDE Ships KDE Applications 18.12.2
major_version: "18.12"
version: '18.12.2'
release: applications-18.12.2
description: KDE Ships Applications 18.12.2.
date: 2019-02-07
changelog: fulllog_applications-18.12.2
layout: application
---

February 7, 2019.

{{% i18n_var "Today KDE released the second stability update for <a href='%[1]s'>KDE Applications %[2]s</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-18.12.0" %}}" "18.12" %}}

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Konsole, Lokalize, Umbrello, among others.

Improvements include:

- Ark no longer deletes files saved from inside the embedded viewer</li>
- The address book now remembers birthdays when merging contacts</li>
- Several missing diagram display updates were fixed in Umbrello</li>
