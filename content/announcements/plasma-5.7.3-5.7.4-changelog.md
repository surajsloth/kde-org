---
title: Plasma 5.7.4 Complete Changelog
version: 5.7.4
hidden: true
plasma: true
type: fulllog
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Remove redundant includes. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ffa3be8a5897c9b802124f9ed901f74569614898'>Commit.</a>

### <a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a>

- [Notifier] Use "update-high" for security updates. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c707f9aab68d05767d3ffcec3306aea1ebc02c7a'>Commit.</a>

### <a name='kactivitymanagerd' href='http://quickgit.kde.org/?p=kactivitymanagerd.git'>kactivitymanagerd</a>

- All config updates have the same importance. <a href='http://quickgit.kde.org/?p=kactivitymanagerd.git&amp;a=commit&amp;h=b7f8c6f1c0fd433863944f24a75288ded95f15cf'>Commit.</a>
- Even in kiosk mode, we need to be able to create at least one activity. <a href='http://quickgit.kde.org/?p=kactivitymanagerd.git&amp;a=commit&amp;h=9c1e5bb02d526be585c73d8fe13e158954bb1483'>Commit.</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- [Show Desktop applet] Fix showing desktop through applet keyboard shortcut. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=570d08e59f5c59dca385b2384c2e2e05e638b7d4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365825'>#365825</a>

### <a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a>

- [kcm] decrease snapping area. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=bdaa4258090782982364ac70ac83c4035272891a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365353'>#365353</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- Ensure to directly delete old Shadow on update. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fe8fc6f83d3a567b0774b433df011f899f7230d3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361154'>#361154</a>
- Select also raw button press/release in XInput2 based polling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=71c996fe33d0b4032c0e71293a1c283d69b9c2f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366612'>#366612</a>
- [wayland] Ensure that pointer enter event carries the correct coordinates. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=921e27257cf13d7e3acf5ac6ee3c7dc18417d955'>Commit.</a>
- [autotest/intergration] Wait for pointer enter before simulating button press. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f31989f701a33ae697f06ac8df6ee83f46a61bc5'>Commit.</a>

### <a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a>

- Xrandr-crtc: clear outputs in update(). <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b41afa319d91683e99ba0d7d8c5760aaf0fe86fe'>Commit.</a>
- Update CRTCs before enabling an output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5ad4370cce960c5b5192a47169b353ed94414815'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366346'>#366346</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- [Kicker Dash] Use ComplementaryColorGroup for icon tinting. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=22b909a856312843ef7ede362828d1d8ba088849'>Commit.</a>
- [kcm_mouse] Sync KDE4 config after writing the KF5 settings, not before. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4f4da83327e66f125c8dd1749df8efe1b5084945'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348254'>#348254</a>. Fixes bug <a href='https://bugs.kde.org/367074'>#367074</a>. Code review <a href='https://git.reviewboard.kde.org/r/128703'>#128703</a>
- [Pager] Hide minimized windows. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=092aeb53d36e5d7f9bdf937b5fdb3388e8eaebe1'>Commit.</a>
- [Task Manager ToolTipDelegate] Silence warning on startup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c8799879688febeefbf6b53d4ad01e0fd4fa2269'>Commit.</a>
- [Kickoff] Fix start row for drag not always being correct. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5eaadb75de3dfdd997bdab7acbbeda2a8390858f'>Commit.</a>
- [Kickoff] Fix being unable to reorder entries in favorites menu after scrolling down. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=784b07f26ae8fa68000c844a888cf8d5845ff06c'>Commit.</a>
- Use a Timer to switch on hover event handling and do the initial geo export. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=46919b609d0d6fc148d5c211e0ec5c41208f2b10'>Commit.</a>
- Revert "Use a Timer to switch on hover event handling and do the initial geo export.". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e737c905f095c2380d881ea76b05425567e0e152'>Commit.</a>
- Use a Timer to switch on hover event handling and do the initial geo export. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=41f293891f06f2f4e14a5dec56005aa69e2546b8'>Commit.</a>
- Fix ReferenceError on external drag hover with !separateLaunchers. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b87748c8ed29c1e8585d280cda63d770c25f6fba'>Commit.</a>

### <a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a>

- Add 1s delay before trying to reconnect to pa. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=aa5efa1344ab34ffc580d73669b1eb2bd77d416c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366320'>#366320</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- Fix mistake between Lock and Leave action in lock_logout applet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=aeff2eafb0e1200b02d89da4cfd9a51d6e320dcf'>Commit.</a>
- [Plasma Calendar Integration] Filter holiday regions case-insensitively. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7516a8fb9dacdaf031badfd3a3a08266607f5abb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365621'>#365621</a>
- [System Tray] Don't reserve space for expander if it's not visible. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cd19257085e89015b6ca38e255d555c613b21527'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365636'>#365636</a>

### <a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a>

- Revert "Don't unconditionally emit buttonPressed on profile load". <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=71e9a3369c850df9822396e624a0847eae95cc24'>Commit.</a>
- Don't unconditionally emit buttonPressed on profile load. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=48c3dca61d7cb808c904a3a3659b2cd36fba1866'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366125'>#366125</a>
