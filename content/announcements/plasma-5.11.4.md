---
title: KDE Plasma 5.11.4, Bugfix Release for November
release: plasma-5.11.4
version: 5.11.4
description: KDE Ships 5.11.4
date: 2017-11-28
layout: plasma
changelog: plasma-5.11.3-5.11.4-changelog
---

{{%youtube id="nMFDrBIA0PM"%}}

{{<figure src="/announcements/plasma-5.11/plasma-5.11.png" alt="KDE Plasma 5.11 " class="text-center" width="600px" caption="KDE Plasma 5.11">}}

Tuesday, 28 November 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.11.4." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.11" "October" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "three week's" %}}

- KSysGuard: Use OCS to retrieve Tabs from the KDE store. <a href="https://commits.kde.org/ksysguard/fdead6c30886e17ea0e590df2e7ddb79652fb328">Commit.</a> Fixes bug <a href="https://bugs.kde.org/338669">#338669</a>. Phabricator Code review <a href="https://phabricator.kde.org/D8734">D8734</a>
- Be more explicit about Qt5::Widgets dependencies. <a href="https://commits.kde.org/plasma-desktop/1436ce4c23f8e0bc2e4a1efca73a9a436c34331a">Commit.</a>
- Discover: Don't crash if we get into a weird state. <a href="https://commits.kde.org/discover/63fbc9bf5ef1cbf7fa4743770f032fbe342aa53f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/385637">#385637</a>
