---
title: KDE 4.0-alpha1 Release Announcement
date: "2007-05-11"
description: "KDE Project Ships First Alpha Release for Leading Free Software Desktop."
---


<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships First Alpha Release for Leading Free
   Software Desktop, Codename "Knut"
</h3>

<p align="justify">
  
KDE 4.0-alpha1 features major overwork of artwork, new frameworks taking care
of issues from hardware integration to multimedia and some new and improved applications.
  
</p>

<p align="justify">
   
The KDE Community is happy to announce the immediate availability of the first alpha release of the
KDE Desktop Environment, version 4.0. The release is a basis for the integration of powerful new
technologies that will be included in KDE 4.
<p />

Highlights of KDE 4.0 Alpha1 are:

<ul>
	<li> A new visual appearance through Oxygen</li>
	<li> New frameworks to build applications with, providing vastly improved
hardware and multimedia integration (through <a href="http://solid.kde.org">Solid</a> and <a
href="http://phonon.kde.org">Phonon</a>, spelling and grammar checking, to name just a few)</li>
	<li>New applications that focus on a smooth user experience, such as Dolphin, the
file manager, and Okular, the document viewer</li>
</ul>

KDE 4.0 Alpha1 marks the end of the addition of large features to the KDE base libraries and shifts
the focus onto integrating those new technologies into applications and the basic desktop. The
next few months will be spent on bringing the desktop into shape after two years of frenzied development
leaving very little untouched.
<p />
The final release of KDE 4.0 is expected in late October 2007.
The work on the framework makes application development easier and more satisfying.  This will result in a crop of new features and applications over the next few years, underlining the position KDE holds as the leading Free Desktop and
bringing Free Software to new users world-wide. A detailed <a href="http://techbase.kde.org/Schedules/KDE4/4.0_Release_Schedule">release schedule</a> can be found
at <a href="http://techbase.kde.org">techbase.kde.org</a>, the new technical resource accompanying the platform, that provides extensive and high-quality documentation about KDE.
<p />

<p align="justify">
  For more information, including downloading and installation instructions, please refer to the 
  <a href="http://www.kde.org/announcements/visualguide-4.0-alpha1">visual guide</a> 
  that also gives some ideas of the current state of KDE 4 development.
</p>



<h4>
  Supporting KDE
</h4>
<p align="justify">
KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a> project that exists and grows only because of the
help of many volunteers that donate their time and effort. KDE
is always looking for new volunteers and contributions, whether its
help with coding, bug fixing or reporting, writing documentation,
translations, promotion, money, etc. All contributions are gratefully
appreciated and eagerly accepted. Please read through the <a href="/community/donations/">Supporting
KDE page</a> for further information. </p>

<p align="justify">
We look forward to hearing from you soon!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an <a href="../../community/awards/">award-winning</a>, independent <a href="/people/">project of hundreds</a>
  of developers, translators, artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.</p>

<p align="justify">
  The KDE 3 branch continues to provide a stable, mature desktop including a state-of-the-art browser
  (<a href="http://konqueror.kde.org/">Konqueror</a>), a personal information
  management suite (<a href="http://kontact.org/">Kontact</a>), a full 
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking application and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE<sup>&#174;</sup> and the K Desktop Environment<sup>&#174;</sup> logo are 
  registered trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr />

{{% include "content/includes/press_contacts.html" %}}
