---
title: KDE Plasma 5.16.2, Bugfix Release for June
release: 'plasma-5.16.2'
version: "5.16.2"
description: KDE Ships Plasma 5.16.2.
date: 2019-06-25
layout: plasma
changelog: plasma-5.16.1-5.16.2-changelog
---

{{% youtube id="T-29hJUxoFQ" %}}

{{<figure src="/announcements/plasma-5.16/plasma_5.16.png" alt="Plasma 5.16" class="text-center" width="600px" caption="KDE Plasma 5.16">}}

Tuesday, 25 June 2019.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.16.2." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in June with many feature refinements and new modules to complete the desktop experience." "5.16" %}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Klipper: Always restore the last clipboard item. <a href="https://commits.kde.org/plasma-workspace/3bd6ac34ed74e3b86bfb6b29818b726baf505f20">Commit.</a>
- Discover: Improved notification identification for Snap applications. <a href="https://commits.kde.org/plasma-workspace/951551bc6d1ab61e4d06fe48830459f23879097c">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D21881">D21881</a>
- Notifications: Don't keep non-configurable notifications in history. <a href="https://commits.kde.org/plasma-workspace/1f6050b1740cf800cb031e98fd89bc00ca20c55a">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D22048">D22048</a>
