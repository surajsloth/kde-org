---
title: KDE Ships KDE Applications 15.04.3
description: KDE Ships KDE Applications 15.04.3
date: 2015-07-01
version: 15.04.3
changelog: fulllog_applications-15.04.3
layout: application
---

{{% i18n_var "July 1, 2015. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-15.04.0" %}}

More than 20 recorded bugfixes include improvements to kdenlive, kdepim, kopete, ktp-contact-list, marble, okteta and umbrello.

{{% i18n_var "This release also includes Long Term Support versions of Plasma Workspaces %[1]s, KDE Development Platform %[2]s and the Kontact Suite %[2]s." "4.11.21" "4.14.10" %}}
