---
title: KDE Plasma 5.8.6, Bugfix Release for February
release: "plasma-5.8.6"
version: "5.8.6 LTS"
description: KDE Ships Plasma 5.8.6.
date: 2017-02-21
layout: plasma
changelog: plasma-5.8.5-5.8.6-changelog
---

{{%youtube id="LgH1Clgr-uE"%}}

{{<figure src="/announcements/plasma-5.8/plasma-5.8.png" alt="KDE Plasma 5.8 " class="text-center" width="600px" caption="KDE Plasma 5.8">}}

Tuesday, 21 February 2017.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma %[1]s, versioned %[2]s." "5.8 LTS" "5.8.6 LTS" %}}

{{% i18n_var `<a href='%[1]s'>Plasma %[2]s</a> was released in October with many feature refinements and new modules to complete the desktop experience.` "/announcements/plasma-5.8.0" "5.8 LTS" %}}

This release adds a two months' worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- {{% i18n_var `Avoid a crash on Kwin decoration KCM teardown. <a href="%[1]s">Commit.</a> See bug <a href="%[2]s">%[3]s</a>` "https://commits.kde.org/kwin/70d2fb2378d636ef6d052da08417b27c99182fb0" "https://bugs.kde.org/373628" "#373628" %}}
- {{% i18n_var `[Folder View] Fix right click erroneously opening files. <a href="%[1]s">Commit.</a> Fixes bug <a href="%[2]s">%[3]s</a>` "https://commits.kde.org/plasma-desktop/d2fde361d3c8fb40fb6c1e53e4178042799b6691" "https://bugs.kde.org/360219" "#360219" %}}

- {{% i18n_var `Fix regression in which the Save dialog appears as an Open dialog. <a href="%[1]s">Commit.</a> Code review <a href="%[2]s">%[3]s</a>` "https://commits.kde.org/plasma-integration/87b27476cc8a3865994da066ce06a3e836462719" "https://git.reviewboard.kde.org/r/129732" "#129732" %}}
