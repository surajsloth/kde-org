---
title: "KDE Plasma 5.9.2, bugfix Release for February"
release: "plasma-5.9.2"
description: KDE Ships Plasma 5.9.2.
date: 2017-02-14
layout: plasma
changelog: plasma-5.9.1-5.9.2-changelog
---

{{%youtube id="lm0sqqVcotA"%}}

{{<figure src="/announcements/plasma-5.9/plasma-5.9.png" alt="KDE Plasma 5.9 " class="text-center" width="600px" caption="KDE Plasma 5.9">}}

Tuesday, 14 February 2017.

{{% i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.9.2" %}}

{{% i18n_var "<a href='%[1]s'>Plasma %[2]s</a> was released in January with many feature refinements and new modules to complete the desktop experience." "https://www.kde.org/announcements/plasma-5.9.0.php" "5.9" %}}

This release adds a two week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

+ {{% i18n_var "Fix crash in Screen Locker KCM on teardown. <a href='%[1]s'>Commit.</a> See bug <a href='%[2]s'>%[3]s</a>" "https://commits.kde.org/kscreenlocker/51c4d6c8db8298dbd471fa91de6cb97bf8b4287a" "https://bugs.kde.org/373628" "#373628" %}}
+ {{% i18n_var "Fix Discover Appstream Support: make sure we don't show warnings unless it's absolutely necessary. <a href='%[1]s'>Commit.</a>" "https://commits.kde.org/discover/d79cb42b7d77dd0c6704a3a40d85d06105de55b4" %}}
