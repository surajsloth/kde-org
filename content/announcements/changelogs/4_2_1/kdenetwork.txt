------------------------------------------------------------------------
r915001 | casanova | 2009-01-22 09:49:53 +0000 (Thu, 22 Jan 2009) | 3 lines

Backport commit 914889:
 * No need to kde4_moc_headers'ize headers if the header file is in the same directory as the source file.

------------------------------------------------------------------------
r915129 | scripty | 2009-01-22 13:51:39 +0000 (Thu, 22 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r915334 | rjarosz | 2009-01-22 20:20:11 +0000 (Thu, 22 Jan 2009) | 9 lines

Fix ICQ for CIS countries. Apparently ICQ decided that flac sequence number can't be random anymore.
Please test if there aren't any invalid flacs in the list.

Dirk please put this into KDE 4.2.0, thanks!

BUG: 181535
CCMAIL:mueller@kde.org


------------------------------------------------------------------------
r915670 | rjarosz | 2009-01-23 13:44:40 +0000 (Fri, 23 Jan 2009) | 7 lines

Backport commit 915669.
Fix bug 172188: Clicking on "cancel" instead of "close chat" closes chat anyway.
Patch by Tomáš Trnka. Thanks!

CCBUG: 172188


------------------------------------------------------------------------
r916554 | rjarosz | 2009-01-25 14:38:18 +0000 (Sun, 25 Jan 2009) | 7 lines

Backport commit 916552.
Use algorithm which will randomly generate all valid start flac sequence number.
Code taken from Miranda. Sequence number were debugged against ICQ 6.5 (build 1005).
It's still possible to override it in kopeterc file.

CCBUG: 181535

------------------------------------------------------------------------
r916563 | uwolfer | 2009-01-25 14:56:20 +0000 (Sun, 25 Jan 2009) | 5 lines

Manually backport to 4.2:
SVN commit 916562 by uwolfer:

Disable the KRDC window while opening a KWallet dialog. Otherwise the user can close the tab and KRDC would crash. KWallet does dim the window, but cannot disable inputs unfortunately.
CCBUG:181230
------------------------------------------------------------------------
r918664 | casanova | 2009-01-30 13:07:50 +0000 (Fri, 30 Jan 2009) | 2 lines

 * Set licence to LGPL 2 instead of GPL 3

------------------------------------------------------------------------
r918847 | casanova | 2009-01-30 20:51:58 +0000 (Fri, 30 Jan 2009) | 2 lines

 Backport :  Fix jingle calls GUI and contact registering crash.

------------------------------------------------------------------------
r918868 | kossebau | 2009-01-30 21:50:02 +0000 (Fri, 30 Jan 2009) | 2 lines

backport of 918853: cleanup toResolve on exit, set typebrowser 0 in constructor

------------------------------------------------------------------------
r918974 | hasso | 2009-01-31 07:38:27 +0000 (Sat, 31 Jan 2009) | 2 lines

Backport rev 918972 - make it link.

------------------------------------------------------------------------
r919435 | mzanetti | 2009-01-31 23:54:04 +0000 (Sat, 31 Jan 2009) | 2 lines

backport of plaintext handling fix.

------------------------------------------------------------------------
r919436 | rjarosz | 2009-01-31 23:57:06 +0000 (Sat, 31 Jan 2009) | 7 lines

Backport commit 919432.
Fixes bug when empty messages were shown in chat and history window.

CCBUG: 177442
CCBUG: 182114


------------------------------------------------------------------------
r919442 | ttrnka | 2009-02-01 00:14:08 +0000 (Sun, 01 Feb 2009) | 3 lines

Backport r919429:
Do not send repeated typing notifications.

------------------------------------------------------------------------
r920383 | mzanetti | 2009-02-02 19:58:32 +0000 (Mon, 02 Feb 2009) | 2 lines

backport of rev 920380

------------------------------------------------------------------------
r920417 | darioandres | 2009-02-02 21:32:30 +0000 (Mon, 02 Feb 2009) | 5 lines

Backport of SVN commit 920415 to 4.2branch
Fix crash (it was creating a function-scope object instead of initializing the class one)
BUG: 176645


------------------------------------------------------------------------
r920759 | mzanetti | 2009-02-03 18:47:28 +0000 (Tue, 03 Feb 2009) | 2 lines

backport of bugfix 182121

------------------------------------------------------------------------
r920797 | mzanetti | 2009-02-03 19:54:57 +0000 (Tue, 03 Feb 2009) | 2 lines

backport revision 920794

------------------------------------------------------------------------
r920984 | duffeck | 2009-02-04 09:11:24 +0000 (Wed, 04 Feb 2009) | 3 lines

backport: fix downloading buddy icons
CCBUG:146118

------------------------------------------------------------------------
r921076 | lappelhans | 2009-02-04 13:45:54 +0000 (Wed, 04 Feb 2009) | 3 lines

Change external to tag
BUG:183176

------------------------------------------------------------------------
r921978 | mattr | 2009-02-06 02:57:20 +0000 (Fri, 06 Feb 2009) | 7 lines

Backport patch for 150482 to the KDE 4.2 branch.

It'll be in KDE 4.2.1

BUG: 150482


------------------------------------------------------------------------
r922083 | salem | 2009-02-06 12:38:47 +0000 (Fri, 06 Feb 2009) | 6 lines

- do not prompt to add users already in block or allow list

BUG: 181709
CCBUG: 180982


------------------------------------------------------------------------
r922557 | scripty | 2009-02-07 08:49:12 +0000 (Sat, 07 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r923817 | duffeck | 2009-02-09 14:00:12 +0000 (Mon, 09 Feb 2009) | 3 lines

fix bug where yahoo disconnects immediately when connecting.
CCBUG:163307 

------------------------------------------------------------------------
r924007 | rjarosz | 2009-02-09 21:27:44 +0000 (Mon, 09 Feb 2009) | 4 lines

Backport: Fix bug 182859: History-plugin: Old messages displayed in regular formatting
CCBUG: 182859


------------------------------------------------------------------------
r924704 | mlaurent | 2009-02-11 13:04:54 +0000 (Wed, 11 Feb 2009) | 2 lines

Extract messages

------------------------------------------------------------------------
r925025 | scripty | 2009-02-12 07:43:51 +0000 (Thu, 12 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r925685 | rjarosz | 2009-02-13 21:19:05 +0000 (Fri, 13 Feb 2009) | 4 lines

Backport thiago's commit 922100.
CCBUG:182996


------------------------------------------------------------------------
r925830 | rjarosz | 2009-02-14 10:53:43 +0000 (Sat, 14 Feb 2009) | 9 lines

Backport SVN commit 916110.
Set account priority based on position in account config view.
Allow user to move accounts between identities with drag&drop.

BUG: 165947
CCBUG: 144627
CCBUG: 81629


------------------------------------------------------------------------
r926772 | scripty | 2009-02-16 06:28:10 +0000 (Mon, 16 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r927482 | villani | 2009-02-17 16:34:02 +0000 (Tue, 17 Feb 2009) | 3 lines

Backport from 4.3 (r927480):
Make GCC 4.4 happy

------------------------------------------------------------------------
r928769 | mattr | 2009-02-20 04:35:59 +0000 (Fri, 20 Feb 2009) | 1 line

backport the disconnect fix for KDE 4.2.1
------------------------------------------------------------------------
r928776 | mattr | 2009-02-20 05:01:20 +0000 (Fri, 20 Feb 2009) | 6 lines

Backport the display name display fix
This will be in KDE 4.2.1

BUG: 182366


------------------------------------------------------------------------
r928782 | mattr | 2009-02-20 05:19:07 +0000 (Fri, 20 Feb 2009) | 5 lines

fix the indent contacts setting

BUG: 165252
This will be fixed in KDE 4.2.1

------------------------------------------------------------------------
r928794 | scripty | 2009-02-20 07:22:24 +0000 (Fri, 20 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r929115 | rjarosz | 2009-02-20 17:06:28 +0000 (Fri, 20 Feb 2009) | 4 lines

Fix big memory leak in wlm protocol.

BUG: 183930

------------------------------------------------------------------------
r929341 | scripty | 2009-02-21 07:31:02 +0000 (Sat, 21 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r929530 | darioandres | 2009-02-21 14:53:36 +0000 (Sat, 21 Feb 2009) | 8 lines

Backport to 4.2branch of:
SVN commit 929527 by darioandres:

Fix the crash on kppp -c <connection name>. It isn't the best fix (strange
code) but at least it won't crash
CCBUG: 176028


------------------------------------------------------------------------
r930016 | pino | 2009-02-22 15:09:07 +0000 (Sun, 22 Feb 2009) | 2 lines

fix icons

------------------------------------------------------------------------
r930184 | uwolfer | 2009-02-22 19:55:19 +0000 (Sun, 22 Feb 2009) | 7 lines

Backport:
SVN commit 930182 by uwolfer:

Improve hosts dockwidget behavior:
* do not colapse it when the history changes
* use less place by not showing root item tree lines
CCBUG:169444
------------------------------------------------------------------------
