2008-03-10 15:40 +0000 [r784075]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/widgets/timer.cpp: Fix
	  timer: setInterval shoudl work when the timer is not running as
	  well.

2008-03-10 23:11 +0000 [r784337]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/editor/connectioneditorimpl.cpp:
	  enable additional slots in editor

2008-03-10 23:40 +0000 [r784347]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/editor/connectioneditorimpl.cpp:
	  Direct manipulation of actions with slots using dialog - Yipee!

2008-03-18 18:26 +0000 [r787172]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/plugin/specialinformation.cpp:
	  extend limit on parameters for scripts

2008-03-18 18:32 +0000 [r787178]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog: add recent
	  changes

2008-03-19 12:18 +0000 [r787506]  mojo

	* branches/KDE/3.5/kdewebdev/README: Simplify CCBUG: 159562

2008-03-19 21:29 +0000 [r787775-787773]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/widget/functionlib.cpp:
	  Regression fix: add back the missing str_compare function. I have
	  no idea when did it disappear.

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog: Update the
	  changelog.

2008-03-20 07:43 +0000 [r787913]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/editor/connectioneditorimpl.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.h: see
	  change log - restored sort to connect dialog with actions, added
	  data relevent functionality to TreeWidget for manipulating
	  columns

2008-03-20 19:44 +0000 [r788128]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp: added a
	  sort for whole rows in tables

2008-03-20 21:14 +0000 [r788151]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/textedit.h,
	  branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/textedit.cpp: added
	  lost focus event to TextEdit

2008-03-20 22:36 +0000 [r788177]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/textedit.h,
	  branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/textedit.cpp: added
	  gotFocus event

2008-03-21 04:47 +0000 [r788235-788234]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/aboutdialog.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/aboutdialog.cpp:
	  fixed about dialog license to take text or files as was indicated
	  in the function registration

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/aboutdialog.cpp:
	  clean up

2008-03-22 08:21 +0000 [r788685]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/lineedit.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/lineedit.cpp: added
	  gotfocus to lineedit

2008-03-23 08:25 +0000 [r789114-789113]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp: fixed
	  problem with table widget visibility by added a new function

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog: fix error

2008-03-23 09:11 +0000 [r789117]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/combobox.cpp: made
	  combobox a little less mousy ;)

2008-03-23 10:27 +0000 [r789129]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/combobox.cpp: Oops!
	  So that's what happens if you don't use unique identifiers :/
	  fixed!

2008-03-23 15:51 +0000 [r789205]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp: fix last
	  row or colum being treated as off table grid and nullifying
	  command to keep visible - don't know why it needs to be -1

2008-03-23 16:04 +0000 [r789214]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp: made the
	  cellWidget creation hack honor cell visiblity without needing a
	  special call

2008-03-24 18:35 +0000 [r789619]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/textedit.cpp: if I
	  hear from translators i will revert new strings - I'm using SVN
	  to update the other machines on my network running Kommander and
	  this is a useful feature returns modified property of TextEdit -
	  true if user modified

2008-03-25 00:58 +0000 [r789726]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/variable_cmds.cpp:
	  fix cause of crash when setting variables (Crash when setting the
	  value of a xslt variable using the inspector tool backport fix
	  from KDE trunk) BUG:159812

2008-03-25 22:41 +0000 [r790132]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/nodeview_cmds.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/variable_cmds.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libqtnotfier/xsldbgevent.cpp:
	  apply bug fix for crash when using newer versions of
	  libxml/libxslt: bug #450549 (debian bug system) Backport bug fix
	  from trunk BUG:144749

2008-03-25 23:25 +0000 [r790152]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/kxsldbg_part.cpp:
	  Initial size of text view is too small use minimum size that is a
	  little more sane Backport from kxsldbg in trunk BUG:159864

2008-03-27 03:54 +0000 [r790638]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/data/xsldbghelp.xsl (removed),
	  branches/KDE/3.5/kdewebdev/doc/xsldbg/xsldbghelp.xsl (added),
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/help_unix.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/options.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/data/Makefile.am: Work harder
	  to ensure that help command actually presents the help text by
	  handling the case where $KDEDIRS is used BUG:159931

2008-03-27 09:49 +0000 [r790719]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/scriptobject.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/scriptobject.cpp:
	  added missing boolean slot for scripts - will port as soon as I
	  have KDE4 running and can test... don't want to break trunk :)

2008-03-27 21:20 +0000 [r790936]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgconfigimpl.cpp:
	  When choosing XSL script to debug the open file dialog is missing
	  *.xslt from default file filter BUG:159974

2008-03-28 01:12 +0000 [r791029]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp: minor
	  cleanup - if you were using TreeWidget.addColumnTree it is now
	  cleaner... TreeWidget.addColumn

2008-03-28 01:52 +0000 [r791035]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/specialinformation.cpp:
	  make removeColumn "columns" parameter optional like it says it is
	  now I can use the DCOP function definition for the same function
	  on ListViews

2008-03-28 02:17 +0000 [r791036]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.cpp: if
	  you edit somebody elses code you copied... copy yours too ;)

2008-03-28 19:18 +0000 [r791218]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/specials.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/execbutton.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/specialinformation.cpp:
	  experimenting with adding geometry function

2008-03-30 01:20 +0000 [r791619]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/xsldbg.cpp:
	  Entities in XML document are not listed and unable to set
	  breakpoints on nodes in extenal Entites (both xsldbg and kxsldbg)
	  Will be integrated to the trunk branch of kxsldbg at a later date
	  BUG:160085

2008-03-31 22:58 +0000 [r792386]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/xsldbgnotifier.h,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/cmds.h,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/xsldbgthread.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/xsldbgmsg.h,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/xsldbgthread.h:
	  Correct copyrite information to refer me correctly ie keith ->
	  Keith Isdale keith@linux -> k_isdale@tpg.com.au

2008-03-31 23:46 +0000 [r792396]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgbreakpoints.ui,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgcallstack.ui,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbglocalvariables.ui,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgsources.ui,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgentities.ui,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgconfig.ui,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgmsgdialog.ui,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/debugXSL.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgglobalvariables.ui,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgtemplates.ui,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgwalkspeed.ui:
	  Add my email address to my name in the author details in .ui
	  files

2008-04-11 22:39 +0000 [r795925]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/files.cpp:
	  Remove a check that was leading to the incorrect URI for the
	  supplied node Bug to be ported forward to KDE 4.1 in a separate
	  commit BUG:160730

2008-04-12 01:24 +0000 [r795949]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/qxsldbgdoc.h,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/kxsldbg_part.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/qxsldbgdoc.cpp:
	  Correct the display of debugging Marks BUG:160734

2008-04-13 00:46 +0000 [r796197]  isdale

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/file_cmds.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/files_unix.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/kxsldbg_part.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgdebugger.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/files.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/breakpoint_cmds.cpp:
	  Fix support for files name that start with "~" or "file:/" As far
	  as possible add support for files names that include non-latin1
	  chararacters BUG:160765

2008-04-14 19:31 +0000 [r797043]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/specials.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/listbox.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/editor/assoctexteditorimpl.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/combobox.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/popupmenu.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widget/functionlib.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/textedit.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/datepicker.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/lineedit.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/specialinformation.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/pixmaplabel.cpp:
	  added hasFocus, clipboard menus and geometry updates

2008-04-18 01:46 +0000 [r798347]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/dialog.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/specials.h,
	  branches/KDE/3.5/kdewebdev/kommander/widget/functionlib.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/specialinformation.cpp:
	  minor improvements

2008-04-18 22:38 +0000 [r798709]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/plugin/specials.h,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/specialinformation.cpp:
	  added count columns and rows and fixed bug that crashed dialog
	  when attempting to set the caption of a non existant row in table
	  and listview

2008-04-18 23:21 +0000 [r798716]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/widgets/table.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/table.h: fix wrong
	  parameter count

2008-04-22 21:45 +0000 [r799932]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/editor/kommander-new.xml,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/treewidget.h,
	  branches/KDE/3.5/kdewebdev/kommander/editor/kommander.xml,
	  branches/KDE/3.5/kdewebdev/kommander/plugin/specialinformation.cpp:
	  some cleanup - porting some user enhancements for my use -
	  porting

2008-04-24 08:48 +0000 [r800440]  sequitur

	* branches/KDE/3.5/kdewebdev/kommander/ChangeLog,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/tabwidget.cpp: sleep
	  hacking ;)

2008-06-29 17:09 +0000 [r825998]  anagl

	* branches/KDE/3.5/kdewebdev/kfilereplace/kfilereplace.desktop,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbg.desktop,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus.desktop,
	  branches/KDE/3.5/kdewebdev/kimagemapeditor/kimagemapeditor.desktop,
	  branches/KDE/3.5/kdewebdev/kommander/editor/kmdr-editor.desktop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.desktop: Desktop
	  validation fixes: remove deprecated entries for Encoding.

2008-06-29 22:56 +0000 [r826189]  anagl

	* branches/KDE/3.5/kdewebdev/kimagemapeditor/kimagemapeditorpart.desktop,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/gubed/quantadebuggergubed.desktop,
	  branches/KDE/3.5/kdewebdev/quanta/src/x-webprj.desktop,
	  branches/KDE/3.5/kdewebdev/kommander/x-kommander.desktop,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_be.desktop,
	  branches/KDE/3.5/kdewebdev/kfilereplace/kfilereplacepart.desktop,
	  branches/KDE/3.5/kdewebdev/kommander/executor/kmdr-executor.desktop,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/klinkstatus_part.desktop,
	  branches/KDE/3.5/kdewebdev/kommander/part/kommander_part.desktop,
	  branches/KDE/3.5/kdewebdev/quanta/data/config/quanta_preview_config.desktop,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/kxsldbg_part.desktop,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/widgets.desktop,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/dbgp/quantadebuggerdbgp.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-08-13 10:39 +0000 [r846273]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp:
	  Backport: - Respect user-agent - Insert referer in http header
	  Can someone check if this builds? kubuntu doesn't have unsermake
	  and i can't find out how to make it work with make :/

2008-08-19 09:20 +0000 [r849225]  hasso

	* branches/KDE/3.5/kdewebdev/doc/kommander/parser.docbook: Let
	  translators produce valid docbook files.

2008-08-19 19:48 +0000 [r849608]  coolo

	* branches/KDE/3.5/kdewebdev/kdewebdev.lsm: update for 3.5.10

2008-08-19 19:52 +0000 [r849613]  coolo

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.h: update for 3.5.10

2008-08-20 15:06 +0000 [r849979]  coolo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.cpp:
	  fix compile

