------------------------------------------------------------------------
r932677 | scripty | 2009-02-27 08:15:30 +0000 (Fri, 27 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r932732 | rdale | 2009-02-27 10:37:28 +0000 (Fri, 27 Feb 2009) | 3 lines

* For the methods that need to be special cased to work with 
  PlasmaScripting::Applet type arguments, add lower case/underscore versions
  of the methods
------------------------------------------------------------------------
r932766 | dfaure | 2009-02-27 12:40:50 +0000 (Fri, 27 Feb 2009) | 3 lines

Remove this altogether, it was intended to be only in trunk, not in stable branch nor releases, says dirk.
CCBUG: 185225

------------------------------------------------------------------------
r933026 | winterz | 2009-02-27 20:52:16 +0000 (Fri, 27 Feb 2009) | 4 lines

backport SVN commit 933023 by winterz:

typo fix, found by Robby Stephenson

------------------------------------------------------------------------
r933155 | scripty | 2009-02-28 07:39:35 +0000 (Sat, 28 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r933172 | lmurray | 2009-02-28 09:25:02 +0000 (Sat, 28 Feb 2009) | 4 lines

Backport: Prevent present windows from crashing if the highlighted
window from the previous activation is closed and the effect is
activated a second time.

------------------------------------------------------------------------
r933373 | mart | 2009-02-28 19:29:28 +0000 (Sat, 28 Feb 2009) | 2 lines

backport a painting fix on vertical panels

------------------------------------------------------------------------
r934456 | lmurray | 2009-03-03 06:35:46 +0000 (Tue, 03 Mar 2009) | 2 lines

A couple more present windows stability patches.

------------------------------------------------------------------------
r934794 | aseigo | 2009-03-03 19:59:16 +0000 (Tue, 03 Mar 2009) | 3 lines

change the day on the calendar when the date changes
CCBUG:185784

------------------------------------------------------------------------
r934863 | aseigo | 2009-03-03 23:39:55 +0000 (Tue, 03 Mar 2009) | 3 lines

backport this commit as it's also in the branch
BUG:185736

------------------------------------------------------------------------
r934915 | scripty | 2009-03-04 08:31:15 +0000 (Wed, 04 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r935009 | abizjak | 2009-03-04 12:55:40 +0000 (Wed, 04 Mar 2009) | 4 lines

backport revision 935006

Allow plasmoids to be placed close to screen edges.

------------------------------------------------------------------------
r935015 | graesslin | 2009-03-04 12:59:46 +0000 (Wed, 04 Mar 2009) | 5 lines

Backport rev 935012:
Set window flag X11BypassWindowManagerHint so that the window is on top of stacking order. By that the popup info is also shown when compositing is enabled. The popup info shown time is increased to 750 ms so that it is visible if fade effect is enabled.
CCBUG: 176908


------------------------------------------------------------------------
r935236 | hindenburg | 2009-03-04 17:29:00 +0000 (Wed, 04 Mar 2009) | 5 lines

Revert 930327.  Patch actually does word separation.

CCBUG: 176273
CCBUG: 186048

------------------------------------------------------------------------
r935358 | chehrlic | 2009-03-04 21:40:08 +0000 (Wed, 04 Mar 2009) | 1 line

win32 compile++
------------------------------------------------------------------------
r935375 | mjansen | 2009-03-04 22:22:14 +0000 (Wed, 04 Mar 2009) | 5 lines

SVN commit 934666 by froscher:

Fix a bug that prevented the deletion of list items if Qt was compiled with QT_NO_DEBUG.

BUG:179969
------------------------------------------------------------------------
r935439 | scripty | 2009-03-05 07:48:57 +0000 (Thu, 05 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r935874 | darioandres | 2009-03-06 13:28:31 +0000 (Fri, 06 Mar 2009) | 8 lines

Backport to 4.2branch of:
SVN commit 935866 by darioandres:

Show default (KToolBar) context menu in BookmarksToolbar if no action(bookmark)
is clicked.

CCBUG:167395

------------------------------------------------------------------------
r936127 | mpyne | 2009-03-07 04:06:42 +0000 (Sat, 07 Mar 2009) | 2 lines

Backport fix for wrong KDialog textinputbox QString encoding to KDE 4.2.2.

------------------------------------------------------------------------
r936129 | mpyne | 2009-03-07 04:20:12 +0000 (Sat, 07 Mar 2009) | 4 lines

Backport fix for bug 186413 (kdialog --textinputbox doesn't return result status) to KDE 4.2.2.

BUG:186413

------------------------------------------------------------------------
r936239 | ossi | 2009-03-07 08:57:25 +0000 (Sat, 07 Mar 2009) | 2 lines

backport: remove arbitrary UID limitation

------------------------------------------------------------------------
r936619 | adawit | 2009-03-08 06:28:55 +0000 (Sun, 08 Mar 2009) | 1 line

Backported the debug statement cleanup from trunk commit r936618
------------------------------------------------------------------------
r936622 | adawit | 2009-03-08 06:41:57 +0000 (Sun, 08 Mar 2009) | 1 line

Backport bug fix commit r936621 from trunk
------------------------------------------------------------------------
r936626 | scripty | 2009-03-08 07:35:23 +0000 (Sun, 08 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r936630 | adawit | 2009-03-08 07:42:45 +0000 (Sun, 08 Mar 2009) | 1 line

Backported bug fix commit r936629 from trunk
------------------------------------------------------------------------
r936951 | darioandres | 2009-03-08 18:07:22 +0000 (Sun, 08 Mar 2009) | 9 lines

Backport to 4.2branch of:

SVN commit 936948 by darioandres:

Align found items count to the right to allow long texts in the left
(translated status)

CCBUG: 179833

------------------------------------------------------------------------
r936979 | lappelhans | 2009-03-08 19:14:35 +0000 (Sun, 08 Mar 2009) | 3 lines

Backport rev 936572
Thanks to Chani for the svnbackport-script-tip!

------------------------------------------------------------------------
r937021 | lueck | 2009-03-08 21:45:58 +0000 (Sun, 08 Mar 2009) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r937058 | darioandres | 2009-03-08 22:35:49 +0000 (Sun, 08 Mar 2009) | 7 lines

Backport to 4.2branch of:
SVN commit 937050+937053 by darioandres:

Skip /dev files before looking if isBinaryData (at processQuery)

CCBUG: 168255

------------------------------------------------------------------------
r937072 | mpyne | 2009-03-08 22:53:52 +0000 (Sun, 08 Mar 2009) | 1 line

Backport fix for desktop: broken icon to KDE 4.2.2
------------------------------------------------------------------------
r937123 | adawit | 2009-03-09 04:28:11 +0000 (Mon, 09 Mar 2009) | 1 line

Backported fix for bug# 172445 from r937122 commit in trunk
------------------------------------------------------------------------
r937129 | adawit | 2009-03-09 04:45:05 +0000 (Mon, 09 Mar 2009) | 1 line

Fix decoding of path in sftpRealPath.
------------------------------------------------------------------------
r937237 | scripty | 2009-03-09 08:22:57 +0000 (Mon, 09 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r937311 | dfaure | 2009-03-09 13:51:57 +0000 (Mon, 09 Mar 2009) | 2 lines

backport: sort the mimetype list (#186517)

------------------------------------------------------------------------
r937339 | hein | 2009-03-09 15:51:15 +0000 (Mon, 09 Mar 2009) | 6 lines

Backport r935623. Fixes a layout regression affecting apps
using the KPart, sometimes causing the terminal not to show
up as well as generally adding an ugly margin around the
terminal.
BUG:171544

------------------------------------------------------------------------
r937364 | lueck | 2009-03-09 16:23:32 +0000 (Mon, 09 Mar 2009) | 1 line

remove option from backport which is not in 4.2
------------------------------------------------------------------------
r937439 | fredrik | 2009-03-09 17:37:46 +0000 (Mon, 09 Mar 2009) | 1 line

Backport r937436.
------------------------------------------------------------------------
r937558 | fredrik | 2009-03-09 22:29:18 +0000 (Mon, 09 Mar 2009) | 1 line

Backport r937557.
------------------------------------------------------------------------
r937600 | freininghaus | 2009-03-10 01:11:08 +0000 (Tue, 10 Mar 2009) | 6 lines

Call updatePasteAction() in DolphinPart::openUrl(). Fixes the problem that the paste action is disabled in Konqueror on startup.

Fix will be in KDE 4.2.2.

CCBUG: 186482

------------------------------------------------------------------------
r937627 | scripty | 2009-03-10 07:43:25 +0000 (Tue, 10 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r937639 | asommer | 2009-03-10 08:09:03 +0000 (Tue, 10 Mar 2009) | 3 lines

Backport 937637.
Resize the overlay-window to allow compositing to continue working after xrandr-events.
CCBUG:180994
------------------------------------------------------------------------
r937841 | huynhhuu | 2009-03-10 14:26:26 +0000 (Tue, 10 Mar 2009) | 4 lines

Backport of r937686:
Set proper size for groupboxes with bold titles
CCBUG: 180204

------------------------------------------------------------------------
r937903 | freininghaus | 2009-03-10 18:48:53 +0000 (Tue, 10 Mar 2009) | 6 lines

Display folder names containing '&' correctly in the "Copy To"/"Move To" menus.

This fix will be in KDE 4.2.2.

CCBUG: 186580

------------------------------------------------------------------------
r937993 | sebsauer | 2009-03-10 22:54:12 +0000 (Tue, 10 Mar 2009) | 4 lines

the backtrace from bug #186599 says this can happen. so, let's fix it
the dirty way (dirty cause the assert means, that Applet::init got
called more then once, hmmmmm....)

------------------------------------------------------------------------
r938001 | aseigo | 2009-03-10 23:09:23 +0000 (Tue, 10 Mar 2009) | 3 lines

Backport sorting fix for .. 
BUG:186317

------------------------------------------------------------------------
r938063 | scripty | 2009-03-11 08:38:09 +0000 (Wed, 11 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r938152 | dfaure | 2009-03-11 13:11:32 +0000 (Wed, 11 Mar 2009) | 4 lines

Fix infinite loop in kmail when the message window is really small in height.
The so-called "temp fix" didn't work because the rectangle was reduced again later on
and then not checked for height=0.

------------------------------------------------------------------------
r938542 | scripty | 2009-03-12 07:45:51 +0000 (Thu, 12 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r938590 | helio | 2009-03-12 12:47:04 +0000 (Thu, 12 Mar 2009) | 1 line

- Save same group apps settings on taskbar
------------------------------------------------------------------------
r938660 | hindenburg | 2009-03-12 15:53:53 +0000 (Thu, 12 Mar 2009) | 1 line

update version; version already added to bugs.kde.org
------------------------------------------------------------------------
r938760 | alexmerry | 2009-03-12 22:14:44 +0000 (Thu, 12 Mar 2009) | 3 lines

Backport r938759: don't crash when a player doesn't implement the MPRIS interface properly.


------------------------------------------------------------------------
r939017 | dfaure | 2009-03-13 15:16:39 +0000 (Fri, 13 Mar 2009) | 2 lines

backport: libkonq not needed here, as reavertm noticed.

------------------------------------------------------------------------
r939403 | orlovich | 2009-03-14 19:48:56 +0000 (Sat, 14 Mar 2009) | 7 lines

Block handling of normal I/O streams until javascript: queries are handled.
Fixes the problem with opening of local flash files reported by KMess 
developers, and hopefully the ocassional white windows on web flash stuff 
as well

CCBUG: 169626

------------------------------------------------------------------------
r939532 | scripty | 2009-03-15 07:48:29 +0000 (Sun, 15 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r939933 | hindenburg | 2009-03-16 01:05:36 +0000 (Mon, 16 Mar 2009) | 4 lines

Allow the disabling of blinking text.

BUG: 182414

------------------------------------------------------------------------
r939959 | scripty | 2009-03-16 07:53:25 +0000 (Mon, 16 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r940156 | mfuchs | 2009-03-16 18:13:33 +0000 (Mon, 16 Mar 2009) | 5 lines

Backport r937519

Deactivate the hovering of the backArrow as soon as there is a
leaveEvent.

------------------------------------------------------------------------
r940290 | scripty | 2009-03-17 08:07:39 +0000 (Tue, 17 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r940679 | mjansen | 2009-03-18 00:20:17 +0000 (Wed, 18 Mar 2009) | 7 lines

 Fix MenuEntryActions created from khotkeys. From kmenuedit they worked.

 Pop up a messagebox if a application fails to start or isn't configured.

Backport of 933943 for

BUG: 186992
------------------------------------------------------------------------
r940736 | scripty | 2009-03-18 08:17:57 +0000 (Wed, 18 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r941201 | hindenburg | 2009-03-19 03:11:44 +0000 (Thu, 19 Mar 2009) | 4 lines

Do not remove whitespace if it is the last character of wrapping line.

CCBUG: 90201

------------------------------------------------------------------------
r941204 | hindenburg | 2009-03-19 03:53:50 +0000 (Thu, 19 Mar 2009) | 4 lines

Clear selection on selected text when it is not static.

BUG: 153372

------------------------------------------------------------------------
r941235 | scripty | 2009-03-19 07:46:51 +0000 (Thu, 19 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r941438 | hindenburg | 2009-03-19 14:38:19 +0000 (Thu, 19 Mar 2009) | 4 lines

Revert previous patch; need to only delete space if at end of screen line

CCBUG: 90201

------------------------------------------------------------------------
r941449 | hein | 2009-03-19 15:06:35 +0000 (Thu, 19 Mar 2009) | 3 lines

Simplify translucency support in the Konsole KPart
thanks to Qt::WA_TranslucentBackground.

------------------------------------------------------------------------
r941450 | hein | 2009-03-19 15:08:09 +0000 (Thu, 19 Mar 2009) | 2 lines

Revert 941449 -- I was on the wrong branch, damn.

------------------------------------------------------------------------
r941704 | mlaurent | 2009-03-20 12:29:21 +0000 (Fri, 20 Mar 2009) | 2 lines

Backport: don't load twice

------------------------------------------------------------------------
r941722 | darioandres | 2009-03-20 13:07:17 +0000 (Fri, 20 Mar 2009) | 10 lines

Backport to 4.2branch of:
SVN commit 941717 by darioandres:

Enlarge the clock hands if the rect size is < KIconLoader::SizeEnormous
(currently 128px)
Improves readability on small analog clocks
Patch approved by notmart

CCBUG: 165199

------------------------------------------------------------------------
r941927 | aseigo | 2009-03-20 18:15:52 +0000 (Fri, 20 Mar 2009) | 2 lines

backport 920202 so that bug: works with our new bugzilla in 4.2 as well as trunk

------------------------------------------------------------------------
r941979 | dafre | 2009-03-20 20:01:27 +0000 (Fri, 20 Mar 2009) | 4 lines

CCMAIL:adam@piggz.co.uk

Backporting Adam Pigg's fix

------------------------------------------------------------------------
r942014 | ltoscano | 2009-03-20 20:53:10 +0000 (Fri, 20 Mar 2009) | 5 lines

Backport r942011:

add a check for setpriority() that got lost during KDE4 porting somehow
(ask Lubos for more infos)

------------------------------------------------------------------------
r942088 | pino | 2009-03-21 02:02:54 +0000 (Sat, 21 Mar 2009) | 3 lines

now that kde can load the generic icons for mimetypes, we can get rid of the duplicate versions of mimetype icons
remove duplicates for image and video categories

------------------------------------------------------------------------
r942229 | ppenz | 2009-03-21 14:00:12 +0000 (Sat, 21 Mar 2009) | 7 lines

Backport of SVN commit 942194:
- Write the settings to the disk as soon as "Apply" or "OK" has been pressed inside a settings dialog.
- Assure that the settings are saved in the DolphinPart destructor for settings that are changed outside the settings dialog (e. g. by using Ctrl + mouse wheel for adjusting the icon size).

Thanks to Frank Reininghaus and David Faure for the analyses.

BUG: 175085
------------------------------------------------------------------------
r942548 | cfeck | 2009-03-21 23:33:59 +0000 (Sat, 21 Mar 2009) | 2 lines

Backport 942544

------------------------------------------------------------------------
r942591 | scripty | 2009-03-22 07:42:25 +0000 (Sun, 22 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r942655 | dafre | 2009-03-22 12:19:16 +0000 (Sun, 22 Mar 2009) | 2 lines

Backporting patch regarding bug #183273 to 4.2

------------------------------------------------------------------------
r942882 | asommer | 2009-03-22 19:31:50 +0000 (Sun, 22 Mar 2009) | 5 lines

Backport 937005.

Query screen-geometry from kephal instead of QDesktopWidget, this makes "only 
current screen"-setting work as expected.
BUG:176765
------------------------------------------------------------------------
r942883 | asommer | 2009-03-22 19:34:55 +0000 (Sun, 22 Mar 2009) | 14 lines

Backport 937003 and 938069.

- update struts when the framebuffer is resized
- create PanelView instances only when the screen they are for is active
- destroy PanelViews when their screen is deactivated

Use mouse-poll-timer for auto-hide, even if no popup is open. This makes the 
panel stay, if the pointer only leaves for a few ms.
Correctly resize new panels to the screen they are created on.

BUG:181441
BUG:185471
BUG:179131
BUG:178066
------------------------------------------------------------------------
r943007 | scripty | 2009-03-23 07:45:09 +0000 (Mon, 23 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943288 | jacopods | 2009-03-23 16:07:28 +0000 (Mon, 23 Mar 2009) | 3 lines

+ use of the newly introduced signal userTextChanged
+ backporting some cosmetic improvement by notmart

------------------------------------------------------------------------
r943367 | pino | 2009-03-23 18:06:43 +0000 (Mon, 23 Mar 2009) | 4 lines

extract messages in .h files in the base subdir
(this gives us ~46 untranslated messages, sorry for that)
CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
r943379 | pino | 2009-03-23 18:47:23 +0000 (Mon, 23 Mar 2009) | 3 lines

missing message in plasma's engine explorer
CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
r943674 | scripty | 2009-03-24 08:19:26 +0000 (Tue, 24 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943715 | graesslin | 2009-03-24 10:19:57 +0000 (Tue, 24 Mar 2009) | 2 lines

Backport: 940812
Center desktop name box on active screen instead of the center of whole area. So the desktop name is always visible in multi screen setups and only shown on one screen.
------------------------------------------------------------------------
r943717 | graesslin | 2009-03-24 10:22:59 +0000 (Tue, 24 Mar 2009) | 6 lines

Backport: 936880
New way to determine in which direction to animate the magic lamp. Based on the assumption that icon geometry is part of a panel. Using the panel to find the position is more safe as a the height of a vertical panel is greater than the width. This might not be true for the icon geometry. If the panel is autohidden we still have to use the icon geometry using the assumption that it will border one screen edge. For the unlikely case of bordering two screen edges the wrong animation might be used but it won't be distorted.
CCBUG: 183059
CCBUG: 183099
CCBUG: 169792

------------------------------------------------------------------------
r943985 | fredrik | 2009-03-24 19:25:10 +0000 (Tue, 24 Mar 2009) | 1 line

Backport r943984.
------------------------------------------------------------------------
r944035 | fredrik | 2009-03-24 21:43:11 +0000 (Tue, 24 Mar 2009) | 1 line

Backport r944033.
------------------------------------------------------------------------
r944075 | dfaure | 2009-03-25 00:24:04 +0000 (Wed, 25 Mar 2009) | 2 lines

backport the fix for the first part of 187793, "cookies hidden in filter dialog"

------------------------------------------------------------------------
r944110 | reed | 2009-03-25 04:04:22 +0000 (Wed, 25 Mar 2009) | 1 line

fix info_osx build
------------------------------------------------------------------------
r944335 | jacopods | 2009-03-25 13:02:29 +0000 (Wed, 25 Mar 2009) | 2 lines

backporting 933424 by notmart

------------------------------------------------------------------------
r944606 | fredrik | 2009-03-25 20:22:16 +0000 (Wed, 25 Mar 2009) | 1 line

Backport r944605.
------------------------------------------------------------------------
r944627 | huynhhuu | 2009-03-25 20:53:08 +0000 (Wed, 25 Mar 2009) | 3 lines

Backport of r937006:
restore hover on toolbuttons

------------------------------------------------------------------------
r944634 | huynhhuu | 2009-03-25 21:04:42 +0000 (Wed, 25 Mar 2009) | 3 lines

Backport of r939241:
One assignment of gw (glow width) is enough

------------------------------------------------------------------------
r944636 | huynhhuu | 2009-03-25 21:05:44 +0000 (Wed, 25 Mar 2009) | 5 lines

Backport of r939314:
Tabbar arrows should not overlap with the contents area
also fix their colorgroup
BUG: 187118

------------------------------------------------------------------------
r944701 | dfaure | 2009-03-25 23:29:16 +0000 (Wed, 25 Mar 2009) | 4 lines

Workaround the Qt-4.4 fromAce/toAce bug for the kdelibs-4.2.2 release.
I thought this affected the cookie handling, but in fact it doesn't, it only affects the configuration module :(
CCBUG: 183720

------------------------------------------------------------------------
r944757 | cfeck | 2009-03-26 02:20:30 +0000 (Thu, 26 Mar 2009) | 4 lines

Do not access empty file list (backport r944756)

CCBUG: 178052

------------------------------------------------------------------------
r944768 | cfeck | 2009-03-26 03:08:53 +0000 (Thu, 26 Mar 2009) | 5 lines

Do not access empty file list (backport r944767)

CCBUG: 178853
CCBUG: 181090

------------------------------------------------------------------------
r944771 | cfeck | 2009-03-26 03:12:15 +0000 (Thu, 26 Mar 2009) | 4 lines

Do not access empty file list (backport 944769)

CCBUG: 185910

------------------------------------------------------------------------
r944808 | scripty | 2009-03-26 08:12:31 +0000 (Thu, 26 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r944888 | metellius | 2009-03-26 12:40:50 +0000 (Thu, 26 Mar 2009) | 3 lines

BUG: 187884
Do not start a new drag while one is currently underway.

------------------------------------------------------------------------
r944985 | dfaure | 2009-03-26 16:16:15 +0000 (Thu, 26 Mar 2009) | 2 lines

krunner needs KLineEdit::userText, new in kdelibs-4.2.2

------------------------------------------------------------------------
r944986 | mueller | 2009-03-26 16:18:22 +0000 (Thu, 26 Mar 2009) | 2 lines

bump version to 4.2.2

------------------------------------------------------------------------
