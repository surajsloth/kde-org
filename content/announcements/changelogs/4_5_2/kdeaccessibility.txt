------------------------------------------------------------------------
r1174634 | scripty | 2010-09-13 14:35:28 +1200 (Mon, 13 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1176916 | scripty | 2010-09-19 14:45:55 +1200 (Sun, 19 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1177650 | lueck | 2010-09-21 07:45:20 +1200 (Tue, 21 Sep 2010) | 1 line

backport: change gui strings from ktts to Jovie
------------------------------------------------------------------------
r1177738 | scripty | 2010-09-21 15:37:28 +1200 (Tue, 21 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1178839 | scripty | 2010-09-24 14:42:09 +1200 (Fri, 24 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179227 | whiting | 2010-09-25 09:23:43 +1200 (Sat, 25 Sep 2010) | 2 lines

Backport fix of plugin loading.

------------------------------------------------------------------------
r1179229 | whiting | 2010-09-25 09:24:21 +1200 (Sat, 25 Sep 2010) | 2 lines

Fix loading and saving locations of stringreplacer xml files.

------------------------------------------------------------------------
r1179232 | whiting | 2010-09-25 09:25:56 +1200 (Sat, 25 Sep 2010) | 2 lines

Backport removal of lib prefix from plugins and desktop files.

------------------------------------------------------------------------
r1179292 | scripty | 2010-09-25 14:45:59 +1200 (Sat, 25 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
