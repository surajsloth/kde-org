2007-01-18 21:04 +0000 [r625072]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/ui/icqeditaccountwidget.cpp:
	  Catch the empty ICQ UIN case when creating an account, which
	  would crash Kopete. Patch by Felix <fe_kde@gmx.de>, received with
	  thanks :). BUG:139719

2007-01-25 13:24 +0000 [r627017]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp:
	  Use passive popups where appropriate

2007-01-27 23:21 +0000 [r627746]  mklingens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnsocket.cpp:
	  Don't dump binary data to kdDebug Michel, if this also applies to
	  the new MSN lib in KDE 4, please forward port, I have no KDE 4
	  checkout here CCMAIL: Michel Hermier <michel.hermier@gmail.com>

2007-01-28 16:22 +0000 [r627923]  mklingens

	* branches/KDE/3.5/kdenetwork/kopete/plugins/history/historyviewer.ui:
	  Use normal margins and don't double them at some places

2007-01-31 19:13 +0000 [r628865]  mklingens

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/private/kopeteemoticons.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/kopeteemoticonaction.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/private/kopeteemoticons.h,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/emoticonselector.cpp:
	  Refactor the emoticon handling to return *all* emoticons
	  including "alternatives". Besides being more correct semantically
	  this makes it possible to "export" all emoticons as custom
	  emoticons via e.g. MSN. (Note that the latter doesn't work all
	  that reliable though, but that is the MSN P2P protocol, at least
	  the underlying code is now able to figure out that something has
	  to be sent in the first place)

2007-02-06 10:17 +0000 [r630769]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/keepalivetask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/logintask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/keepalivetask.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/logintask.h:
	  Fix a bug in the SP2 keepalive code that resulted in the
	  keepalive task deleting itself after sending one keepalive :(

2007-02-06 15:40 +0000 [r630865]  mueller

	* branches/KDE/3.5/kdenetwork/ksirc/ksircserver.h: remove pointless
	  qualification

2007-02-06 20:15 +0000 [r630932]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/statusnotifiertask.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/client.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/client.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/statusnotifiertask.cpp:
	  Fix buddyicons once again. Now buddyicons are always downloaded
	  and they don't disappear on the other side if messages are sent.

2007-02-07 19:04 +0000 [r631338]  dgp

	* branches/KDE/3.5/kdenetwork/krdc/maindialogwidget.cpp,
	  branches/KDE/3.5/kdenetwork/krdc/maindialogwidget.h: Enable
	  ZeroConf discovery of RDP/RFB services by creating the dnssd
	  locator at the creation of the widget, as the current scope-based
	  code is never executed, at least if SLP is disabled. Plus, a
	  single instance can take care of the continous scan during the
	  whole life of the widget, rather than having to be recreated
	  every scan.

2007-02-11 17:31 +0000 [r632577]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp:
	  Stop transmission on shutdown.

2007-02-11 18:04 +0000 [r632590]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/webcamtask.cpp:
	  Fix webcam-freeze when more than one people are watching the cam.

2007-02-11 19:06 +0000 [r632618]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/webcamtask.cpp:
	  compile++

2007-02-12 12:10 +0000 [r632817]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp:
	  Stop sending keepalives after disconnect

2007-02-13 20:01 +0000 [r633326]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp:
	  Initialize variable.

2007-02-17 18:36 +0000 [r634596]  jpetso

	* branches/KDE/3.5/kdenetwork/kopete/styles/Clear/Contents/Resources/Variants/Makefile.am
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/styles/Clear/Contents/Resources/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/styles/Clear/Contents/Resources/Variants/No_avatars.css
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/styles/Clear/Contents/Resources/Variants/No
	  avatars.css (removed): After noticing that the "No avatars" style
	  variant (r551419) actually doesn't work because autotools don't
	  know about it, make it work at least for the remaining 3.5.x
	  point releases. CCMAIL: jussi.kekkonen@gmail.com

2007-02-18 17:38 +0000 [r634872]  mklingens

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/chatmessagepart.cpp:
	  About 10-20% performance improvement for formatStyleKeywords.
	  This is important when lots of messages are added to the chat at
	  the same time, like when browsing history with alt-left/right, or
	  when the style changes, e.g. when someone is changing display
	  picture. Especially the latter is unfortunately a fairly common
	  thing, and Kopete freezes horribly if you have chats open with a
	  person changing pic. Approved by Michael.

2007-02-21 23:02 +0000 [r636111]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/localcontactlisttask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimanager.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssilisttask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimanager.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssilisttask.h:
	  Don't send local contact list if list is empty. Fix login with
	  new (empty) icq account. We cannot use lastModTime in
	  listComplete function because new icq account has lastModTime = 0
	  too. Will forwardport tomorrow.

2007-02-22 15:31 +0000 [r636245]  bram

	* branches/KDE/3.5/kdenetwork/krfb/krfb_httpd/krfb_httpd: Replace
	  Mac newlines with Unix newlines, and the problem of 100% CPU
	  utilization with the first connection disappeared. Credits go to
	  Reinhold and Modestas. BUG:135013

2007-02-23 23:57 +0000 [r636731]  mklingens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/tests/kopetemessage_test.cpp:
	  Add several more testcases, fix existing ones

2007-02-24 00:14 +0000 [r636734]  mklingens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemessage.h,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/chatmessagepart.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  Major optimization of the code used by formatStyleKeywords(). In
	  practice this makes the following cases a LOT faster: * Opening a
	  chat with large amounts of 'old' messages from history * Browsing
	  history in the chat window with alt-left/right * Updating the
	  chat window someone changes avatar * Updating the chat window
	  when switching style Performance improvement initially is about
	  30% due to optimized Kopete::Message::plainBody() code for
	  determining whether the message is left-to-right or not. Each
	  subsequent call on the same messages will be even about 60%
	  faster because the LTR value is cached. In practice the code is
	  unfortunately still quite slow, but the massive improvement is
	  already quite noticable. Reviewed and approved by Will, also
	  tested against the unit tests.

2007-02-25 19:05 +0000 [r637208]  mklingens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/tests/kopetemessage_test.cpp:
	  Add more testcases, otherwise <img> isn't tested and it's special
	  code

2007-02-25 20:22 +0000 [r637222]  mklingens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  Make Kopete::Message::unescape a whopping 4 times as fast by
	  eliminating QRegExp. Of the remaining cycles, about 2/3rd goes to
	  QString::fromLatin1() and ~QString(), but that can't be optimized
	  in KDE 3.5. (Moving it out of the loop had actually a negative
	  effect because not all strings are created as much.) When porting
	  to KDE 4, consider static QLatinStrings to avoid the creation of
	  those objects altogether. The other 1/3rd of time goes to the
	  block of QString::replace() calls at the bottom. Can be optimized
	  relatively trivial by using a similar loop as the new code for
	  elements, but that is *much* less readable, so I hope that won't
	  be needed for acceptable speed. Another 75% on code that just got
	  50% faster is not bad anyway ;)

2007-02-26 20:18 +0000 [r637557]  mklingens

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/tests/chatwindowstylerendering_test.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/tests/Makefile.am:
	  Make 'make check' work again. Note that none of the dates in the
	  output were as expected by the test program. Since I don't know
	  what is "correct" I blindly adjusted the test to match reality.
	  If reality is broken, please tell me :) Michael, can you forward
	  port again? CCMAIL: Michaël Larouche <larouche@kde.org>

2007-03-04 08:31 +0000 [r639055]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/plugins/netmeeting/netmeetingpreferences.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/netmeeting/netmeetinginvitation.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/netmeeting/netmeetingprefs_ui.ui:
	  Fix #142465: gnomemeeting was renamed to ekiga

2007-03-12 20:02 +0000 [r641883]  mueller

	* branches/KDE/3.5/kdenetwork/lanbrowsing/lisa/addressvalidator.cpp:
	  the usual "daily unbreak compilation"

2007-03-16 09:03 +0000 [r643048]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/icqprotocol.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/aim/aimprotocol.cpp:
	  Fix bug 143032: Kopete crashes with SIGSEGV when launched with
	  aim quasi-url Also fix the same crash in ICQ and crash when
	  account is offline. Add error messages to improve error feedback.
	  BUG: 143032

2007-03-22 00:10 +0000 [r645234]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscarcontact.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimanager.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimodifytask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimanager.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssimodifytask.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssiauthtask.cpp:
	  Update our local Server Side Info correctly. Fix authorization
	  status in ICQ. Fix bug 133950: adding users to visible/invisible
	  list does not work Please test it because I can't reproduce it.
	  BUG: 133950

2007-03-28 13:54 +0000 [r647488]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/identity/kopeteidentityconfig.cpp:
	  Backport 647486 : Don't feed the encoded URL to the
	  KURLRequester; QImage can't read it

2007-03-29 14:20 +0000 [r647786]  binner

	* branches/KDE/3.5/kdenetwork/knewsticker/eventsrc: this sound file
	  doesn't exist

2007-03-31 21:21 +0000 [r648641]  mlaurent

	* branches/KDE/3.5/kdenetwork/lanbrowsing/kcmlisa/portsettingsbar.cpp:
	  Fix signal/slot

2007-04-05 13:04 +0000 [r650799]  scripty

	* branches/KDE/3.5/kdenetwork/kopete/icons/cr22-action-delete_user.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr22-action-voicecall.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr32-action-delete_user.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr32-action-voicecall.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr16-action-delete_user.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr16-action-voicecall.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr22-action-add_user.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr32-action-add_user.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr64-action-voicecall.png,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/icons/cr16-action-yahoo_busy.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr128-action-voicecall.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr16-action-add_user.png,
	  branches/KDE/3.5/kdenetwork/kopete/icons/cr48-action-voicecall.png,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/icons/cr16-action-yahoo_mobile.png,
	  branches/KDE/3.5/kdenetwork/kopete/styles/Clear/Contents/Resources/images/icon-time.png:
	  Remove svn:executable (as it is a graphic file) (goutte)

2007-04-12 15:30 +0000 [r653053]  mlaurent

	* branches/KDE/3.5/kdenetwork/kdnssd/ioslave/dnssd.h,
	  branches/KDE/3.5/kdenetwork/kdnssd/ioslave/dnssd.cpp: Fix mem
	  leak

2007-04-21 20:14 +0000 [r656600]  scripty

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/Makefile.am: A
	  Makefile.am file should not be executable (goutte)

2007-04-27 13:32 +0000 [r658500]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahoocontact.cpp:
	  Fix linebreaks, showing up as <br/> BUG:139503

2007-05-01 15:30 +0000 [r660112]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/messagereceivertask.cpp:
	  Don't show plugin messages, because we can't handle them.

2007-05-02 16:47 +0000 [r660412]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/coreprotocol.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/listtask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/ymsgprotocol.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/listtask.h:
	  Fix loss of contacts on large contact lists. BUG:138376

2007-05-02 20:38 +0000 [r660486]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/messagereceivertask.cpp:
	  return something

2007-05-06 10:06 +0000 [r661656]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/plugins/cryptography/cryptographyplugin.cpp:
	  Revert the commit that broke the cryptography plugin in the
	  previous Kopete version BUG: 134907

2007-05-09 17:06 +0000 [r662951]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/senddcinfotask.cpp:
	  Fix bug 145199: ICQ "Hide IP address" feature not working BUG:
	  145199

2007-05-10 01:01 +0000 [r663090]  girko

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/ssiauthtask.cpp:
	  Fixed encoding handling in ICQ authorization messages. This is
	  already fixed in trunk, so this change can be considered to be
	  simplified backport of encoding fix. BUG: 145240

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

