2007-01-18 13:23 +0000 [r624936]  mueller

	* branches/KDE/3.5/kdeutils/kmilo/generic/generic_monitor.h,
	  branches/KDE/3.5/kdeutils/kmilo/generic/generic_monitor.cpp:
	  instead of going relative, which has rounding errors, go absolute
	  see http://bugzilla.novell.com/show_bug.cgi?id=234196 Patch was
	  made by Stefan Seyfried <seife at suse>

2007-02-02 09:55 +0000 [r629304]  dfaure

	* branches/KDE/3.5/kdeutils/klaptopdaemon/portable.cpp: Don't
	  output "QFile::open: no file name specified" into my
	  .xsession-errors every half second.

2007-03-15 14:32 +0000 [r642833]  mueller

	* branches/KDE/3.5/kdeutils/kmilo/thinkpad/kcmthinkpad/main.cpp,
	  branches/KDE/3.5/kdeutils/kmilo/thinkpad/kcmthinkpad/Makefile.am:
	  fix default paths for SUSE users

2007-04-05 19:02 +0000 [r650909]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalc.cpp: Put the calculator
	  display at the top of the window, so that ugly resizing is often
	  avoided.

2007-04-15 17:47 +0000 [r654281]  mkoller

	* branches/KDE/3.5/kdeutils/ark/filelistview.cpp,
	  branches/KDE/3.5/kdeutils/ark/filelistview.h: BUG: 144133 Store
	  filesize as KIO::filesize_t instead of long to correctly handle
	  files > 4GB

2007-04-15 19:29 +0000 [r654322]  mkoller

	* branches/KDE/3.5/kdeutils/ark/filelistview.cpp: BUG: 129446 Don't
	  crash with files starting with "/"

2007-04-15 20:37 +0000 [r654346]  mkoller

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp: Get rid of "Warning,
	  part has a widget ArkWidget with a focus policy of NoFocus"

2007-04-19 16:04 +0000 [r655860]  wirr

	* branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/main.cpp: FEATURE:
	  Allow themes to receive mouse wheel events on meters Approved by
	  k-c-d

2007-05-01 12:46 +0000 [r660060]  pradeepto

	* branches/KDE/3.5/kdeutils/kjots/main.cpp: Changing maintainership
	  details. Thanks Jaison.

2007-05-11 14:25 +0000 [r663513]  lunakl

	* branches/KDE/3.5/kdeutils/klaptopdaemon/laptop_daemon.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/laptop_daemon.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock.cc,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock.h: Move the
	  load average checking out of XAutoLock.

2007-05-11 14:48 +0000 [r663522]  lunakl

	* branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock_c.h (added),
	  branches/KDE/3.5/kdeutils/klaptopdaemon/laptop_daemon.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/Makefile.am,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/configure.in.in,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock.cc,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock.h,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock_diy.c (added),
	  branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock_engine.c
	  (added): Replace old XAutoLock code with newer one from KDesktop,
	  for the same reasons like this happened in KDesktop (gotta love
	  code reuse by copy&paste).

2007-05-11 14:51 +0000 [r663525]  lunakl

	* branches/KDE/3.5/kdeutils/klaptopdaemon/laptop_daemon.cpp,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock.cc,
	  branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock.h: Readd
	  XAutoLock modifications for load average checking.

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

