2006-10-17 20:42 +0000 [r596549]  cartman

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/encoderconfigimp.cpp:
	  Apply patch from Haris Kouzinopoulos <kouzinopoulos@gmail.com>
	  changing encoder names to be more user friendly. Thanks for the
	  patch! CCMAIL: kouzinopoulos@gmail.com BUG:116135

2006-10-17 21:07 +0000 [r596565]  charis

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/encodefile.ui,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/encodefileimp.h,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/encodefileimp.cpp:
	  Only enable the "Encode File" button when the user has selected a
	  file

2006-10-24 17:04 +0000 [r598797]  carewolf

	* branches/KDE/3.5/kdemultimedia/kfile-plugins/mpeg/kfile_mpeg.cpp:
	  Fix lookup of audio_rate. Possible fix for bug 119116 CCBUG:
	  119116

2006-10-31 23:16 +0000 [r600833]  charis

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/encodefile.ui,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/job.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/encoderconfigimp.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/kaudiocreator.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/encoder.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/encoderconfigimp.h,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/kaudiocreator.h:
	  Small maintenance work, mostly tab removal (they are evil)

2006-11-05 17:34 +0000 [r602314]  larkang

	* branches/KDE/3.5/kdemultimedia/libkcddb/cache.h,
	  branches/KDE/3.5/kdemultimedia/libkcddb/cache.cpp: If the DISCID
	  line contains several discids, save to all of them instead of
	  saving to "discid1,..,discidN"

2006-11-06 17:10 +0000 [r602723]  larkang

	* branches/KDE/3.5/kdemultimedia/libkcddb/cdinfodialogbase.ui:
	  Allow setting year to 0 BUG: 116136

2006-11-06 17:25 +0000 [r602728]  larkang

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/encoder.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/job.h,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/encodefileimp.cpp:
	  Replace '~' by homedir-path before doing the filename expansion
	  Don't the wav-files when using "Encode File" BUG: 115217 BUG:
	  119656

2006-11-07 13:01 +0000 [r602974]  mueller

	* branches/KDE/3.5/kdemultimedia/kmix/mdwslider.cpp: some valgrind
	  fix

2006-11-12 06:44 +0000 [r604233]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/jukservicemenu.desktop: Fix
	  the broken "Add to JuK Collection" service menu action for KDE
	  3.5. I'm not sure that it ever worked since it's calling a
	  function in the Player interface that doesn't exist, and to my
	  knowledge never existed. In the process I changed it to pass all
	  files to the dcop command line instead of calling dcop one by one
	  for each file.

2006-11-12 07:05 +0000 [r604234]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/main.cpp: Bump version of JuK
	  for KDE 3.5.6 before I forget, make the copyright symbol more
	  legal as well.

2006-11-12 15:50 +0000 [r604344]  neundorf

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp: make
	  the example for replacing parts of the string by specifyong
	  regexps actually work: " " -> "_" -> the qoutes will be removed
	  from the string after being read from the config file Alex

2006-11-12 16:51 +0000 [r604373]  neundorf

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/kcmaudiocd.cpp:
	  -enable the apply button also if the search/replace regex has
	  been modified -automatically add qoutes around these regexps if
	  required Alex

2006-11-14 16:15 +0000 [r604923]  reitelbach

	* trunk/KDE/kdemultimedia/kaudiocreator/main.cpp,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/main.cpp: fix issue
	  with l10n. The CDDB edit dialog appears untranslated on screen
	  even if the corresponding strings (which are from libkcddb) are
	  fully translated. Reason: kaudiocreator does not include the
	  i18n-catalogue "libkcddb". CCMAIL:kde-i18n-de@kde.org

2006-12-04 22:53 +0000 [r610680]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/mixertoolbox.cpp: Fixing
	  wrong master selection on start of KMix (if you have multiple
	  cards and master is not on first card). This will fix at least
	  aspects of bug 136378. CCBUGS: 136378

2006-12-04 23:53 +0000 [r610688]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/mixerIface.h,
	  branches/KDE/3.5/kdemultimedia/kmix/mixer.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/mixer.h: Adding DCOP
	  interface for muting master. It should have been added earlier
	  (when the masterVolume() DCOP calls were introduced). Also
	  neccesary, as it aids in fixing a KMilo/KMix interoperability
	  issue. CCBUGS: 134820 CCBUGS: 134604

2006-12-05 00:00 +0000 [r610691]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/kmix.cpp: Reverting the
	  change done between revison 581567 and 581568. It is probably
	  causing trouble (bug 136378). The "master shortcuts" still work
	  and follow changes. The reason for the revison 581568 patch were
	  two other bugs (KMilo not honoring master and volume shortcuts do
	  not work). These problems have been rectified now in the right
	  place.

2006-12-09 20:35 +0000 [r611870]  charles

	* branches/KDE/3.5/kdemultimedia/noatun/library/downloader.cpp:
	  Didn't I tell you that we weren't going to talk about this? But
	  you had to wind up in the wrong branch...

2006-12-15 20:47 +0000 [r613956]  pino

	* branches/KDE/3.5/kdemultimedia/kmix/kmix.desktop,
	  branches/KDE/3.5/kdemultimedia/kaudiocreator/kaudiocreator.desktop:
	  Give them proper categories.

2006-12-22 19:29 +0000 [r615802]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/mixer_alsa9.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/mixer.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/kmix.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/mixer.h,
	  branches/KDE/3.5/kdemultimedia/kmix/mixer_alsa.h: Fix for bug
	  127294: hangs linux usb detection and keeps cpu at 100% when
	  unplugging a usb audio device CCBUGS: 127294

2006-12-22 23:03 +0000 [r615856]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/version.h: Bump verson number
	  => 2.6.1 (bugfix release for KDE 3.5.6)

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

