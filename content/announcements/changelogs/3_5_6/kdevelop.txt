2006-10-10 06:17 +0000 [r594103]  rgruber

	* branches/KDE/3.5/kdevelop/parts/grepview/grepdlg.cpp: Fixed
	  broken and removed unused templates

2006-10-24 07:45 +0000 [r598621]  binner

	* branches/KDE/3.5/kdevelop/src/profileengine/editor/profileeditor.cpp,
	  branches/KDE/3.5/kdevelop/src/profileengine/editor/profileeditor.h:
	  compile better

2006-10-27 19:55 +0000 [r599602]  mutz

	* branches/KDE/3.5/kdevelop/src/generalinfowidgetbase.ui,
	  branches/KDE/3.5/kdevelop/buildtools/autotools/choosetargetdlgbase.ui,
	  branches/KDE/3.5/kdevelop/parts/documentation/docprojectconfigwidgetbase.ui,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/debuggertracingdialogbase.ui,
	  branches/KDE/3.5/kdevelop/buildtools/projectmanager/importers/qmake/projectconfigurationdlgbase.ui,
	  branches/KDE/3.5/kdevelop/buildtools/lib/widgets/makeoptionswidgetbase.ui,
	  branches/KDE/3.5/kdevelop/parts/filelist/projectviewconfigbase.ui,
	  branches/KDE/3.5/kdevelop/vcs/cvsservice/checkoutdialogbase.ui,
	  branches/KDE/3.5/kdevelop/kdevdesigner/designer/editfunctions.ui,
	  branches/KDE/3.5/kdevelop/vcs/cvsservice/diffdialogbase.ui,
	  branches/KDE/3.5/kdevelop/vcs/cvsservice/editorsdialogbase.ui,
	  branches/KDE/3.5/kdevelop/src/settingswidget.ui,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlgbase.ui,
	  branches/KDE/3.5/kdevelop/parts/scripting/scriptingglobalconfigbase.ui,
	  branches/KDE/3.5/kdevelop/languages/cpp/configproblemreporter.ui,
	  branches/KDE/3.5/kdevelop/languages/cpp/ccconfigwidgetbase.ui,
	  branches/KDE/3.5/kdevelop/editors/editor-chooser/editchooser.ui,
	  branches/KDE/3.5/kdevelop/languages/php/phpconfigwidgetbase.ui,
	  branches/KDE/3.5/kdevelop/parts/uimode/uichooser.ui,
	  branches/KDE/3.5/kdevelop/parts/filelist/projectviewprojectconfigbase.ui,
	  branches/KDE/3.5/kdevelop/parts/vcsmanager/vcsmanagerprojectconfigbase.ui:
	  fix ui files

2006-11-20 12:12 +0000 [r606420]  aclu

	* branches/kdevelop/3.4/parts/doxygen/messages.cpp,
	  branches/KDE/3.5/kdevelop/parts/doxygen/doxygenpart.cpp,
	  branches/KDE/3.5/kdevelop/parts/doxygen/README.dox,
	  branches/KDE/3.5/kdevelop/parts/doxygen/lang_cfg.h,
	  branches/KDE/3.5/kdevelop/parts/doxygen/version.cpp,
	  branches/KDE/3.5/kdevelop/parts/doxygen/doxygenconfigwidget.cpp,
	  branches/KDE/3.5/kdevelop/parts/doxygen/config.h,
	  branches/kdevelop/3.4/parts/doxygen/config.cpp: Fix Doxygen 1.5.1
	  support

2006-11-20 15:29 +0000 [r606495]  apaku

	* branches/KDE/3.5/kdevelop/parts/doxygen/config.cpp: Backport fix
	  from 3.4 branch, committing on behalf of Amilcar.

2007-01-04 16:22 +0000 [r619861]  apaku

	* branches/KDE/3.5/kdevelop/languages/java/javasupportpart.cpp: Fix
	  compilation on KDE 3.3 and older

2007-01-15 09:05 +0000 [r623683]  coolo

	* branches/KDE/3.5/kdevelop/configure.in.in: I go forward as I
	  planned, if the tag leaves unused, it's ok too

2007-01-15 09:24 +0000 [r623698]  binner

	* branches/KDE/3.5/kdevelop/kdevelop.lsm: coolo wants to release
	  this

