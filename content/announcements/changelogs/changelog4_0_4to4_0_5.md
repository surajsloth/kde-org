---
title: "KDE 4.0.5 Changelog"
hidden: true
---

  <h2>Changes in KDE 4.0.5</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_0_5/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="klauncher">klauncher</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Remove reading of X-DCOP-ServiceType from desktop files. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=156339">156339</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=155873">155873</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=805921&amp;view=rev">805921</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Workaround Qt4.4 bugs affecting form widget painting. See SVN commits <a href="http://websvn.kde.org/?rev=813596&amp;view=rev">813596</a> and <a href="http://websvn.kde.org/?rev=805438&amp;view=rev">805438</a>. </li>
        <li class="bugfix ">Fix crash when generated content requires a layer (affected pidgin.im) Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=156419">156419</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=812955&amp;view=rev">812955</a>. </li>
        <li class="bugfix ">Properly compute option names when their first child is a comment node. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=161196">161196</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=812985&amp;view=rev">812985</a>. </li>
        <li class="bugfix ">Properly return names for custom events. See SVN commits <a href="http://websvn.kde.org/?rev=806618&amp;view=rev">806618</a> and <a href="http://websvn.kde.org/?rev=809502&amp;view=rev">809502</a>. </li>
        <li class="bugfix ">Fix item() and length on DOMStyleSheetList objects. See SVN commit <a href="http://websvn.kde.org/?rev=812538&amp;view=rev">812538</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_0_5/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix CPU 100 % bug. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160826">160826</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=806202&amp;view=rev">806202</a>. </li>
      </ul>
      </div>
      <h4><a name="fish">fish</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make the fish:/ kioslave more reliable. See SVN commit <a href="http://websvn.kde.org/?rev=806786&amp;view=rev">806786</a>. </li>
      </ul>
      </div>
      <h4><a name="kcm_knotify">kcm_knotify</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Restaure correctly "No audio output". Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162088">162088</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=816333&amp;view=rev">816333</a>. </li>
      </ul>
      </div>
      <h4><a name="kdm">kdm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Use radiobuttons for the session selection menu. See SVN commit <a href="http://websvn.kde.org/?rev=807235&amp;view=rev">807235</a>. </li>
        <li class="bugfix ">Use an action group for the authentication methods. See SVN commit <a href="http://websvn.kde.org/?rev=807236&amp;view=rev">807236</a>. </li>
        <li class="bugfix ">Make the delayed reboot popup button work again. See SVN commit <a href="http://websvn.kde.org/?rev=807238&amp;view=rev">807238</a>. </li>
        <li class="bugfix ">Fix background painting with qt 4.4. See SVN commit <a href="http://websvn.kde.org/?rev=807239&amp;view=rev">807239</a>. </li>
      </ul>
      </div>
      <h4><a name="knotify">knotify</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Show the composite overlay window only before the first painting pass
actually needs to flush the output to the screen. Avoids windows temporarily disappearing during KDE startup or similar visual glitches. See SVN commit <a href="http://websvn.kde.org/?rev=806388&amp;view=rev">806388</a>. </li>
        <li class="bugfix ">Do not register with the session manager at all, it's not needed and there
may be a deadlock (noticeable when using other WM than KWin). See SVN commit <a href="http://websvn.kde.org/?rev=807026&amp;view=rev">807026</a>. </li>
      </ul>
      </div>
      <h4><a name="krdb">krdb</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">This ensures systemGtkrc doesn't get lost. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=146779">146779</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=805916&amp;view=rev">805916</a>. </li>
      </ul>
      </div>
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Brightness support in XRender compositing mode (makes Logout and DimInactive effects work). See SVN commit <a href="http://websvn.kde.org/?rev=805578&amp;view=rev">805578</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fixed memory leak with compositing enabled. See SVN commit <a href="http://websvn.kde.org/?rev=804133&amp;view=rev">804133</a>. </li>
        <li class="bugfix ">Clip properly with shaped windows with XRender compositing. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160141">160141</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=805222&amp;view=rev">805222</a>. </li>
        <li class="bugfix ">Fix XRender compositing initialization with non-truecolor depths. See SVN commit <a href="http://websvn.kde.org/?rev=805345&amp;view=rev">805345</a>. </li>
        <li class="bugfix ">Several other bugfixes in XRender compositing mode.</li>
        <li class="bugfix ">Do not try to use saturation in XRender compositing mode. See SVN commit <a href="http://websvn.kde.org/?rev=805575&amp;view=rev">805575</a>. </li>
        <li class="bugfix ">Restore color settings after painting background. Fixes splashscreen being fully black during KDE startup. See SVN commit <a href="http://websvn.kde.org/?rev=807453&amp;view=rev">807453</a>. </li>
        <li class="bugfix ">Avoid crash with the ThumbnailAside effect. See SVN commit <a href="http://websvn.kde.org/?rev=810011&amp;view=rev">810011</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_0_5/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/ktouch" name="ktouch">KTouch</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Add three new keyboard layouts: Arabic, Finnish and Swiss. See SVN commit <a href="http://websvn.kde.org/?rev=807684&amp;view=rev">807684</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_0_5/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Don't convert cover art to PNG, simply copy it as-is to disk. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=157987">157987</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=806110&amp;view=rev">806110</a>. </li>
        <li class="bugfix ">Make dragging and dropping tracks work again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162068">162068</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=807600&amp;view=rev">807600</a>. </li>
        <li class="bugfix ">Make the global shortcuts panel show up in the shortcuts dialog again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=157028">157028</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=807912&amp;view=rev">807912</a>. </li>
        <li class="bugfix ">Improved crossfading, volume transients with incoming song now much less frequent. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=161168">161168</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=809463&amp;view=rev">809463</a>. </li>
        <li class="bugfix ">Do not assume Phonon is playing immediately after playback requested.  Makes phonon-gst work on one song before crashing. See SVN commit <a href="http://websvn.kde.org/?rev=809540&amp;view=rev">809540</a>. </li>
        <li class="bugfix ">Load the correct icon for the Cover column. See SVN commit <a href="http://websvn.kde.org/?rev=810101&amp;view=rev">810101</a>. </li>
        <li class="bugfix ">Do not close JuK when clicking on the cover in the track popup window when minimized to system tray. See SVN commit <a href="http://websvn.kde.org/?rev=811011&amp;view=rev">811011</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="modulehomepage"> [ <a href="http://games.kde.org">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_0_5/kdegames.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://games.kde.org/game.php?game=kreversi" name="kreversi">kreversi</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Correctly reset score on new game start Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162667">162667</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=813606&amp;view=rev">813606</a>. </li>
        <li class="bugfix ">Start new game if needed before launching Demo Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=161326">161326</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=813618&amp;view=rev">813618</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_0_5/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not create scrolling artifacts with Qt 4.4. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=161737">161737</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=805179&amp;view=rev">805179</a>. </li>
        <li class="bugfix ">Do not consider hidden pages when getting the text within the selected rectangle. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=157927">157927</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=808367&amp;view=rev">808367</a>. </li>
        <li class="bugfix ">Open bookmarked documents even when there is no open document. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162459">162459</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=811110&amp;view=rev">811110</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_0_5/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kopete.kde.org/" name="kopete">Kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not crash when closing chat windows and "Always show tabs" is enabled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=161769">161769</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=805382&amp;view=rev">805382</a>. </li>
        <li class="bugfix ">Less confusing "Hide Offline Users" button. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160561">160561</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=806558&amp;view=rev">806558</a>. </li>
        <li class="bugfix ">Don't pretend the balloon/bubble message notifications are still there. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=159997">159997</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=808051&amp;view=rev">808051</a>. </li>
        <li class="bugfix ">Fix a crash occuring during internal message handling. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=161790">161790</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=808179&amp;view=rev">808179</a>. </li>
        <li class="bugfix ">Fix plugin loading of view plugins, prevents a crash when saving behavior configuration settings in some localized versions. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=161078">161078</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=811502&amp;view=rev">811502</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_0_5/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Successful importing keys from clipboard is no longer reported as failed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160929">160929</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=804542&amp;view=rev">804542</a>. </li>
        <li class="bugfix ">Minimize to tray works again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=158955">158955</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=805531&amp;view=rev">805531</a>. </li>
        <li class="bugfix ">Key generation popup get's hidden after error. See SVN commit <a href="http://websvn.kde.org/?rev=810046&amp;view=rev">810046</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebindings"><a name="kdebindings">kdebindings</a><span class="allsvnchanges"> [ <a href="4_0_5/kdebindings.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="pykde">PyKDE</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Added vararg, int and float argument support to the i18n() family of functions. Added the missing i18np() and i18ncp() functions. See SVN commit <a href="http://websvn.kde.org/?rev=813476&amp;view=rev">813476</a>. </li>
      </ul>
      </div>
    </div>
