------------------------------------------------------------------------
r837899 | tmcguire | 2008-07-26 01:41:47 +0200 (Sat, 26 Jul 2008) | 14 lines

Backport r837888 by tmcguire to the 4.1 branch:

Merged revisions 837809 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r837809 | tilladam | 2008-07-25 21:16:50 +0200 (Fri, 25 Jul 2008) | 4 lines
  
  A todo that has no due date cannot be overdue.
  
  kolab/issue2633
........


------------------------------------------------------------------------
r838884 | winterz | 2008-07-29 02:04:15 +0200 (Tue, 29 Jul 2008) | 5 lines

merge SVN commit 837626 by tilladam:

Make the IMAP parser a bit more robust in this area.


------------------------------------------------------------------------
r839404 | winterz | 2008-07-30 02:14:13 +0200 (Wed, 30 Jul 2008) | 7 lines

merge SVN commit 839401 by winterz:

a real fix for the crash that had djarvie and myself confused.
I always got crashes when quitting Kontact or adding a new calendar
incidence would crash korgac.


------------------------------------------------------------------------
r839457 | scripty | 2008-07-30 07:34:51 +0200 (Wed, 30 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r839801 | djarvie | 2008-07-30 22:50:27 +0200 (Wed, 30 Jul 2008) | 1 line

Krazy
------------------------------------------------------------------------
r841029 | tmcguire | 2008-08-02 15:17:17 +0200 (Sat, 02 Aug 2008) | 4 lines

Backport r840099 by tmcguire to the 4.1 branch:

Make the linklocator a lot faster by caching the emoticons theme name.

------------------------------------------------------------------------
r841308 | scripty | 2008-08-03 06:49:35 +0200 (Sun, 03 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r841539 | winterz | 2008-08-03 15:27:43 +0200 (Sun, 03 Aug 2008) | 4 lines

backport SVN commit 841535 by winterz:

Fix bug "Events running for several days aren't properly displayed"

------------------------------------------------------------------------
r841636 | winterz | 2008-08-03 20:57:09 +0200 (Sun, 03 Aug 2008) | 7 lines

backport SVN commit 841635 by winterz:

port e3
SVN commit 840684 by vkrause:

Avoid some reallocations.

------------------------------------------------------------------------
r842671 | aacid | 2008-08-05 21:02:36 +0200 (Tue, 05 Aug 2008) | 3 lines

Backport r842670
insert the libmailtransport catalog so that mailtransport users get the translations

------------------------------------------------------------------------
r843335 | winterz | 2008-08-07 01:37:53 +0200 (Thu, 07 Aug 2008) | 5 lines

backport SVN commit 843334 by winterz:

a fix in removeRemote() when removing the specified Id from the idMap.


------------------------------------------------------------------------
r844270 | scripty | 2008-08-09 08:07:40 +0200 (Sat, 09 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r844834 | dfaure | 2008-08-10 18:56:01 +0200 (Sun, 10 Aug 2008) | 2 lines

provide the same ctor arguments as KPluginFactory, so that it's possible to pass a componentName (e.g. for kstddirs, not much used in kdepim) and a catalogName (much useful; defaults to componentName)

------------------------------------------------------------------------
r844837 | dfaure | 2008-08-10 19:00:26 +0200 (Sun, 10 Aug 2008) | 2 lines

set a component name so that the right catalog is loaded

------------------------------------------------------------------------
r845949 | mikearthur | 2008-08-12 19:08:08 +0200 (Tue, 12 Aug 2008) | 2 lines

Merging rich-text fixes from r845920.

------------------------------------------------------------------------
r846411 | harald | 2008-08-13 14:43:58 +0200 (Wed, 13 Aug 2008) | 2 lines

With tmcguire's blessing, show a busy cursor while the "Check Capabilities..." is running. Make it consistent with kmail's imap and pop settings.

------------------------------------------------------------------------
r847080 | winterz | 2008-08-14 19:33:44 +0200 (Thu, 14 Aug 2008) | 6 lines

backport SVN commit 836884 by bbroeksema:

- Added modification time support for items.
- Added test for modification time support.
- Set revision of item returned by itemcreatejob to 0.

------------------------------------------------------------------------
r847082 | winterz | 2008-08-14 19:50:55 +0200 (Thu, 14 Aug 2008) | 2 lines

revertlast

------------------------------------------------------------------------
r847482 | lukas | 2008-08-15 15:07:07 +0200 (Fri, 15 Aug 2008) | 2 lines

need this for translations, pls forward port

------------------------------------------------------------------------
r848343 | vkrause | 2008-08-17 15:12:04 +0200 (Sun, 17 Aug 2008) | 2 lines

missing revert of revision 847080

------------------------------------------------------------------------
r848345 | vkrause | 2008-08-17 15:13:35 +0200 (Sun, 17 Aug 2008) | 2 lines

also build with kdesupport/akonadi from trunk

------------------------------------------------------------------------
r850102 | winterz | 2008-08-20 21:56:43 +0200 (Wed, 20 Aug 2008) | 7 lines

backport SVN commit 850101 by winterz:

a couple easy fixes I found while testing with the freeassociation libical.
these are also in the work/kdepimlibs-nocal branch, but I figured we
could use them in the meantime (or in case we don't end up using
the freeassociation version)

------------------------------------------------------------------------
r851238 | tmcguire | 2008-08-23 12:57:33 +0200 (Sat, 23 Aug 2008) | 11 lines

Backport r847414 by tmcguire from trunk to the 4.1 branch:

Remove the slave from the slavepool in case of error _before_ emitting the result, this
prevents the slave from being reused by a caller who listens to the result signal.

This fixes the "Continue Sending" button not working when there are muliple mails in the
outbox and the first one can not be sent.

Thanks to Will for reporting.


------------------------------------------------------------------------
r851239 | tmcguire | 2008-08-23 12:58:42 +0200 (Sat, 23 Aug 2008) | 7 lines

Backport r848388 by tmcguire from trunk to the 4.1 branch:

Don't confuse NTML with GSSAPI.

CCBUG: 169308


------------------------------------------------------------------------
r851240 | tmcguire | 2008-08-23 13:00:16 +0200 (Sat, 23 Aug 2008) | 20 lines

Backport r851237 by tmcguire from trunk to the 4.1 branch:

Merged revisions 850305 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

................
  r850305 | ervin | 2008-08-21 10:41:06 +0200 (Thu, 21 Aug 2008) | 10 lines
  
  Merged revisions 768539 via svnmerge from 
  svn+ssh://ervin@svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r768539 | tilladam | 2008-01-30 11:52:40 +0100 (Wed, 30 Jan 2008) | 3 lines
    
    Don't send the base64 encoded password to stdout.
    kolab/issue2433
  ........
................


------------------------------------------------------------------------
r851691 | vkrause | 2008-08-24 13:24:59 +0200 (Sun, 24 Aug 2008) | 2 lines

Don't crash on future protocol extensions.

------------------------------------------------------------------------
r853473 | winterz | 2008-08-27 21:11:12 +0200 (Wed, 27 Aug 2008) | 10 lines

backport SVN commit 853375 by winterz:

E3 port 
SVN commit 851291 by vkrause:

Only show the incidence difference if we are comparing with an older
version.

Kolab issue 2680

------------------------------------------------------------------------
