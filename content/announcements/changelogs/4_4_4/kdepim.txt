------------------------------------------------------------------------
r1121669 | smartins | 2010-05-02 14:35:48 +1200 (Sun, 02 May 2010) | 8 lines

Backport r1121650 by smartins from trunk to branch 4.4:

When setting a to-do 100% completed, use the COMPLETION_MODIFIED action, so a journal is created when setting completion through the dialogs, and not only through the to-do view.

The dialog to choose the collection appears, so i'm not closing this bug yet. It should use the same collection as the to-do's.

BUG: 219895

------------------------------------------------------------------------
r1121877 | smartins | 2010-05-03 03:55:54 +1200 (Mon, 03 May 2010) | 4 lines

Don't crash when there are no resources.

BUG: 233901

------------------------------------------------------------------------
r1122433 | mlaurent | 2010-05-04 09:33:54 +1200 (Tue, 04 May 2010) | 3 lines

Fix bug #236098 "Plugin list not updated correctly"
BUG: 236098

------------------------------------------------------------------------
r1122444 | mlaurent | 2010-05-04 09:56:54 +1200 (Tue, 04 May 2010) | 9 lines

Fix bug #235353
In new kaddressbook we can't call "kaddressbook --editor-only --uid"
We don't have this option now.
I think it will a good idea to readd it for trunk.
For the moment to avoid a messagebox I open just kaddressbook, not very good
CCMAIL: tokoe@kde.org

CCBUG: 235353

------------------------------------------------------------------------
r1122645 | mlaurent | 2010-05-04 23:57:02 +1200 (Tue, 04 May 2010) | 2 lines

We use libmessagecore too => load catalog

------------------------------------------------------------------------
r1122651 | mlaurent | 2010-05-05 00:05:36 +1200 (Wed, 05 May 2010) | 2 lines

load libkpgp catalog too

------------------------------------------------------------------------
r1123325 | winterz | 2010-05-06 08:29:32 +1200 (Thu, 06 May 2010) | 6 lines

merge SVN commit 1123306 by winterz:

When handling invitation clicks, do not put the KOrganizer window
on top of the plugin stack except in the [Check Calendar] situation.
kolab/issue3999

------------------------------------------------------------------------
r1123414 | scripty | 2010-05-06 14:05:14 +1200 (Thu, 06 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1123583 | smartins | 2010-05-06 22:12:04 +1200 (Thu, 06 May 2010) | 9 lines

Backport r1123581 from trunk to branch 4.4:

Don't create reminder when there are no alarms.
Between writting the incidence's uid to config and then reeding it again, someone could have removed the alarm.

Fixes segfault.

CCBUG: 235085

------------------------------------------------------------------------
r1123755 | mjansen | 2010-05-07 08:31:28 +1200 (Fri, 07 May 2010) | 5 lines

Make it possible to compile kdepim with kdelibs from trunk since according
to my knowledge it's still recommended for people not to keen on losing
email to use it instead of trunk.

ACKED by tsdgeos .
------------------------------------------------------------------------
r1123906 | tokoe | 2010-05-07 21:48:02 +1200 (Fri, 07 May 2010) | 2 lines

Backport of bugfix #235434

------------------------------------------------------------------------
r1125253 | scripty | 2010-05-11 14:38:42 +1200 (Tue, 11 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1125653 | winterz | 2010-05-12 09:54:59 +1200 (Wed, 12 May 2010) | 6 lines

backport SVN commit 1125482 by skelly:

Some printing fixes from Reinhold Kainhofer
REVIEW: 3789


------------------------------------------------------------------------
r1125666 | winterz | 2010-05-12 09:56:21 +1200 (Wed, 12 May 2010) | 7 lines

backport SVN commit 1125483 by skelly:

Make "Print Incidence" is now also available in the default print dialog

REVIEW: 3729


------------------------------------------------------------------------
r1125910 | tokoe | 2010-05-13 04:41:38 +1200 (Thu, 13 May 2010) | 4 lines

Add unknown email addresses correctly to the distribution list

BUG: 237361

------------------------------------------------------------------------
r1126985 | tokoe | 2010-05-15 19:34:58 +1200 (Sat, 15 May 2010) | 2 lines

Backport of bugfix for #237689

------------------------------------------------------------------------
r1127306 | tokoe | 2010-05-16 21:24:37 +1200 (Sun, 16 May 2010) | 2 lines

Backport of bugfix #237626

------------------------------------------------------------------------
r1127308 | tokoe | 2010-05-16 21:28:13 +1200 (Sun, 16 May 2010) | 2 lines

Backport of bugfix #237127

------------------------------------------------------------------------
r1127396 | mlaurent | 2010-05-17 01:34:48 +1200 (Mon, 17 May 2010) | 3 lines

Disable "finish" button when we select an readonly folder.
already done in trunk

------------------------------------------------------------------------
r1127448 | mlaurent | 2010-05-17 04:17:05 +1200 (Mon, 17 May 2010) | 2 lines

not necessary to create a local variable

------------------------------------------------------------------------
r1127589 | scripty | 2010-05-17 14:18:10 +1200 (Mon, 17 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1127639 | mlaurent | 2010-05-17 20:19:11 +1200 (Mon, 17 May 2010) | 3 lines

Change status of composer when we add an attachment. It's modified
=> when we close it we can store or not messages

------------------------------------------------------------------------
r1127642 | mlaurent | 2010-05-17 20:37:17 +1200 (Mon, 17 May 2010) | 3 lines

Fix bug #237596  mail search does not honour locale settings for the date
BUG: 237596

------------------------------------------------------------------------
r1127683 | mlaurent | 2010-05-17 22:23:23 +1200 (Mon, 17 May 2010) | 2 lines

Not necessary to allow to archive a folder which don't have content.

------------------------------------------------------------------------
r1127686 | mlaurent | 2010-05-17 22:29:07 +1200 (Mon, 17 May 2010) | 2 lines

This check is not necessary

------------------------------------------------------------------------
r1128669 | djarvie | 2010-05-20 10:17:14 +1200 (Thu, 20 May 2010) | 3 lines

Bug 237822: Fix alarm edit dialog not saving changes when invoked from alarm
message window's Edit button.

------------------------------------------------------------------------
r1128832 | mlaurent | 2010-05-21 00:47:08 +1200 (Fri, 21 May 2010) | 3 lines

Backport:
Fix enable/disable delete button

------------------------------------------------------------------------
r1128894 | djarvie | 2010-05-21 03:41:04 +1200 (Fri, 21 May 2010) | 2 lines

Fix invalid signal reports

------------------------------------------------------------------------
r1128936 | djarvie | 2010-05-21 06:17:12 +1200 (Fri, 21 May 2010) | 2 lines

Fix main window close action not working when system tray icon not shown

------------------------------------------------------------------------
r1128967 | kainhofe | 2010-05-21 08:00:47 +1200 (Fri, 21 May 2010) | 2 lines

KOrganizer's search dialog: Make result list stretchable again

------------------------------------------------------------------------
r1128985 | kainhofe | 2010-05-21 08:51:07 +1200 (Fri, 21 May 2010) | 1 line

KOrganizer's Birthdays: Fix leap year glitch when calculating bdays / anniversaries
------------------------------------------------------------------------
r1129719 | otrichet | 2010-05-24 02:22:38 +1200 (Mon, 24 May 2010) | 5 lines

backport to 4.4: Ensure that finished jobs (thus deleted job) are not use later.                                                                                                                                                             
                                                                                                                                                                                                                                             
Fix a bug where already finished job are kill() in cancelation.                                                                                                                                                                              
CCBUG: 232482

------------------------------------------------------------------------
r1129743 | otrichet | 2010-05-24 03:40:28 +1200 (Mon, 24 May 2010) | 5 lines

Force the column to sort groups by inside the group view

BUG: 232494
Group order confusing after adding new groups

------------------------------------------------------------------------
r1130665 | djarvie | 2010-05-26 11:04:59 +1200 (Wed, 26 May 2010) | 2 lines

Call QApplication::beep() before KNotification::beep() in case of delays

------------------------------------------------------------------------
