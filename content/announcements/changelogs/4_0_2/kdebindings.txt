2008-02-12 19:05 +0000 [r774211]  helio

	* branches/KDE/4.0/kdebindings/python/pykde4/cmake/modules/FindLibPython.py,
	  branches/KDE/4.0/kdebindings/smoke/qt/CMakeLists.txt: - Fix
	  proper arch detection and install ( backport from trunk )

2008-02-21 03:15 +0000 [r777649-777648]  sebsauer

	* branches/KDE/4.0/kdebindings/ruby/krossruby/rubyextension.h,
	  branches/KDE/4.0/kdebindings/ruby/krossruby/rubyscript.cpp,
	  branches/KDE/4.0/kdebindings/ruby/krossruby/rubymodule.cpp,
	  branches/KDE/4.0/kdebindings/ruby/krossruby/rubyextension.cpp:
	  backport r777533, r777628, r777631 and r777633 from trunk.

	* branches/KDE/4.0/kdebindings/python/krosspython/cxx/WrapPython.h,
	  branches/KDE/4.0/kdebindings/python/krosspython/pythonconfig.h:
	  backport r777633 from trunk

