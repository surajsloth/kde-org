---
title: "KDE 3.2 to 3.2.1 Changelog"
hidden: true
---

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.2 and 3.2.1 releases.
</p>

<h3>arts</h3><ul>
</ul>

<h3>kdelibs</h3><ul>
<li>Improve suggestions given when in the rename dialog shown when copying/moving files (<a href="http://bugs.kde.org/show_bug.cgi?id=59796">#59796</a>)
</li>
<li>Progress dialog: large files (&gt;4GB) fixes; make "keep open" work again</li>
<li>Progress dialog (all in one): don't block logging out from KDE</li>
<li>KIO: Preserve file premissions when copying from remote protocols that support listing (<a href="http://bugs.kde.org/show_bug.cgi?id=74593">#74593</a>)
</li>
<li>KIO: Give again the choice to overwrite when moving/renaming files (<a href="http://bugs.kde.org/show_bug.cgi?id=73095">#73095</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=73379">#73379</a>)
</li>
<li>KIO: Make 'overwrite' ask only once for a given dir (when moving a directory with subdirs)</li>
<li>KSystemTray: if the mainwindow is minimized or not on the current desktop, clicking will show it instead of hiding</li>
<li>KLocale: fixing support for YY aka %y (<a href="http://bugs.kde.org/show_bug.cgi?id=73693">#73693</a>)
</li>
<li>KDatePicker: fixing layout of close button</li>
<li>KatePart: fixed broken "replace from cursor"</li>
<li>KatePart: fixed some redraw problems (no artefacts while scrolling)</li>
<li>Don't let knotify start aRts if aRts is disabled in its kcontrol module.</li>
<li>kdeprint: Added check for print file names given by the rename dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=68399">#68399</a>)</li>
<li>KHTML: fix crash in parsing weird pages (<a href="http://bugs.kde.org/show_bug.cgi?id=73112">#73112</a>)</li>
<li>KHTML: fixing support for clear:both (<a href="http://bugs.kde.org/show_bug.cgi?id=68068">#68068</a>)</li>
<li>KHTML: better support for DOM Test Suite</li>
<li>KHTML: fix Heirmenus</li>
<li>KHTML: fix animated GIFs not looping (<a href="http://bugs.kde.org/show_bug.cgi?id=72953">#72953</a>)</li>
<li>KHTML: fix crash in style changes (<a href="http://bugs.kde.org/show_bug.cgi?id=73311">#73311</a>)</li>
<li>KHTML: don't recurse when printing (<a href="http://bugs.kde.org/show_bug.cgi?id=72775">#72775</a>)</li>
<li>KHTML: use content, not viewport coordinates for server side image maps (<a href="http://bugs.kde.org/show_bug.cgi?id=59701">#59701</a>)</li>
<li>KHTML: fixing overflow content (<a href="http://bugs.kde.org/show_bug.cgi?id=59701">#59701</a>)</li>
<li>KHTML: fix repainting logic to optimize for reduced X server load and minimum CPU (<a href="http://bugs.kde.org/show_bug.cgi?id=72575">#72575</a>)</li>
<li>KHTML: Reduce window.<name> to forms, images and applets, like document.<name> (<a href="http://bugs.kde.org/show_bug.cgi?id=71363">#71363</a>)</li>
<li>KHTML: restore KDE 3.1 behavior of &lt;font size=-1&gt; (<a href="http://bugs.kde.org/show_bug.cgi?id=73500">#73500</a>)</li>
<li>KHTML: update frame sizes before returning value for innerWidth/innerHeight, in case the window was just resized (<a href="http://bugs.kde.org/show_bug.cgi?id=73528">#73528</a>)</li>
<li>KHTML: return attribute names in lowercase in compatibility mode (DOM L2/L3)</li>
<li>KHTML: fixing insertRow for tables without tbody (<a href="http://bugs.kde.org/show_bug.cgi?id=74125">#74125</a>)</li>
<li>KHTML: implementing TreeWalkerImp</li>
<li>KHTML: fixing javascript events with frames (<a href="http://bugs.kde.org/show_bug.cgi?id=61467">#61467</a>)</li>
<li>KHTML: fixing attributes for repeated body elements (<a href="http://bugs.kde.org/show_bug.cgi?id=74329">#74329</a>)</li>
<li>KHTML: implement support for pageX/pageY. fix layerX/layerY (<a href="http://bugs.kde.org/show_bug.cgi?id=74718">#74718</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=70290">#70290</a>)</li>
<li>KHTML: Activate merged alternate code path for inline boxes construction/painting (<a href="http://bugs.kde.org/show_bug.cgi?id=40776">#40776</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=45548">#45548</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=56814">#56814</a>, ...)</li>
<li>KHTML: implementing incremental XML parsing</li>
<li>KHTML: fix detection of the selector (<a href="http://bugs.kde.org/show_bug.cgi?id=73214">#73214</a>)</li>
<li>KHTML: implementing CSSStyleDeclarationImpl (<a href="http://bugs.kde.org/show_bug.cgi?id=26311">#26311</a>)</li>
<li>KHTML: skip pseudo styles when updating anonymous blocks'style (<a href="http://bugs.kde.org/show_bug.cgi?id=73978">#73978</a>)</li>
<li>KHTML: fixing pure punctuation/single letter text childs (<a href="http://bugs.kde.org/show_bug.cgi?id=70546">#70546</a>)</li>
<li>KHTML: fixing user-defined stylesheets (<a href="http://bugs.kde.org/show_bug.cgi?id=69697">#69697</a>)</li>
<li>KHTML: fixing mixed block/inline content (<a href="http://bugs.kde.org/show_bug.cgi?id=73573">#73573</a>)</li>
<li>KHTML: fixing relayouting of childs with floats (<a href="http://bugs.kde.org/show_bug.cgi?id=71445">#71445</a>)</li>
<li>KHTML: respect padding and borders on cnn.com (<a href="http://bugs.kde.org/show_bug.cgi?id=65788">#65788</a>)</li>
<li>KHTML: treat xhtml transitional that way (<a href="http://bugs.kde.org/show_bug.cgi?id=76449">#76449</a>)</li>
<li>KHTML: enhanced border drawing code for dotted and dashed lines (<a href="http://bugs.kde.org/show_bug.cgi?id=62296">#62296</a>)</li>
<li>KHTML: fixing handling of &lt;nobr&gt;</li>
<li>KJS: fixed crash on NaN toString() conversion with a radix (<a href="http://bugs.kde.org/show_bug.cgi?id=75830">#75830</a>)</li>
<li>KJS: accept 'const' as a synonym for 'var' (<a href="http://bugs.kde.org/show_bug.cgi?id=73040">#73040</a>)</li>
</ul>

<h3>kdeaddons</h3><ul>
<li>Konqueror webarchiver plugin: webarchiver didn't get images for XHTML pages (<a href="http://bugs.kde.org/show_bug.cgi?id=61358">#61358</a>)</li>
<li>Konqueror webarchiver plugin: including original URL in .war archives (<a href="http://bugs.kde.org/show_bug.cgi?id=35130">#35130</a>)</li>
</ul>

<h3>kdeadmin</h3><ul>
</ul>

<h3>kdeartwork</h3><ul>
<li>Plastik: fixed toolbar repaints lagging behind window frame (<a href="http://bugs.kde.org/show_bug.cgi?id=72203">#72203</a>)</li>
<li>Plastik: fixed small tab bar rendering problem in Konqueror (<a href="http://bugs.kde.org/show_bug.cgi?id=73258">#73258</a>)</li>
<li>Plastik: fixed crash while filling table in kopete (<a href="http://bugs.kde.org/show_bug.cgi?id=74604">#74604</a>)</li>
<li>Plastik: fixed wrong menu separator line width (<a href="http://bugs.kde.org/show_bug.cgi?id=76269">#76269</a>)</li>
<li>Plastik: change visited link color so visited links can be easier distinguished from non-visited ones</li>
<li>Window decoration System++: ported to the new kwin api</li>
</ul>

<h3>kdebase</h3><ul>
<li>Konqueror: fixed "Create Folder..." not available in sidebar dirtree when starting with a profile</li>
<li>Konqueror: fixed crashes related to the "Find files" functionality</li>
<li>Konqueror: fixed case-sensitive sorting, wasn't working for most locale settings</li>
<li>Konqueror: make it possible to paste files using the mouse into listviews when no free area is left (<a href="http://bugs.kde.org/show_bug.cgi?id=68975">#68975</a>)
</li>
<li>Konqueror: make different users in smb:/ not ask password for every dir change (<a href="http://bugs.kde.org/show_bug.cgi?id=68893">#68893</a>)
</li>
<li>Konqueror: don't disable "Toggle selection" if no files are selected</li>
<li>Konqueror: don't show 'link view' checkbox when there's only one view + sidebar, and other related fixes</li>
<li>Konqueror: Fixed tab navigation for right-to-left locales (<a href="http://bugs.kde.org/show_bug.cgi?id=62755">#62755</a>)</li>
<li>Konqueror: Fixed synchronisation problem when tabs were moved (<a href="http://bugs.kde.org/show_bug.cgi?id=70432">#70432</a>)</li>
<li>Konqueror: Show newly fetched url images in tabs (<a href="http://bugs.kde.org/show_bug.cgi?id=71026">#71026</a>)</li>
<li>Konqueror: Added setting to let popups ignore "open in tab instead of window" ("[FMSettings] PopupsWithinTabs=false")</li>
<li>Konqueror: "Go/Applications" now calls "programs:/" slave</li>
<li>Konqueror: Linking checkbox did not redraw when unchecking (<a href="http://bugs.kde.org/show_bug.cgi?id=74822">#74822</a>)</li>
<li>kdeprintfax: fixed crash happening after sending the fax</li>
<li>KWin: improved detection of windows belonging together, fixing some stacking and focus stealing prevention problems</li>
<li>KWin: windows kept below others no longer affect smart placement</li>
<li>KWin: obey requests to go fullscreen even from applications with maximum size set (<a href="http://bugs.kde.org/show_bug.cgi?id=73509">#73509</a>)
</li>
<li>KWin: keeping windows in workarea is not so strict (<a href="http://bugs.kde.org/show_bug.cgi?id=74057">#74057</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=74555">#74555</a>)
</li>
<li>KWin: return focus to previously active window when hover unshaded window is shaded again (<a href="http://bugs.kde.org/show_bug.cgi?id=75302">#75302</a>)</li>
<li>Klipper: fixed problem with Klipper sometimes not reading new clipboard contents (<a href="http://bugs.kde.org/show_bug.cgi?id=59187">#59187</a>)</li>
<li>Kate: fixed problems with session restore (restoring the documents and windows now works correct)</li>
<li>khelpcenter : improve startup time for systems with info pages (<a href="http://bugs.kde.org/show_bug.cgi?id=62097">#62097</a>)
</li>
<li>Kicker: Don't start menu entry in K menu when right clicking it (<a href="http://bugs.kde.org/show_bug.cgi?id=4229">#4229</a>)</li>
<li>KControl: No default for possible destructive "Logout without Confirmation" shortcut</li>
<li>KSplash: Updated locolor splash to KDE 3.2 version (<a href="http://bugs.kde.org/show_bug.cgi?id=74919">#74919</a>)</li>
<li>Konsole: Made sending of master input to added session working (<a href="http://bugs.kde.org/show_bug.cgi?id=73695">#73695</a>)</li>
<li>Konsole: Fixed dynamic toolbar hiding initialization (<a href="http://bugs.kde.org/show_bug.cgi?id=75638">#75638</a>)</li>
<li>Konsole: Handle schemas with absolute paths (<a href="http://bugs.kde.org/show_bug.cgi?id=73997">#73997</a>)</li>
<li>Konsole: Fixed crash with --noscrollbar (<a href="http://bugs.kde.org/show_bug.cgi?id=74152">#74152</a>)</li>
<li>Konsole: Settings/keyboard entries are now sorted (<a href="http://bugs.kde.org/show_bug.cgi?id=74269">#74269</a>)</li>
<li>Konsole: Ctrl-C killed konsole window, not processes running in shell (<a href="http://bugs.kde.org/show_bug.cgi?id=73226">#73226</a>)</li>
<li>KHotKeys: Included actions with Konqueror mouse gestures.</li>
</ul>

<h3>kdebindings</h3><ul>
</ul>

<h3>kdeedu</h3><ul>
<li>KBruch: removed some warnings showing up on the shell</li>
<li>KBruch: the check task button is now the default button</li>
</ul>

<ul>
<li>KStars: Several INDI Telescope control fixes, including a crash condition</li>
<li>KStars: Patch from Leo Savernik that fixes "--enable-final" for all non-INDI code.</li>
<li>KStars: Fixed Empty category lists in Find Dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=73905">#73905</a>)

<li>KStars: Fixed "Unknown" image types in Export Image dialog (<a href="http://bugs.kde.org/show_bug.cgi?id=72788">#72788</a>)

<li>KStars: Fixed error in rendering Greek letters (<a href="http://bugs.kde.org/show_bug.cgi?id=72900">#72900</a>)

<li>KStars: Fixed bug when precessing coordinates from epoch B1950.0
</ul>

<h3>kdegames</h3><ul>
<li>KAsteroids: Fix crash when both shield and ship hit powerup at same time. (<a href="http://bugs.kde.org/show_bug.cgi?id=69324">#69324</a>)
</li>
<li>KAtomix: Fix molecule in level 68</li>
<li>KMahjongg: Restore removed tiles when loading a saved game (<a href="http://bugs.kde.org/show_bug.cgi?id=73944">#73944</a>)
</li>
<li>KWin4: Fix alpha issues in images with Qt 3.3 (<a href="http://bugs.kde.org/show_bug.cgi?id=71317">#71317</a>)
</li>
<li>Compilation fixes on Ktron, KPatience and libksirtet.</li>
</ul>

<h3>kdegraphics</h3><ul>
<li>KSVG: fixed crash when creating a new Konqueror tab while viewing an SVG file (<a href="http://bugs.kde.org/show_bug.cgi?id=74844">#74844</a>)</li>
</ul>

<h3>kdemultimedia</h3><ul>
<li>KMix: Properly save volumes on exit so volumes are correctly restored on next login.</li>
<li>Noatun: remember last equalizer setting.</li>
<li>KAudioCreator: Fixed broken relitive path option.</li>
<li>KAudioCreator: Fixed cursor flickering.</li>
</ul>

<h3>kdenetwork</h3><ul>
<li>KNewsTicker: Removed BSDtoday from list, fixed addresses of SecurityFocus and Freshports</li>
<li>Kopete: disable send button when account goes offline while a chat is open (<a href="http://bugs.kde.org/show_bug.cgi?id=74562">#74562</a>)
</li>
<li>Kopete: allow renaming of groups inline, like we already do for contact (<a href="http://bugs.kde.org/show_bug.cgi?id=71321">#71321</a>)
</li>
<li>Kopete: fixed redraw bug in contactlist when scrollbar disappears (<a href="http://bugs.kde.org/show_bug.cgi?id=69121">#69121</a>)
</li>
<li>Kopete: rearrange icq account dialog to make it fit onto 800x600 (<a href="http://bugs.kde.org/show_bug.cgi?id=74238">#74238</a>)
</li>
<li>Kopete: don't add contacts twice to contaclist (<a href="http://bugs.kde.org/show_bug.cgi?id=62607">#62607</a>)
</li>
<li>Kopete: fixed "Add Contact Wizard" forgetting contactname and KAB contact when going backwards (#75128)</li>
<li>Kopete: choose preferred contact by online status for starting a chat (<a href="http://bugs.kde.org/show_bug.cgi?id=75054">#75054</a>)
</li>
<li>Kopete: don't parse links inside HTML tags (<a href="http://bugs.kde.org/show_bug.cgi?id=65909">#65909</a>)
</li>
<li>Kopete: stop jabber icon animation when connecting failed</li>
<li>Kopete: properly exit Kopete when chatwindows are open on exit (<a href="http://bugs.kde.org/show_bug.cgi?id=71657">#71657</a>)
</li>
<li>Kopete: fix several possiblities where icq/aim accounts are show offline although they are online (<a href="http://bugs.kde.org/show_bug.cgi?id=61223">#61223</a>)
</li>
<li>Kopete: don't get disconnected on sending long AIM messages (<a href="http://bugs.kde.org/show_bug.cgi?id=71195">#71195</a>)
</li>
<li>Kopete: fixed crashes when deleting contacts already deleted from the contactlist (#74950)</li>
<li>Kopete: fix crash on exit caused by webpresence plugin (<a href="http://bugs.kde.org/show_bug.cgi?id=73677">#73677</a>)
</li>
<li>Kopete: don't show kopete balloon on incoming messages while being away (<a href="http://bugs.kde.org/show_bug.cgi?id=73392">#73392</a>)
</li>
<li>Kopete: add support for timestamps including date and timestamps with no date if it's a message from today (<a href="http://bugs.kde.org/show_bug.cgi?id=71818">#71818</a>)</li>
<li>WiFi: improved ./configure behaviour which kept some distributions from including kdenetwork/wifi in their binary packages</li>
<li>KWiFiManager: system tray remains open when main app is closed or minimized</li>
</ul>

<a name="pim"><h3>kdepim</h3></a>

<ul>
<li>KAlarm: Fix freeze at login</li>
<li>KAlarm: Fix alarms being missed at login</li>
<li>KAlarm: Fix memory leaks</li>
<li>KAlarm: Fix errors saving the expired alarms calendar</li>
<li>KAlarm: Fix scheduleCommand() and scheduleEmail() DCOP handling</li>
<li>KAlarm: Prevent email alarms from being sent if no 'From' address is configured (<a href="http://bugs.kde.org/show_bug.cgi?id=74525">#74525</a>)</li>
<li>KAlarm: Omit 'Bcc' when sending email alarms if no 'Bcc' address is configured</li>
<li>KAlarm: Sound file chooser dialog now shows all audio file types</li>
<li>KMail: Don't save messages to disk with world-readable permissions set by default.</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=69418">#69418</a> (filters don't work anymore as in 3.1.4)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=73307">#73307</a> (Expire on exit does not work if confirmation is enabled)</li>
<li>KMail: Fix 'Copy Link Location' in a detached reader.</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=73310">#73310</a> (sending a new mail while forgetting the receiver and/or the subject the composing window is minimized and hidden)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=73602">#73602</a> (Message body lines starting with &quot;From &quot; are incorrectly parsed as message seperator in mbox folders)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=72808">#72808</a> (reply to myself is broken / empty To: address field)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=62919">#62919</a> (reply all should not use From when ReplyTo is set (except for mailing list messages))</li>
<li>KMail: If one uses reply-all for replying to a message from oneself then now the first other recipient is added to To:; previously To: was empty because the own address (which was the sender address) is removed from the list of recipients and all other recipients were listed in the Cc: header.</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=73527">#73527</a> (filter-Window is switch in the background when &quot;rename filter&quot;-Dialog appears. (just within Kontact))</li>
<li>KMail: Correct the sizes shown in the message structure viewer for IMAP messages.</li>
<li>KMail: Fix critical bug <a href="http://bugs.kde.org/show_bug.cgi?id=71866">#71866</a> (conversion of pop filter settings dangerous)</li>
<li>KMail: Fix critical bug which caused whole IMAP folders to be deleted when messages were filtered through spamc or other external applications.</li>
<li>KMail: Fix a crash during startup with invalid IMAP folders.</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=74272">#74272</a> (Cannot subscribe to cyrus imap &quot;sharedprefix&quot; folders)</li>
<li>KMail: Don't activate reader window on kmail --check.</li>
<li>KMail: Fix saving of multipart/signed message parts inside multipart/mixed message parts.</li>
<li>KMail: Don't disable sign/encrypt if we're using S/MIME and no OpenPGP key has been specified for this identity.</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=74591">#74591</a> (kmail crashes after retrieving imap folders)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=74380">#74380</a> (Change of the account symbolic name caused loss of the sent and drafts folders)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=70558">#70558</a> (IDN domains are shown ACE-encoded in the statusbar)</li>
<li>KMail: Fix problems with changed sort order after arrival of new mail or closing config dialog.</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=75024">#75024</a> (search for <new> or <unread> also returns read emails)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=70459">#70459</a> (Keyboard shortcuts for Forward, etc. don't work in Kontact after another component has been selected)</li>
<li>KMail: Fix signature creation time validation. Visible bug was that for signatures made in January, the printing of the creation time was suppressed.</li>
<li>KMail: Fix passive popup placement and taskbar flashing of new mail notification in Kontact (<a href="http://bugs.kde.org/show_bug.cgi?id=67017">#67017</a>)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=75983">#75983</a> (moved IMAP messages lose attachments)</li>
<li>KNode: Prevent unnecessary wallet openings when closing KNode (<a href="http://bugs.kde.org/show_bug.cgi?id=73937">#73937</a>)</li>
</ul>

<h3>kdesdk</h3><ul>
<li>Umbrello: code generation does not ignore output directory anymore (<a href="http://bugs.kde.org/show_bug.cgi?id=73042">#73042</a>)</li>
<li>Umbrello: creating new states from context menu in state diagram is not
broken anymore (<a href="http://bugs.kde.org/show_bug.cgi?id=#73277">73277</a>)</li>
<li>Umbrello: changing the name of a state doesn't cause it to be drawn in bold
anymore (<a href="http://bugs.kde.org/show_bug.cgi?id=#73278">73278</a>)</li>
<li>Umbrello: not unable to change the specification of an operation anymore
(<a href="http://bugs.kde.org/show_bug.cgi?id=#73926">73926</a>)</li>
<li>Umbrello: fixed problems while moving floating text block of role belonging
to association</li>
<li>Umbrello: fixed a lot of internal problems in code generation, association
handling</li>
<li>Umbrello: fixed XMI loading</li>
<li>Cervisia: prevent crash when activating the create tag/branch action while embedded into Quanta (<a href="http://bugs.kde.org/show_bug.cgi?id=70936">#70936</a>)</li>
</ul>

<h3>kdetoys</h3><ul>
<li>KWeather: updated / corrected list of weather stations (<a href="http://bugs.kde.org/show_bug.cgi?id=70746">#70746</a>)</li>
</ul>

<h3>kdeutils</h3><ul>
<li>KCalc:  "&lt;" and "&gt;" keypress on some systems wrong (<a href="http://bugs.kde.org/show_bug.cgi?id=75555">#75555</a>)</li>
<li>KCalc:  Pasting empty clipboard made kcalc crash (<a href="http://bugs.kde.org/show_bug.cgi?id=73437">#73437</a>)</li>
<li>KCalc:  Fix paste-function: Pasting e.g. "123  \n" did not work because of the trailing spaces and CarriageReturn.</li>
<li>KGpg: Fixed popup never disappearing when wrong passphrase entered (<a href="http://bugs.kde.org/show_bug.cgi?id=73913">#73913</a>)
</li>
<li>KGpg: Fixed wrong extension for temp files in folder archiving (<a href="http://bugs.kde.org/show_bug.cgi?id=74077">#74077</a>)
</li>
<li>KGpg: Fixed Photo size not translated (<a href="http://bugs.kde.org/show_bug.cgi?id=74333">#74333</a>)
</li>
<li>KGpg: Enable auto import of missing keys when using a non english language (<a href="http://bugs.kde.org/show_bug.cgi?id=74873">#74873</a>)
</li>
<li>KGpg: Comments exported inside public key should be fixed (<a href="http://bugs.kde.org/show_bug.cgi?id=75440">#75440</a>)
</li>
<li>KGpg: Fix focus problems causing dialogs to appear behind konqueror (<a href="http://bugs.kde.org/show_bug.cgi?id=69519">#69519</a>)
</li>
<li>KGpg: Re-added Keyserver dialog in the applet popup menu (<a href="http://bugs.kde.org/show_bug.cgi?id=75400">#75400</a>)
</li>
<li>KGpg: Save editor's window size (<a href="http://bugs.kde.org/show_bug.cgi?id=74377">#74377</a>)
</li>
<li>KGpg: Fix import of keys dropped into the editor</li>
<li>KBytesEdit part: Fixed support for mousedriven wordwise selection</li>
<li>KBytesEdit part: Fixed internal drag'n'drop</li>
</ul>

<h3>quanta</h3><ul>
<li>VPL: fix editing in the attribute editor of tags which contain special areas</li>
<li>VPL: show the parent tag in the tag attribute view when editing text</li>
<li>VPL: put messageBoxes for cut/copy/paste/undo/redo instead of disabling the actions (was confusing) (<a href="http://bugs.kde.org/show_bug.cgi?id=73366">#73366</a>)</li>
<li>VPL: stop inserting non-breaking space everywhere (<a href="http://bugs.kde.org/show_bug.cgi?id=72535">#72535</a>)</li>
<li>VPL: fix some various crashes (<a href="http://bugs.kde.org/show_bug.cgi?id=72532">#72532</a>)</li>
<li>fix the slow typing problem (<a href="http://bugs.kde.org/show_bug.cgi?id=63000">#63000</a> and its duplicates)</li>
<li>don't crash on upload if the current tab holds a plugin (<a href="http://bugs.kde.org/show_bug.cgi?id=72912">#72912</a>)</li>
<li>don't crash the CSS editor on invalid CSS</li>
<li>don't crash the table editor on invalid nested tables</li>
<li>don't crash the table editor when invoked on a table without &lt;tbody&gt;</li>
<li>don't crash when setting table/body/header/footer attributes for newly created tables (<a href="http://bugs.kde.org/show_bug.cgi?id=74949">#74949</a>)</li>
<li>don't crash when trying to drag the No Project text in the project tree</li>
<li>fix crash when clicking on an item of the structure tree which pointed to an included file</li>
<li>increase timeout for network operations to 60s (<a href="http://bugs.kde.org/show_bug.cgi?id=73173">#73173</a>)</li>
<li>don't try to copy the same file twice if it's dropped to the templates tree view</li>
<li>don't let the attribute tree to grow if the tag name is very long</li>
<li>make insertion and renaming in the project tree view work as expected</li>
<li>improve the usability and behavior of the different treeviews</li>
<li>fix opening of documentation pages with references (<a href="http://bugs.kde.org/show_bug.cgi?id=70345">#70345</a>)</li>
<li>always enable the Open and Open Project buttons</li>
<li>creation of templates directory failed in some cases when using the new project wizard</li>
<li>fix creation of project when the main directory starts with a protocol name</li>
<li>fix creation of remote projects (<a href="http://bugs.kde.org/show_bug.cgi?id=73172">#73172</a>)</li>
<li>fix saving of files with fish:// (<a href="http://bugs.kde.org/show_bug.cgi?id=74716">#74716</a>)</li>
<li>make the spellchecker actually replace the wrongly spelled words (<a href="http://bugs.kde.org/show_bug.cgi?id=75106">#75106</a>)</li>
<li>make the New Project wizard usable on lower resolutions (<a href="http://bugs.kde.org/show_bug.cgi?id=75192">#75192</a>)</li>
<li>don't switch to the parent node when clicking on a node in the structure tree and Follow Cursor is enabled (<a href="http://bugs.kde.org/show_bug.cgi?id=75332">#75332</a>)</li>
<li>faster startup and less memory used</li>
<li>better namespace support in autocompletion</li>
<li>various parsing fixes</li>

</ul>

<h3>kdevelop</h3><ul>
<li>IMPROVED - Much improved KDevelop User Manual.</li>
<li>IMPROVED - C++ parser twice as fast and more memory efficient.</li>
<li>IMPROVED - Faster/lighter configuration dialogs - the heaviest pages are now demand-loaded.</li>
<li>IMPROVED - FileGroups plugin now smarter at applying regexps - much faster for common group definitions (<a href="http://bugs.kde.org/show_bug.cgi?id=72831">#72831</a>)</li>
<li>IMPROVED - Removed performance bottleneck that made the initial parsing of a project very slow. (refer to <a href="http://bugs.kde.org/show_bug.cgi?id=73671">#73671</a>)</li>
<li>IMPROVED - KCModule Template to use KDE 3.0 API and KGenericFactory.</li>
<li>IMPROVED - Make source files generated with appwizard honor the selected file templates.</li>
<li>IMPROVED - Clanlib documentation</li>
<li>IMPROVED - Project templates by ignoring irrelevant documentation and other small fixes.</li>
<li>IMPROVED - Configuration script now allows to disable the compilation of certain KDevelop parts.</li>

<li>ADDED    - A new "Simple Designer based KDE application" template was added.</li>
<li>ADDED    - SearchText in QEditor</li>
<li>ADDED    - Possibility to change fontsize in documentation browser.</li>

<li>FIXED    - The infamous "Gentoo-crash" finally found and fixed. (<a href="http://bugs.kde.org/show_bug.cgi?id=73346">#73346</a>)</li>
<li>FIXED    - Make Haskell language support work again</li>
<li>FIXED    - Debugger plugin: "Clear all breakpoints"</li>
<li>FIXED    - Grep plugin: Don't sometimes crash when interrupting a find while a build is running.</li>
<li>FIXED    - Grep plugin: Don't confuse files with very similar filenames and avoid potentially slow O(n) lookup.</li>
<li>FIXED    - Grep plugin: Don't show garbage hits - clean up after a previous interrupted run.</li>
<li>FIXED    - A case where unsaved files aren't queried for saving when KDevelop is closed.</li>
<li>FIXED    - A case where a running program that produced a lot of debug output could crash KDevelop (<a href="http://bugs.kde.org/show_bug.cgi?id=67217">#67217</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=71858">#71858</a>) </li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=58999">#58999</a> : Debugger will not start (libtool issue)</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=62888">#62888</a> : Back and forward buttons available in context menu.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=69035">#69035</a> : Run Options not honored (custom makefiles)</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=70175">#70175</a> : Problems on minimum wxWindows version test.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=70176">#70176</a> : wxWindows apptemplate broke compilation with Unicode compiled wxWindows</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=71003">#71003</a> : Cannot translate project due to srcdir != builddir.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=72502">#72502</a> : Kdevelop crashes when closing multiple files via Keyboard</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=72523">#72523</a> : Grep dialog doesn't prepopulate with selected text.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=72552">#72552</a> : Unsaved changes icon disappears even if save could not be performed.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=72582">#72582</a> : Crashes while loading plugins after unloading plugins.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=73025">#73025</a> : expand text (ctrl+J) crashes kdevelop.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=73039">#73039</a> : cvs tools may crash kdevelop.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=73045">#73045</a> : switching docks when no file is loaded crash kdevelop.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=73116">#73116</a> : Kdevelop crashes when typing a dot at the end of a comment.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=73467">#73467</a> : Incomplete make clean for parts/appwizard/common.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=73567">#73567</a> : Use -u instead of -u3 since it is obsoleted.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=73672">#73672</a> : kdevelop looks weird after previewing forms in KUIViewer.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=73716">#73716</a> : close all in context menu with two changed documents doesn't respond to cancel properly.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=74955">#74955</a> : Crash when working with Java ant projects.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=74650">#74650</a> : Drop-down lists of toolbar combo views don't appear with UI effects on.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=74712">#74712</a> : Automake 1.8.x not supported.</li>
<li>FIXED    - <a href="http://bugs.kde.org/show_bug.cgi?id=74585">#74585</a> : Unable to debug Hello World C++ program.</li>
<li>FIXED    - Lots of other small things we've forgotten...</li>
</ul>
