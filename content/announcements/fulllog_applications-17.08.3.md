---
title: KDE Applications 17.08.3 Full Log Page
type: fulllog
version: 17.08.3
hidden: true
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Akonaditest: port "app.disableSessionManagement" to Qt5. <a href='http://commits.kde.org/akonadi/86f2e800a78388f3c94f20934df4e11537619662'>Commit.</a> </li>
<li>ItemSync: speed up by not using takeFirst(). <a href='http://commits.kde.org/akonadi/033667a64ff06c39c1643f8976795ba57ac4b498'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Hide]</a></h3>
<ul id='ulakonadi-contacts' style='display: block'>
<li>Generate translated string with i18n. <a href='http://commits.kde.org/akonadi-contacts/3b57bd769395cfa79093b9800ffbf55d47c68453'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>QDateTime::toSecsSinceEpoch() -> toMSecsSinceEpoch(). <a href='http://commits.kde.org/ark/d688d04b7ef0dd6583483a6cafae4a5c256df771'>Commit.</a> </li>
<li>Libzip: Preserve modtime when extracting. <a href='http://commits.kde.org/ark/b99bf9620f124f75e32f4e9de2b90f58cbffc611'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380225'>#380225</a></li>
<li>Improve layout of internal previewer. <a href='http://commits.kde.org/ark/f78dc3357284e8d03877df9ce08a2794295caf44'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385710'>#385710</a></li>
<li>Increase max size of archive volumes. <a href='http://commits.kde.org/ark/0c4f5b6202022ddf8f33bdc642c69503cdf26ff3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384869'>#384869</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix padding of "Fit Width" button. <a href='http://commits.kde.org/gwenview/7389aa55dbdf9c8af95106498aa6ce082cc03eb2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385035'>#385035</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Set a proper desktop file name to fix an icon under Wayland. <a href='http://commits.kde.org/kdenlive/33931eee9180d1e2ac3cb8458680061654d8e929'>Commit.</a> </li>
<li>Sort clip zones by position instead of name. <a href='http://commits.kde.org/kdenlive/3f9180b5c092cabcae48afcf698bdd8c829fe61a'>Commit.</a> </li>
<li>Fix melt.exe finding on windows. <a href='http://commits.kde.org/kdenlive/164f9fe0a665a85a5c8b41318dfa58290d8e7f6b'>Commit.</a> </li>
<li>Revert "Windows: terminate KDE session on window close". <a href='http://commits.kde.org/kdenlive/0a12e90cb55e27be72109629dd3836873ad4cca2'>Commit.</a> </li>
<li>Make KCrash optional. <a href='http://commits.kde.org/kdenlive/04ee4fa9259dcaa2d18a499a40c5192f0f5f96d9'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>We need to load value not saving it. <a href='http://commits.kde.org/kdepim-addons/34ff02e3762c7a0e08dc7df79fae8b79347e8e17'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>IMAP: add folder name in error message about select. <a href='http://commits.kde.org/kdepim-runtime/82001e7a2dd793429c0d61dfbb1ae97e282a9f8a'>Commit.</a> </li>
</ul>
<h3><a name='kdialog' href='https://cgit.kde.org/kdialog.git'>kdialog</a> <a href='#kdialog' onclick='toggle("ulkdialog", this)'>[Hide]</a></h3>
<ul id='ulkdialog' style='display: block'>
<li>Call KLocalizedString::setApplicationDomain only after QApp creation. <a href='http://commits.kde.org/kdialog/99f1626ba3a1190acc472f4307527d4db41d53b8'>Commit.</a> </li>
<li>Call KAboutData::setApplicationData earlier. <a href='http://commits.kde.org/kdialog/c7e1290e43608497a712acc686a9a2618b752170'>Commit.</a> </li>
<li>Call KLocalizedString::setApplicationDomain only after QApp creation. <a href='http://commits.kde.org/kdialog/4a8ed1b2340daf62fe64b9ae8159e8666e1cc575'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Hide]</a></h3>
<ul id='ulkgpg' style='display: block'>
<li>Fix message after importing keys. <a href='http://commits.kde.org/kgpg/4b0b69868513f101c401c04d04c06a30d5a6c713'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368950'>#368950</a></li>
<li>Do not show message when password changing ends successfully. <a href='http://commits.kde.org/kgpg/b0cbbdd314a0a1eed3cc5b6c6ff9ad433cfdc2d0'>Commit.</a> See bug <a href='https://bugs.kde.org/383958'>#383958</a></li>
<li>Do not flag sequence errors when using pinentry for changing passwords. <a href='http://commits.kde.org/kgpg/9b88ec6a7b9048476abb98076c96f63cc3d6e38f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/383958'>#383958</a></li>
<li>Make PINENTRY_LAUNCHED a hint that can be triggered on. <a href='http://commits.kde.org/kgpg/7c2acfd3acc7c460ee797319bb6e51b136649b00'>Commit.</a> </li>
<li>Fix output filename constructions for files passed on the commandline. <a href='http://commits.kde.org/kgpg/89ef50ccf93d98680c7f00bc9412b7774e6ca1a1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385437'>#385437</a></li>
</ul>
<h3><a name='kholidays' href='https://cgit.kde.org/kholidays.git'>kholidays</a> <a href='#kholidays' onclick='toggle("ulkholidays", this)'>[Hide]</a></h3>
<ul id='ulkholidays' style='display: block'>
<li>Russia: Internal Troops Day renamed. <a href='http://commits.kde.org/kholidays/62bf3b981803f8b326138803926342893686d087'>Commit.</a> </li>
<li>Holidays/plan2/holiday_kr_ko - Added Korean lunar holiday entries until 2037/12/31. <a href='http://commits.kde.org/kholidays/08c5c6be92b4af471e307b53506c8f7a95a8ef80'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Workaround incorrectly returned EEXIST instead of EPERM regression introduced by libsmbclient 4.7. <a href='http://commits.kde.org/kio-extras/5ddb81c5ec7d8907ca4a8be20499fa40e75be861'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385708'>#385708</a></li>
<li>Fix smb:/ handling. <a href='http://commits.kde.org/kio-extras/859ed4f192ef3ab5d723bfb733d4a40d1e94fc68'>Commit.</a> </li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Hide]</a></h3>
<ul id='ulkleopatra' style='display: block'>
<li>Fix S/MIME sign+encrypt. <a href='http://commits.kde.org/kleopatra/f365138b34e3d52e7047f6886b48356e4ae2d8de'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Fix bug when welcome page is displayed. <a href='http://commits.kde.org/kmail/76137dffa1b055971fbb366621e3275aed73fc03'>Commit.</a> </li>
<li>Display  warning on top of composer + fix special case. <a href='http://commits.kde.org/kmail/bfc2979670f1e57fa69e8723e6e6a9838c2e0258'>Commit.</a> </li>
<li>Increase dependancy. <a href='http://commits.kde.org/kmail/d77ffafd653d07dfdd3a60f7b987cc81324cb519'>Commit.</a> </li>
<li>Fix show label. <a href='http://commits.kde.org/kmail/93f81ea7a76be0543854dc722ebf43d1259788a1'>Commit.</a> </li>
</ul>
<h3><a name='kmailtransport' href='https://cgit.kde.org/kmailtransport.git'>kmailtransport</a> <a href='#kmailtransport' onclick='toggle("ulkmailtransport", this)'>[Hide]</a></h3>
<ul id='ulkmailtransport' style='display: block'>
<li>Fix generated translated string. <a href='http://commits.kde.org/kmailtransport/3564c3e70ad048b9b9588e536a566e7c4c40683e'>Commit.</a> </li>
</ul>
<h3><a name='knotes' href='https://cgit.kde.org/knotes.git'>knotes</a> <a href='#knotes' onclick='toggle("ulknotes", this)'>[Hide]</a></h3>
<ul id='ulknotes' style='display: block'>
<li>Fix crash on startup on Wayland. <a href='http://commits.kde.org/knotes/f603ffb669f58da5e5ef4a8c1ef0d5785ed2ec90'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/370750'>#370750</a></li>
</ul>
<h3><a name='kwave' href='https://cgit.kde.org/kwave.git'>kwave</a> <a href='#kwave' onclick='toggle("ulkwave", this)'>[Hide]</a></h3>
<ul id='ulkwave' style='display: block'>
<li>Fix crash in pulseaudio playback. <a href='http://commits.kde.org/kwave/649f8cbc569cd2a3d687a02ea560eb57c230fb74'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Call KLocalizedString::setApplicationDomain only after QApp creation. <a href='http://commits.kde.org/lokalize/1ea084b8b5e223b91f42f04869091b3317dda369'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Don't export "account name" when we export filter as sieve script. <a href='http://commits.kde.org/mailcommon/e79c281ecc53bc5a558d25849010a20609ef6083'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Fix Bug 386368 - Links in HTML emails broken by escaped ampersands. <a href='http://commits.kde.org/messagelib/2ed33b9fcd46334b4c7867a2f3a9e34e57282e8d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386368'>#386368</a></li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Hide]</a></h3>
<ul id='ulokteta' style='display: block'>
<li>Structures settings: Fix defunct structure plugin enabling/disabling. <a href='http://commits.kde.org/okteta/a6011caf025b9f4b4dee045148badbb1a9760e8a'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Fix crash on rotation jobs. <a href='http://commits.kde.org/okular/662fa69a2dcc0c1403b1773262368943d5cacd52'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382725'>#382725</a></li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Fix crash in X11ImageGrabber::getDrawableGeometry. <a href='http://commits.kde.org/spectacle/74bba5fd94c562ce4517dea0b58d4732499ad180'>Commit.</a> </li>
</ul>


