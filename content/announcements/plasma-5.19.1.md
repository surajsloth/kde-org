---
title: KDE Plasma 5.19.1, bugfix Release for June
release: "plasma-5.19.1"
version: "5.19.1"
description: KDE Ships Plasma 5.19.1
date: 2020-06-16
layout: plasma
changelog: plasma-5.19.0-5.19.1-changelog
---

{{% plasma-5-19-video %}}

Tuesday, 16 June 2020.

{{% i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.19.1." %}}

{{% i18n_var "<a href='https://kde.org/announcements/plasma-%[1]s.0'>Plasma %[1]s</a> was released in June 2020 with many feature refinements and new modules to complete the desktop experience." "5.19" %}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Dr Konqi: Map neon in platform guessing. <a href="https://commits.kde.org/drkonqi/4f0544a2e92a9c01a5ba58a01e9c268318dab31f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/422556">#422556</a>
- Battery applet not showing up in tray. <a href="https://commits.kde.org/plasma-workspace/1c3dc48629d1b27df93244147f0a52a65dd8c0c9">Commit.</a>
- Fix confirmLogout setting for SessionManagement. <a href="https://commits.kde.org/plasma-workspace/d49e0a406857287658f205ed8b0aaf8bf31dbb80">Commit.</a>
