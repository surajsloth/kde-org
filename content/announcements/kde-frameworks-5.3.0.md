---
title: Release of KDE Frameworks 5.3.0
date: "2014-10-07"
description: KDE Ships Frameworks 5.3.0.
---

{{<figure src="/announcements/frameworks5TP/KDE_QT.jpg" class="text-center" caption="Collaboration between Qt and KDE" >}}

October 07, 2014. KDE today announces the release of KDE Frameworks 5.3.0.

KDE Frameworks are 60 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see <a href='http://kde.org/announcements/kde-frameworks-5.0.php'>the Frameworks 5.0 release announcement</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

### KActivities

{{< stop-translate >}}

- Added DBus interface to load plugins at runtime

### KArchive

- Add convenience method KArchive::file()
- Compilation fixes for MSVC

### KBookmarks

- Fix encoding issue in KIO places dialog (bug 337642)

### KCMUtils

- Fix initial size of KCMultiDialog (bugs 337494, 325897)

### KCompletion

- Fixed size hint and positioning of clear button in highDpi mode

### KConfig

- KConfigLoader: fix sharing of KSharedConfig objects

### KConfigWidgets

- Now provides the kf5_entry.desktop files it needs for KLanguageButton

### KCoreAddons

- Kdelibs4Migration: allow distributions to set KDE4_DEFAULT_HOME_POSTFIX
  so that the kde4 home is found properly.
- Compilation fixes for MSVC and gcc 4.5
- Turn KFormat into a Q_GADGET so we can expose its properties to QML indirectly

### KEmoticons

- Add unicode characters to Glass emoticon theme

### KGuiAddons

- Make KFontUtils::adaptFontSize be a bit more exact

### KI18n

- Remove leftover Perl dependency

### KIO

- Now includes kio_trash
- Add new KIO job, KIO::fileSystemFreeSpace, that allows you to get a filesystem's total and available space.
- kshorturifilter: Remove redundant forward slashes from the beginning of an URI
- Add searchprovider definitions for the qwant search engine
- File dialog: fix relative paths being turned into HTTP URLs
- Fix thumbnails for mimetype groups.

### KJS

- Implement Math.Clz32
- U+0000 through U+001F is not allowed as char, but as escaped unicode sequence (bug 338970)

### KNotifications

- Avoid infinite recursion in KStatusNotifierItem / QSystemTray.

### KService

- Many many fixes to KPluginInfo
- Add functions to convert between lists KPluginMetaData and KPluginInfo

### KTextEditor

- Multiple memory leaks fixed
- Avoid auto-completion to interfere with search/replace text (bug 339130), and more autocompletion fixes
- Many fixes to the VIM mode

### KWidgetAddons

- KPageListView: fixes for high-dpi mode
- KPageWidget: margin fixes

### KWindowSystem

- NETWinInfo now provides convenient wrapper for WM_PROTOCOLS.
- NETWinInfo now provides convenient wrapper for input and urgency hints of WM_HINTS property.

### Solid

- New freedesktop backend, not yet default but due to replace the upower/udev/systemd backends

### Extra-cmake-modules

- New module ECMGeneratePkgConfigFile, for frameworks to install a pkgconfig file.
- New option ECM_ENABLE_SANITIZERS, to enable clang sanitizers. Example: ECM_ENABLE_SANITIZERS='address;undefined'
- New option BUILD_COVERAGE, to enable code coverage.

### Frameworkintegration

- Fixed for compilation with Qt 5.4
- Fixed a few standard shortcuts

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.

