---
title: KDE 3.1 Release Announcement
date: "2003-01-28"
description: KDE Project Ships Major Feature Upgrade of Third- Generation GNU/Linux - UNIX Desktop, Offering Enterprises and Governments a Compelling Free and Open Desktop Solution
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Major Feature Release of Leading Open Source Desktop,
   Improves Enterprise Usability
</h3>

<p align="justify">
  
KDE Project Ships Major Feature Upgrade of Third- Generation
GNU/Linux - UNIX Desktop, Offering Enterprises and Governments a Compelling
Free and Open Desktop Solution
  
</p>

<p align="justify">
  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.1,
  a major feature upgrade to the third generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes.  KDE 3.1
  ships with a basic desktop, an
  <a href="http://www.kdevelop.org/">integrated development environment</a>
  and seventeen other packages
  (PIM, administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more).  KDE's award-winning tools and
  applications are available in 47 languages.
</p>

<p align="justify">
  Consistent with KDE's rapid but disciplined development pace, the release
  of KDE 3.1 heralds an <a href="#changes">impressive catalog</a> of
  feature enhancements and additions.  As has been typical in recent
  KDE major releases, a great many of the new features provide
  welcome news particularly to private and public enterprises.
</p>

<p align="justify">
  "We are delighted to see the KDE Project placing such a strong emphasis
  on Enterprise Desktops," noted Markus Rex, Vice President of
  Research and Development at SuSE.  "Features like the kiosk mode, the
  improved Mail-encryption and the new remote Desktop operation are key
  components of SuSE's upcoming office and enterprise product lines."
</p>

<p align="justify">
  "People interested in deploying KDE in the enterprise will be pleased
  with the kiosk features that have been added to KDE 3.1," explained Waldo
  Bastian, a lead developer in the "KDE Kiosk" initiative.  "These new features
  make it easy to lock down parts of the KDE desktop, precluding users from
  making unapproved configuration changes or even running unapproved
  applications."
</p>

<p align="justify">
  "Recent additions to KDE's email security and groupware functionality,
  implemented at the initiative of the German federal government, will
  bring KDE a long way towards general use not only on the corporate desktop,
  but also in governments across the European Union and elsewhere," noted
  Matthias Kalle Dalheimer, President of KDE e.V. as well as Klar&auml;lvdalens
  Datakonsult AB, one of the companies contracted by the German government to
  implement the new functionality in close cooperation with the KDE
  community.
</p>

<p align="justify">
  "KDE 3.1 is easily the best overall Open Source desktop ever released,"
  added Andreas Pour, Chairman of the KDE League.  "From enterprise support
  to eye candy to security, this release is a testament to the success
  of Internet collaboration."
</p>

<p align="justify">
  KDE, including all its libraries and its applications, is
  available <em>for free</em> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from the KDE
  <a href="http://download.kde.org/stable/3.1/">http</a> or
  <a href="http://www.kde.org/mirrors/ftp">ftp</a> mirrors, and can
  also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
  or with any of the major GNU/Linux - UNIX systems shipping today.
</p>

<h4>
  <a id="changes">Major Enhancements</a>
</h4>
<p align="justify">
  KDE 3.1 brings a large number of
  <a href="/info/3.1/feature_guide_1.html">impressive</a>
  feature enhancements to
  KDE users.  Chief among these are:
</p>

<ul>
  <li>
    <div align="justify">
    <a href="/info/3.1/feature_guide_2.html#kmail">Enhanced security</a>
    (S/MIME, PGP/MIME and X.509v3 support) for the
    email client (<a href="http://kmail.kde.org/"><em>KMail</em></a>).
    </div>
  </li>
  <li>
    <div align="justify">
    <a href="/info/3.1/feature_guide_2.html#korganizer">Exchange 2000
    compatibility</a> for the calendaring client
    (<a href="http://korganizer.kde.org/"><em>KOrganizer</em></a>).
    </div>
  </li>
  <li>
    <div align="justify">
    Greatly <a href="/info/3.1/feature_guide_2.html">improved LDAP
    integration</a> throughout the KDE PIM framework.
    </div>
  </li>
  <li>
    <div align="justify">
    A <a href="/info/3.1/feature_guide_3.html#kiosk">desktop
    &quot;lockdown&quot; framework</a> which enables system
    administrators to restrict various configuration settings, such as
    bookmarks, and user actions, such as launching applications
    ("<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdecore/README.kiosk?rev=1.23&amp;content-type=text/x-cvsweb-markup"><em>KDE Kiosk</em></a>").
    </div>
  </li>
  <li>
    <div align="justify">
    A
    <a href="/info/3.1/feature_guide_2.html#KRfb-KRdc">desktop
    sharing framework</a> which currently is ideal for remote
    technical assistance by a technical support staff (<em>KRfb, KRdc</em>).
    </div>
  </li>
  <li>
    <div align="justify">
      <a href="/info/3.1/feature_guide_4.html">Tabbed browsing</a> in the
      web browser
      (<a href="http://konqueror.kde.org/"><em>Konqueror</em></a>).
    </div>
  </li>
  <li>
    <div align="justify">
      A <a href="/info/3.1/feature_guide_11.html#quanta">web development</a>
      platform with support for PHP
      (<a href="http://quanta.sourceforge.net/"><em>Quanta Plus</em></a>).
    </div>
  </li>
  <li>
    <div align="justify">
    A new <a href="/info/3.1/feature_guide_4.html#KGET">download manager</a>
    to simplify and enhance managing downloads using the file manager
    (<a href="http://kget.sourceforge.net/"><em>KGET</em></a>).
    </div>
  </li>
  <li>
    <div align="justify">
    An original and creative
    <a href="/info/3.1/feature_guide_5.html#Keramik">new default style</a>
    (<em>Keramik</em>) and a contemporary and impressively original new
    <a href="/info/3.1/feature_guide_5.html#Crystal">default icon style</a>
    (<em>Crystal Icons</em>).
    </div>
  </li>
  <li>
    <div align="justify">
    A new multimedia player plugin
    (based on <a href="http://xine.sourceforge.net/"><em>Xine</em></a>).
    </div>
  </li>
  <li>
    <div align="justify">
    A number of new <a href="/info/3.1/feature_guide_8.html#Games">games</a>,
    including a golf game
    (<a href="http://www.katzbrown.com/kolf/"><em>Kolf</em></a>) and a
    board game inspired by the New Jersey boardwalk
    (<a href="http://www.unixcode.org/atlantik/"><em>Atlantik</em></a>).
    </div>
  </li>
</ul>

<p align="justify">
  Please visit the <a href="/info/3.1/feature_guide_1.html">KDE
  3.1 New Feature Guide</a> for a more detailed description (including
  thumbnails and screenshots) of what is new in this release.
</p>

<h4>
  Installing KDE 3.1 Binary Packages
</h4>
<p align="justify">
  <em>Packaging Policies</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.1 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1/">http</a> or
  <a href="http://www.kde.org/mirrors/ftp">ftp</a> mirrors.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  Please note that the KDE Project makes these packages available from the
  KDE web site as a convenience to KDE users.  The KDE Project provides source
  code, not binary packages; it is the responsibility of the OS vendors to
  package the source code into binary packages.  The KDE Project is not
  responsible for these packages as they are provided by third
  parties - typically, but not always, the distributor of the relevant
  distribution - using tools, compilers, library versions and quality
  assurance procedures over which KDE exercises no control. If you
  cannot find a binary package for your OS, or you are displeased with
  the quality of binary packages available for your system, please read
  the <a href="http://www.kde.org/download/packagepolicy">KDE Binary Package
  Policy</a> and/or contact your OS vendor.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="http://www.kde.org/info/3.1.html">KDE 3.1 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.1
</h4>
<p align="justify">
  <a id="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.1 may be
  <a href="http://download.kde.org/stable/3.1/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.1
  are available from the <a href="/info/3.1.html#binary">KDE
  3.1 Info Page</a>.
</p>

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/people/people.html">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandriva.com/">MandrakeSoft</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a> provide significant support for KDE promotion,
  <a href="http://www.ibm.com/">IBM</a> has donated significant hardware
  to the KDE Project, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr/>

<p align="justify">
  <font size="2">
  <em>Press Release</em>:  Written by <a href="&#x6d;a&#x69;lt&#111;:p&#x6f;ur&#x40;&#x6b;d&#101;.&#0111;&#0114;g">Andreas
  Pour</a> from the <a href="http://www.kdeleague.org/">KDE League</a>, with the
  invaluable assistance of numerous generous volunteers from the KDE Project.
  <br />
  <em>Release Coordinator</em>:  Thanks to
  <a href="mailto:mueller@kde.org">Dirk Mueller</a> for his services as
  release coordinator.
  </font>
</p>

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

Linux is a registered trademark of Linus Torvalds.

UNIX is a registered trademark of The Open Group in the United States and
other countries.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

</p>

<hr />

<table id ="press" border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
Andreas Pour<br />
KDE League, Inc.<br />

[pour@kde.org](mailto:pour@kde.org)<br>
(1) 917 312 3122

  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(33) 4 3250 1445

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
      (49) 2421 502758
  </td>
</tr>

<tr Valign="top">
  <td >
  Latin America (Portuguese, Spanish and English):
  </td>
  <td>
   Roberto Teixeira <br>
    
  [roberto@kde.org](mailto:roberto@kde.org) <br>
  (55) 41 360 2702

  </td>
</tr>
<tr Valign="top">
  <td >
      Southeast Asia (English and Indonesian):
  </td>
  <td>
   Ariya Hidayat <br>
    
  [ariya@kde.org](mailto:ariya@kde.org) <br>
  (62) 815 8703177

  </td>
</tr>
</table>
