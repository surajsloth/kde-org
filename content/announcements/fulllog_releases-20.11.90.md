---
title: Release Service 20.11.90 Full Log Page
type: fulllog
version: 20.11.90
hidden: true
---
{{< details id="akonadi" title="akonadi" href="https://commits.kde.org/akonadi" >}}
+ Check if external library is needed to have atomic support. [Commit.](http://commits.kde.org/akonadi/f150406573c89b55454e97d2de6d8ac45a8bd374) 
+ CMake: Don't use the LZMA imported targets. [Commit.](http://commits.kde.org/akonadi/0586ae9d4f4fc5ef27b11e6c397f049bbd405e19) 
+ Revert "Fix building akonadiprivate_obj with out of system lzma libs on macOS". [Commit.](http://commits.kde.org/akonadi/35c64fc2244821297d512fb5ff15cd342d5150f7) 
+ Install compressionstream private header. [Commit.](http://commits.kde.org/akonadi/6848651cec013cbaa830ae421afe950f91c6526c) 
+ Introduce Config class, disable payload comp. by default. [Commit.](http://commits.kde.org/akonadi/53d1b4c63189146f8998c5abe3473c1741bdd4d3) 
{{< /details >}}
{{< details id="akonadiconsole" title="akonadiconsole" href="https://commits.kde.org/akonadiconsole" >}}
+ Always display decompressed payload in Browser. [Commit.](http://commits.kde.org/akonadiconsole/27e0ff97d3ff533e62b2855c721029f0ee978e08) 
{{< /details >}}
{{< details id="cantor" title="cantor" href="https://commits.kde.org/cantor" >}}
+ Fix unusable Sage session after a syntax error. [Commit.](http://commits.kde.org/cantor/7abf57f88ba69883237d84c82db3f195eb961693) Fixes bug [#429078](https://bugs.kde.org/429078)
{{< /details >}}
{{< details id="dolphin" title="dolphin" href="https://commits.kde.org/dolphin" >}}
+ Allow the openURLJob to run executables. [Commit.](http://commits.kde.org/dolphin/bcdb1957af4de9d7fc480e370c33fd8c666b663f) Fixes bug [#429603](https://bugs.kde.org/429603)
+ Places panel: highlight place only when it is displayed. [Commit.](http://commits.kde.org/dolphin/2cd1c07cad5945a5286914c05987e21b53cc3c7f) Fixes bug [#156678](https://bugs.kde.org/156678)
+ PlacesItemModelTest: consider also Pictures/Music/Videos folders. [Commit.](http://commits.kde.org/dolphin/d5a4835f01a7a269dc94f170860dddbd512bee6a) 
+ PlacesItemModelTest: use helper method to increase indexes. [Commit.](http://commits.kde.org/dolphin/a218b2ce51d22a1d1ea8ab38ab5a2834ed598b55) 
+ Select last visited folder when going up/back. [Commit.](http://commits.kde.org/dolphin/760700819cfdcb767efe0af5a26b385f41c38aae) Fixes bug [#429097](https://bugs.kde.org/429097)
+ Re-allow icons that are not theme icon. [Commit.](http://commits.kde.org/dolphin/b3c37507dc7032feed256b5e974590bd988cbac2) Fixes bug [#429113](https://bugs.kde.org/429113)
+ Allow having the UrlNavigators below the tab bar. [Commit.](http://commits.kde.org/dolphin/50ca5af7e0ec460f9f004a3660efa10bb1dd8cb1) 
+ PlacesPanel: tooltip, use toLocalFile instead of path. [Commit.](http://commits.kde.org/dolphin/f2d2f325b628f8b831cce076c1a5f5dc43ac21ee) 
+ Increment kpartgui version. [Commit.](http://commits.kde.org/dolphin/8999580e2a049996fa48095cc0ed97b886f70eb5) 
+ Context Menu: Only add paste action to folders. [Commit.](http://commits.kde.org/dolphin/5c99790c945c827d45ce8bd9e1b7acd6657e6be3) 
+ Fix navigator alignment for right-to-left localizations. [Commit.](http://commits.kde.org/dolphin/4f645f1b29f0f858b14eed5eca77bf941b52d150) 
{{< /details >}}
{{< details id="elisa" title="elisa" href="https://commits.kde.org/elisa" >}}
+ Remove unneeded margins around list browser view. [Commit.](http://commits.kde.org/elisa/de73a5dc83a6e839e2b14f0483a7b2c341f05745) Fixes bug [#429330](https://bugs.kde.org/429330)
+ Fix navigation to album or artist by not using immediate transition. [Commit.](http://commits.kde.org/elisa/36049d7c074d06d3a64c0002ccc43b4f8c7fac9b) 
+ Prevent crash by stopping play before freeing libvlc resources. [Commit.](http://commits.kde.org/elisa/468eaa7e9d0614884971326cfbc1caacdde904b7) Fixes bug [#424891](https://bugs.kde.org/424891). See bug [#421662](https://bugs.kde.org/421662)
+ Fix display of disc headers in album view. [Commit.](http://commits.kde.org/elisa/7fab8f86febe61f0ebd70cdf0c06148a47ea355b) 
+ Forget the saved position withing a track when switching to another one. [Commit.](http://commits.kde.org/elisa/30f23ab531c7bbee8b8aaec2b8311f0217bd53cc) Fixes bug [#408295](https://bugs.kde.org/408295)
+ Use a role of the model to also choose the correct delegate for metadata. [Commit.](http://commits.kde.org/elisa/e7546560c3c1608a42745ad36b5196d5e2af7864) Fixes bug [#429039](https://bugs.kde.org/429039)
+ Fix modification of tracks with embedded covers. [Commit.](http://commits.kde.org/elisa/7f5e5436491ce58474e866ea7aba7dadeecbf303) Fixes bug [#429041](https://bugs.kde.org/429041)
+ Add support for cover.jpg/png album art files. [Commit.](http://commits.kde.org/elisa/fdee9412b9a5ae4d0c3a490420578c5ee136da09) 
+ Year was somehow always there: update after the fix in database. [Commit.](http://commits.kde.org/elisa/041d509fd1beaab75dbbf67d3939397e1a9d1e7b) 
+ Database sends less track change signals (they were not needed): update. [Commit.](http://commits.kde.org/elisa/60cee1c0466852ee647b8a6a9b565c7aa0d88074) 
+ Only show open in file manager button for local URLs. [Commit.](http://commits.kde.org/elisa/99e8f04942aa0fb1bacf12cf4d8d284d53909011) 
{{< /details >}}
{{< details id="granatier" title="granatier" href="https://commits.kde.org/granatier" >}}
+ Set and make use of RELEASE_SERVICE_VERSION variables. [Commit.](http://commits.kde.org/granatier/dd8a623718b147337be036bbb3bd11b7e343a7c2) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" href="https://commits.kde.org/itinerary" >}}
+ Add a category to the desktop file. [Commit.](http://commits.kde.org/itinerary/35aa9ee65c7e9e9c3074ecfb8c54d7405743abc0) Fixes bug [#429114](https://bugs.kde.org/429114)
+ Fix build with older CMake versions. [Commit.](http://commits.kde.org/itinerary/e899c75a52eb29b053ab8607312874c8588dd15d) 
{{< /details >}}
{{< details id="kaddressbook" title="kaddressbook" href="https://commits.kde.org/kaddressbook" >}}
+ Fix order of cmake imports. [Commit.](http://commits.kde.org/kaddressbook/9013ee3da153b46f78ed2133f3d29b611ebeab18) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" href="https://commits.kde.org/kalarm" >}}
+ Fix build with newer Qt. [Commit.](http://commits.kde.org/kalarm/f61e4b60f2c83d6f4d1430c08c6b05569b62360a) 
+ Change log format. [Commit.](http://commits.kde.org/kalarm/0e94c714439dd2e43db10ee7a810618563f50831) 
+ On completion, calendar migration restores previous Akonadi run state. [Commit.](http://commits.kde.org/kalarm/e3580d48a1287250095b07badfff6d6c67d37eee) 
+ Qt5X11Extras dependency is not optional with X11. [Commit.](http://commits.kde.org/kalarm/3377bd08f0bc4eb9604891b0ff6f1870539ad4ab) 
{{< /details >}}
{{< details id="kamoso" title="kamoso" href="https://commits.kde.org/kamoso" >}}
+ Do not follow the color matrix path that makes it look red. [Commit.](http://commits.kde.org/kamoso/fc5ec930e0b342b3936b57f390bafa60c23edf3f) Fixes bug [#424193](https://bugs.kde.org/424193)
+ Qml warnings. [Commit.](http://commits.kde.org/kamoso/a7fdf5d8746dde0843e2e2e00cafb01a2b583812) 
+ Properly QQC2.StackView.push. [Commit.](http://commits.kde.org/kamoso/441d632d436e6fc538f698f3ddcbb410cb7ab552) 
+ Remove all remaining leftovers from QQC1. [Commit.](http://commits.kde.org/kamoso/16cd16c1527b301513974e8447047bfd3ea300eb) 
+ Fix warning. [Commit.](http://commits.kde.org/kamoso/faf0d5e11f29ce29015cf61470888cfb8b450b3b) 
{{< /details >}}
{{< details id="kde-dev-utils" title="kde-dev-utils" href="https://commits.kde.org/kde-dev-utils" >}}
+ Set window icons for non-desktop-metadata WMs. [Commit.](http://commits.kde.org/kde-dev-utils/cdd4843a1ee00960ec7ff1d41bcf917f72b0f59c) 
+ Set and make use of RELEASE_SERVICE_VERSION variables. [Commit.](http://commits.kde.org/kde-dev-utils/aa277e52ed6cf8f221b74c67d766339632794d3e) 
+ Set Qt::AA_UseHighDpiPixmaps to true. [Commit.](http://commits.kde.org/kde-dev-utils/7bcd6d80894ea38a5d589328d356a100693702a3) 
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" href="https://commits.kde.org/kdeconnect-kde" >}}
+ Add a verification key that's displayed when pairing. [Commit.](http://commits.kde.org/kdeconnect-kde/97bde9ce5e6d1b00a7bc61259ce37496e57a8238) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" href="https://commits.kde.org/kdenlive" >}}
+ Allow creating a mix when we don't have the full default mix duration available at clip ends (minimum of 3 frames now required). [Commit.](http://commits.kde.org/kdenlive/45383774edf44f9d0aa506e1acf6a81978dd112b) 
+ Un-blacklist IIRblur. [Commit.](http://commits.kde.org/kdenlive/ab915ac34a31ecb2814157224a114798393d824d) 
+ Don't blacklist subtitles effect. [Commit.](http://commits.kde.org/kdenlive/e2875cd5374aa62f26395dd756b053a805d9bc91) 
+ Fix subtitles track cannot be hidden when minimized. [Commit.](http://commits.kde.org/kdenlive/f0508967296ec94ca5d3a70f0bce4fafcda0b382) 
+ Various mix and transition fixes. [Commit.](http://commits.kde.org/kdenlive/bab26ba709f0ca406bf1598da49e552ec6df8149) 
+ Update blacklisted effects. [Commit.](http://commits.kde.org/kdenlive/1e89a75cc5e43dda09740a9bc375fd35aec514e5) 
+ Drop semi-working clip name offset in timeline. [Commit.](http://commits.kde.org/kdenlive/6bfa9a439d74bc96323aad3ac972416ccca79e03) 
+ Qml performance fix, patch by Martin Tobias Holmedahl Sandsmark. [Commit.](http://commits.kde.org/kdenlive/14a15e8958e0ed37db2b1d313d5311d8f7eb2ba9) 
+ Subtitles: work on temp files until we save the project so that each change to the subtitles is not instantly saved. [Commit.](http://commits.kde.org/kdenlive/d2409500e164bbae2245085ff85da037c1f843de) 
+ Fix error message about subtitle filter not initialized. [Commit.](http://commits.kde.org/kdenlive/2cc7b347f5f7f8a4201ea3293a31740868c319f5) 
+ Fix typo in subtitles qml header. [Commit.](http://commits.kde.org/kdenlive/2d24a30ac8663deb138fc245e799a43998ea1e67) 
+ Add proper icon for subtitle feature. [Commit.](http://commits.kde.org/kdenlive/ef0809c99c58789243340c07830cc0256ae5b07b) 
+ Don't allow moving a subtitle below 0. [Commit.](http://commits.kde.org/kdenlive/4135769a1712c0e257882068c68407c773eaeace) 
+ By default, move subtitles widget in clip monitor tab. [Commit.](http://commits.kde.org/kdenlive/7ae95a2b184615178e592ff3cd858f8a9250b94d) 
+ Center view when seeking to timeline clip. [Commit.](http://commits.kde.org/kdenlive/25c1628ee3525204a5feb2a9efd69d67c745126e) 
+ After clearing bin filter line, ensure selected item is visible. [Commit.](http://commits.kde.org/kdenlive/8fc743985ca62cfee14c8416a180697d5fecd6cc) 
+ Fix undo import subtitle file, and improve subtitle group operations (only reload subtitle file once). [Commit.](http://commits.kde.org/kdenlive/5e9c863de14e829f441d03ebb99ae496a183845a) 
+ Fix possible crash. Related to #841. [Commit.](http://commits.kde.org/kdenlive/a4c5e0bf85259dae249ac0c17722438e5e9311d2) 
+ Hide subtitle track name on collapse. [Commit.](http://commits.kde.org/kdenlive/39e07a56511147cedcf42576e117734186eda920) 
+ Fix export subtitle not overwriting existing one. [Commit.](http://commits.kde.org/kdenlive/ee3fa9cd32eb9a8dbba984a95008c6cec1f9504b) 
+ Fix group subtitle deletion undo. [Commit.](http://commits.kde.org/kdenlive/c30157070386ce0d684d5f91dcbe10efd575c98c) 
+ Subtitle track: add expand button and track label. [Commit.](http://commits.kde.org/kdenlive/824741bd1237ccb65bca3602d8c94978213ce3ac) 
+ Allow moving subtitle through subtitle widget. [Commit.](http://commits.kde.org/kdenlive/c8440acfbc08f904a9226d9bc2902f204f83c06e) 
+ Add shortcut in/out/delete buttons to subtitle widget. [Commit.](http://commits.kde.org/kdenlive/69ca3d75c087435f85f3eae7038445876f36f836) 
+ Fix cut subtitle. [Commit.](http://commits.kde.org/kdenlive/78f5b9dfa9e27ffc5c7b04873f4c06e2fb543ab6) 
+ Subtitles: when cutting from subtitle widget, split text at cursor position. [Commit.](http://commits.kde.org/kdenlive/ca2c198ed06a005311cddc2d582aae49023ef7f3) 
+ Fix crash on adding first subtitle from subtitle widget. [Commit.](http://commits.kde.org/kdenlive/6e639f6a6c1afd5709efcb0998a3fa93d40e8b8b) 
+ Fix subtitle model used start time as index - caused issues if a subtitle if moved after another one. Add start/end position control in subtitle widget. [Commit.](http://commits.kde.org/kdenlive/46dd76e4c1af85abdd547f7fc5539ddadf891596) 
+ Fixuifiles. [Commit.](http://commits.kde.org/kdenlive/380315e39c24645244edd21d2577cfaa4ba4ce05) 
+ Don't move subtitle on right mouse click. [Commit.](http://commits.kde.org/kdenlive/a6d5dbc01cc13f172d7f65526aa528bd8afca963) 
+ Add libass target for AppImage. [Commit.](http://commits.kde.org/kdenlive/f87002ed48385c6b51a4abe1f0306815a494db02) 
+ Fix crash on mix deletion. [Commit.](http://commits.kde.org/kdenlive/5baea51458d6d6f506386d8f80d349f86056dcbe) 
+ Fix crash on exit caused by subtitlemodel. [Commit.](http://commits.kde.org/kdenlive/7338455a4cceb87bab99bc52e78823e6eec08341) 
+ Select subtitle item when moving between subtitles from the widget. [Commit.](http://commits.kde.org/kdenlive/f74787edb2ade88688b0fee7a64bcb990f855b29) 
+ Fix various issues with subtitles (the filter was duplicated on project opening). [Commit.](http://commits.kde.org/kdenlive/299cd9e33b6096dc8979d504a2be55a1396f422b) 
+ Fix crash loading some ass subtitles, add basic widget for subtitle edit otherwise the feature was not much usable. [Commit.](http://commits.kde.org/kdenlive/87616c7f4c437578cd96d8aa9dd15a55e0c5e85a) 
+ Fix integration of subtitles in timeline (snap, group, cut). [Commit.](http://commits.kde.org/kdenlive/1d4228c88a1c2f217f2693facce49d178024820f) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" href="https://commits.kde.org/kdepim-addons" >}}
+ Fix build with newer Qt. [Commit.](http://commits.kde.org/kdepim-addons/100f473227bade29fc1610641d02cadf9f94a933) 
+ Don't clear line edit when we finish editing. [Commit.](http://commits.kde.org/kdepim-addons/dae485b54462c384c1a0d1226ef5d1bac104b771) See bug [#429546](https://bugs.kde.org/429546)
+ Specify calendar when we open editor. [Commit.](http://commits.kde.org/kdepim-addons/83a83c8683e3158f2d33a07ca23f5ac7e68386ba) See bug [#429546](https://bugs.kde.org/429546)
+ Fix select correct calendar when we open editor. [Commit.](http://commits.kde.org/kdepim-addons/ef8daebeab50ed2d3265c79d020db5203079718b) See bug [#429546](https://bugs.kde.org/429546)
+ Fix CSS from HTML emails messing up the layout of our buttons. [Commit.](http://commits.kde.org/kdepim-addons/9b56bbd93c7821aefbb3ccf30964c44460bce2a2) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" href="https://commits.kde.org/kio-extras" >}}
+ Add PowerPoint slideshow mime types. [Commit.](http://commits.kde.org/kio-extras/e9d11a647b4f3362fa38ed0e2ffcbbe3e09411ba) 
{{< /details >}}
{{< details id="kiten" title="kiten" href="https://commits.kde.org/kiten" >}}
+ Set initial focus on the search bar on startup. [Commit.](http://commits.kde.org/kiten/07ca88d6c311224ea37778b06620dfcaa6d2cf2d) Fixes bug [#427077](https://bugs.kde.org/427077)
+ Radselect: Add trailing "+" to last radicals column header. [Commit.](http://commits.kde.org/kiten/fff03c8049917a433febbc880f2c8bc31e7c0f0b) 
+ Radselect: Actually clear the list of search results in clearSearch(). [Commit.](http://commits.kde.org/kiten/c0fee669a9849c11a4c5f391040c1f29aad4f40d) 
+ Radselect: Properly fetch kanji stroke count from kanjidic. [Commit.](http://commits.kde.org/kiten/c46baf4bae2ef257199be63bb0710634de7f0277) Fixes bug [#385070](https://bugs.kde.org/385070)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" href="https://commits.kde.org/kitinerary" >}}
+ Work around another variant of invalid JSON. [Commit.](http://commits.kde.org/kitinerary/8a4fd1c5229abf4d248654a8b996bddf168694da) 
+ These tests don't need a QGuiApplication. [Commit.](http://commits.kde.org/kitinerary/9442c08d532fd0c8a20ce86d4c5e2e3b508e00e0) 
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" href="https://commits.kde.org/kleopatra" >}}
+ Make it compile with libgpg-error < 1.36. [Commit.](http://commits.kde.org/kleopatra/787568c78455b9584a5e12c3a7a043930e501060) 
{{< /details >}}
{{< details id="kolourpaint" title="kolourpaint" href="https://commits.kde.org/kolourpaint" >}}
+ Don't grab/release keyboard to get key events. Use setFocus() instead. [Commit.](http://commits.kde.org/kolourpaint/676899ef3b5749b7acb7b8b278059e99eca09c8b) Fixes bug [#405991](https://bugs.kde.org/405991)
{{< /details >}}
{{< details id="kompare" title="kompare" href="https://commits.kde.org/kompare" >}}
+ Set Qt::AA_UseHighDpiPixmaps before app instance creation. [Commit.](http://commits.kde.org/kompare/d4414005ff8f67773650353ade1d97378ed06551) 
+ No need to conditionally call kdoctools_install(po), is required dep. [Commit.](http://commits.kde.org/kompare/d5e62679f5522170289fc4cdccb8caef8ece62d1) 
{{< /details >}}
{{< details id="konsole" title="konsole" href="https://commits.kde.org/konsole" >}}
+ SessionController: iterate over a const container. [Commit.](http://commits.kde.org/konsole/b193b8b1242d1570361d2388c475b5a619c19afe) 
+ SessionContoller: fix crash when closing session from Konsole KPart. [Commit.](http://commits.kde.org/konsole/9b9c204c192e8893ca80aa9d93f77b79111ebe5c) Fixes bug [#429538](https://bugs.kde.org/429538)
+ Revert "Uses SessionDisplayConnection to connect sessions and displays.". [Commit.](http://commits.kde.org/konsole/1b03adf74d0ee2267d8dd1a549c31fe3c770556b) 
{{< /details >}}
{{< details id="kontrast" title="kontrast" href="https://commits.kde.org/kontrast" >}}
+ Prevent steahling in main page. [Commit.](http://commits.kde.org/kontrast/60fdae42c098bc7e4680248c1a42a2788d0381b6) 
+ Increase font size contrast. [Commit.](http://commits.kde.org/kontrast/e5a76992f28400e7da65c4f2ba73dda2ddae427b) 
+ Standardize font sizes. [Commit.](http://commits.kde.org/kontrast/df2cbf0e470fbb7b0d4e4cd90e24aa529614a8a9) 
+ Fix some issues on mobile. [Commit.](http://commits.kde.org/kontrast/5963f900a4cd81624653ec2a08d3304ca620ec08) 
{{< /details >}}
{{< details id="konversation" title="konversation" href="https://commits.kde.org/konversation" >}}
+ Make hisc-apps-konversation.svgz an actually qzip'ed file. [Commit.](http://commits.kde.org/konversation/1ddbe1b8cbd3c33e4d956a70305495013dd220ce) 
+ Remove unused decoder.h. [Commit.](http://commits.kde.org/konversation/9595ea293076626255fb0c9f168a5fd3d9813073) 
+ Fix memory leak in ChannelDialog as well. [Commit.](http://commits.kde.org/konversation/dd7d2b3f911c3becf0c834903038a171b538676b) 
+ Fix possible memory leak in ServerDialog. [Commit.](http://commits.kde.org/konversation/78dbdf29faed396b0d09a7669f011ec95951afb8) 
+ Fixed selected treeview index if activated by a notification. [Commit.](http://commits.kde.org/konversation/76c6e8ca0690377021c7ef842d687298a4bc792a) 
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" href="https://commits.kde.org/kosmindoormap" >}}
+ Massively reduce the node matching distance for geometry reassembly. [Commit.](http://commits.kde.org/kosmindoormap/0b542bec1b521fd21aabf8c8fd0fcbdf7ce9bacd) 
+ Fix build with Qt 5.14. [Commit.](http://commits.kde.org/kosmindoormap/306722e868bfd9b153fdbe39133e1ef2a5b2d0b0) 
+ Enable HTTP2 support when available. [Commit.](http://commits.kde.org/kosmindoormap/aea4be171a157eee561ab8b0945dfbb99a958e69) 
+ Set SOVERSION to a fixed value. [Commit.](http://commits.kde.org/kosmindoormap/3b40af2bd4b635c0c16d438fb19de2c031e035ae) 
+ Fix 32bit color parsing on 32bit architectures. [Commit.](http://commits.kde.org/kosmindoormap/9dea296443255d67bd08b4a224330c1c2d1e113a) 
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" href="https://commits.kde.org/kpmcore" >}}
+ Do not reuse QProcess object in externalcommandhelper for different invocations. [Commit.](http://commits.kde.org/kpmcore/5ac18b9d8f0063f754c8ecc54d322dfa921b1c6d) 
+ Fix division by zero. [Commit.](http://commits.kde.org/kpmcore/a0efc8854f07ae5bdacea446f9deefc51fd1ff66) 
+ Clean up include dirs. [Commit.](http://commits.kde.org/kpmcore/6e8424bed659920fa36ed42e3caff152e755bdd1) 
+ Generate libpartitionmanagerexport.h. [Commit.](http://commits.kde.org/kpmcore/5413e696d3e144803fc3ff3b014abfed83a9c57b) 
+ Testhelpers: link kpmcore to get the include directories from the interface. [Commit.](http://commits.kde.org/kpmcore/2bf69932d1e9133d07b2eb48af45fc8ebdec4f87) 
+ CMake config file: check for Qt5Core in the needed version. [Commit.](http://commits.kde.org/kpmcore/32149e6340d82e057b7fe1ddf56b1e7557456304) 
+ Remove no longer existing include dir. [Commit.](http://commits.kde.org/kpmcore/8804711943f33f2a5349010527208a90c39cddc7) 
+ Make plugins compatible to latest KPluginFactory code. [Commit.](http://commits.kde.org/kpmcore/07e5a3ac2858e6d38cc698e0f740e7a693e9f302) 
+ Fix parsing fstab mountpoints when they contain spaces or tabs. [Commit.](http://commits.kde.org/kpmcore/2e91730f7a428cff81443c66613570259180c173) Fixes bug [#428932](https://bugs.kde.org/428932)
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" href="https://commits.kde.org/kpublictransport" >}}
+ Set SOVERSION to a fixed value rather than the release service version. [Commit.](http://commits.kde.org/kpublictransport/e62e1ddd176c37082f9e5f87d328637c431bcfda) 
+ Disable Hafas binary journey queries on big endian systems. [Commit.](http://commits.kde.org/kpublictransport/96ac04eca3444e8bed51daf3297d3e7b92089b93) 
{{< /details >}}
{{< details id="krdc" title="krdc" href="https://commits.kde.org/krdc" >}}
+ Org.kde.krdc.appdata.xml: add <content_rating>. [Commit.](http://commits.kde.org/krdc/de763b60e7881cc8f2f020998d4b6df9b1865931) 
{{< /details >}}
{{< details id="libkdepim" title="libkdepim" href="https://commits.kde.org/libkdepim" >}}
+ Drop unnecessary KGuiAddons dependency. [Commit.](http://commits.kde.org/libkdepim/8aff1b1cec5ee986d5b27998aa7e014e81ab18f4) 
{{< /details >}}
{{< details id="lskat" title="lskat" href="https://commits.kde.org/lskat" >}}
+ Set and make use of RELEASE_SERVICE_VERSION variables. [Commit.](http://commits.kde.org/lskat/b481b2bfd1e9c98ded24cc8e7240004c807b5cda) 
+ Use more nullptr. [Commit.](http://commits.kde.org/lskat/18a081dd9f60c560140b1ad179f88d58f796a5ae) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" href="https://commits.kde.org/messagelib" >}}
+ Fix assert when deleting a folder in kmail. [Commit.](http://commits.kde.org/messagelib/53ed9a5675a5516da6d599bf5868e0a0e73ea15e) 
+ Fix compilation with recent Qt 5.15. [Commit.](http://commits.kde.org/messagelib/150546da7b46404a7446d0954d7612d84afda202) 
+ Fix Bug 429224 - Enabling undo send results in empty return path breaking DKIM. [Commit.](http://commits.kde.org/messagelib/4aa91c641f34e8d4eb97f134aa779601c868411d) Fixes bug [#429224](https://bugs.kde.org/429224)
+ Start to fix print to pdf. [Commit.](http://commits.kde.org/messagelib/9538a30356896ea0c0ee80dfd2f0899f33be5cc0) See bug [#422095](https://bugs.kde.org/422095)
{{< /details >}}
{{< details id="okular" title="okular" href="https://commits.kde.org/okular" >}}
+ Add support for <subtitle> tag in FictionBook. [Commit.](http://commits.kde.org/okular/9f7b45271fff404d2322e530649b3d4f0fd3a227) 
+ Rework how we open urls that have a #. [Commit.](http://commits.kde.org/okular/239827baad0301d578a861be55d5711d82cc2048) Fixes bug [#426976](https://bugs.kde.org/426976)
+ Show annotation tip when selecting the tool in continuous mode. [Commit.](http://commits.kde.org/okular/023976ccfdd79812180c85dad8ee7e3b6ac7aef9) Fixes bug [#426782](https://bugs.kde.org/426782)
+ More fine tuning of the mime of the opened file. [Commit.](http://commits.kde.org/okular/d2ae2c283df3d42e72a964d8891e0915f8f3a94b) 
+ Fix QScroller crash on Qt < 5.14 and certain screen arrangements. [Commit.](http://commits.kde.org/okular/c6a3275151ffa5c9045c8186b64e45e4b2c3ccbf) Fixes bug [#425188](https://bugs.kde.org/425188)
+ Avoid unintentional accelerating flicks by reducing maximum flick time. [Commit.](http://commits.kde.org/okular/8ae95b29c8ce5511cc8368e0fd891ea5a2605eee) 
+ New part/ directory for okularpart sources from /, ui/, conf/. [Commit.](http://commits.kde.org/okular/19d5dd8ec7dbe230911227730416035ae94243ad) 
{{< /details >}}
{{< details id="pim-sieve-editor" title="pim-sieve-editor" href="https://commits.kde.org/pim-sieve-editor" >}}
+ Fix mem leak. [Commit.](http://commits.kde.org/pim-sieve-editor/780913f9ef574fbf764fe0165318d01e61726b07) 
{{< /details >}}
{{< details id="spectacle" title="spectacle" href="https://commits.kde.org/spectacle" >}}
+ Support Wayland rectangular selection HiDpi. [Commit.](http://commits.kde.org/spectacle/4a3cb5140cc58d73b1f0f0083d7f24f9f3a40b2a) Fixes bug [#409762](https://bugs.kde.org/409762). Fixes bug [#420863](https://bugs.kde.org/420863)
+ Fix autoincrement of %d for templates with subdirs. [Commit.](http://commits.kde.org/spectacle/b79ac7743b99cf19b8464c343cd6cb695fdc669f) 
+ Set pixmap device pixel ratio before sending it to kimageannotator. [Commit.](http://commits.kde.org/spectacle/817f9e282e8af5c577ac2cf83ff0b9534d220594) 
{{< /details >}}
