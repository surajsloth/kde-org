---
title: Announcing KDE 3.4
date: "2005-03-16"
description: KDE Project Ships New Major Release Of Leading Open Source Desktop Environment
---

<p>
The <a href="http://www.kde.org">KDE Project</a> ships a new major release of their leading Open Source desktop environment.</p>

<div class="text-center">
<a href="/announcements/announce-3.4/announce-3.4.jpeg">
<img src="/announcements/announce-3.4/announce-3.4.jpeg" class="img-fluid" alt="Splash"/>
</a>
</div>
<br/>
<p>
After more than a half year of development 
the KDE Project is happy to be able to announce a new major release of the 
<a href="http://www.kde.org/awards">award-winning</a> K Desktop Environment. 
Among the many new features that have been incorporated, the improvements 
in accessibility are most remarkable.
</p>

<p>
One of the milestones in this new release will be the advanced KDE 
Text-to-speech framework. It integrates into KDE's PDF-viewer,
<a href="http://kate.kde.org">editor</a>, <a href="http://konqueror.org">webbrowser</a>
and into the new speaker-tool KSayIt. It also allows to read out notifications
from all KDE applications. Especially partially-sighted people and
speech-impaired users will benefit, but it should also prove a fun
desktop experience overall.
</p>

<p>
For people with low vision, several high contrast themes including a 
complete monochrome icon set have been added. Other accessibility 
applications have been improved. KMouseTool which can click the mouse for 
people with, for example, carpal tunnel syndrome or tendinitis; KMouth to 
allow the computer to speak for the speech impaired; and KMagnifier to 
magnify sections of screen for partially-sighted users. Standard 
accessibility features including "Sticky Keys", "Slow Keys" and "Bounce 
Keys" are also available and are now more easily accessed via keyboard 
gestures. All of these features combine to open the world of computing to 
a much wider audience and to a section of the population that is often 
overlooked. The KDE project will continue its close cooperation with the 
accessibility community to reach even more people in the future.
</p>

<p>Another milestone will be the improvements of KDE's personal information
management suite <a href="http://www.kontact.org/">Kontact</a> and of
KDE's instant messenger <a href="http://www.kopete.org/">Kopete</a>.
Kontact has improved usability including a new message composer and start 
screen, and its support for the free software groupware solution
<a href="http://www.kolab.org/">Kolab</a> has 
been updated to Kolab 2.0. This means that KDE has now a complete groupware 
solution including an open-source server interoperable with proprietary MS 
Windows Outlook clients. Other supported groupware servers include 
eGroupware, GroupWise, OpenGroupware.org and SLOX. Kopete features an improved
contact list showing contact photos, improved Kontact integration and supports
AIM, Gadu-Gadu, GroupWise, ICQ, IRC, Jabber, Lotus Sametime, MSN, Yahoo, and
the sending of SMS.</p>

<p>
With KDE being based on an international community there are more than 49 
translations available and even more to be expected for future service packs
of KDE 3.4. This is why KDE serves best the needs of today's world 
wide Linux community.</p>

<p>
KDE 3.4 is available for free under Open Source licenses and boasts 
eighteen packages of optional applications including accessibility, 
development, games, PIM, network, utilities, administration, 
edutainment, multimedia, graphics and more.</p>

<h2>Reactions from the accessibility community</h2>

<p>&quot;With each new release, KDE continues to enhance its support for people
with disabilities&quot;, Janina Sajka, chair of the
<a href="http://accessibility.freestandards.org/">Accessibility Workgroup</a> of
the <a href="http://www.freestandards.org/">Free Standards Group</a>, said.
&quot;This is making KDE more and more attractive to more persons with disabilities.
And, it's also helping KDE meet various social inclusion objectives worldwide,
such as the Sec. 508 requirements of the U.S. Government.&quot;
</p>

<p>
Lars Stetten from the Accessibility User Group
<a href="http://www.linaccess.org/">Linaccess</a> said about the release:
&quot;The new accessibility features in KDE 3.4 
are an important step for the future, to enable disabled 
people to get to know the KDE desktop and to join its community.&quot;
</p>

<h2>Highlights at a glance</h2>

<ul>
<li>Text-to-speech system with support built into Konqueror, Kate, KPDF and the 
	standalone application KSayIt</li>
<li>Support for text to speech synthesis is integrated with the desktop</li>
<li>Completely redesigned, more flexible trash system</li>
<li>Kicker with improved look and feel</li>
<li>KPDF now enables you to select, copy &amp; paste text and images from PDFs, along with many other improvements</li>
<li>Kontact supports now various groupware servers, including eGroupware, 
        GroupWise, Kolab, OpenGroupware.org and SLOX</li>
<li>Kopete supports Novell Groupwise and Lotus Sametime and gets integrated 
        into Kontact</li>
<li>DBUS/HAL support allows to keep dynamic device icons in media:/ and on the desktop in sync with the state of all devices</li>
<li>KHTML has improved standard support and now close to full support for CSS 2.1 and the CSS 3 Selectors module</li>
<li>Better synchronization between 2 PCs</li>
<li>A new high contrast style and a complete monochrome icon set</li>
<li>An icon effect to paint all icons in two chosen colors, converting third party application icons into high contrast monochrome icons</li>
<li>Akregator allows you to read news from your favourite RSS-enabled websites in one application</li>
<li>Juk has now an album cover management via Google Image Search</li>
<li>KMail now stores passwords securely with KWallet</li>
<li>SVG files can now be used as wallpapers</li>
<li>KHTML plug-ins are now configurable, so the user can selectively disable 
        ones that are not used. This does not include Netscape-style plug-ins.
        Netscape plug-in in CPU usage can be manually lowered, and plug-ins are more 
        stable.</li>
<li>more than 6,500 bugs have been fixed</li>
<li>more than 1,700 wishes have been fullfilled</li>
<li>more than 80,000 contributions with several million lines of code and documentation added or changed</li>
</ul>

<p>
For a more detailed list of improvements since the KDE 3.3 release, please refer to the
<a href="http://developer.kde.org/development-versions/kde-3.4-features.html">KDE 3.4 Feature Plan</a>.
</p>

<h2>Getting KDE 3.4</h2>

<p>
Full information on how to download and install KDE 3.4 is available on 
our official website at <a href="http://www.kde.org/info/3.4">http://www.kde.org/info</a>. Being free and 
open source software, it is available for download at no cost. If you 
use a major Linux distribution then precompiled packages may be 
available from your distributions website or from 
<a href="http://download.kde.org/">http://download.kde.org</a>. The source code can also be downloaded from 
there. Both <a href="http://www.arklinux.org/">ArkLinux</a> and <a href="http://www.ubuntulinux.org/wiki/Kubuntu/">Kubuntu</a> have targeted a release including KDE 3.4 right after the 3.4 release. If you prefer to build KDE from source you should consider using 
<a href="http://developer.kde.org/build/konstruct/">Konstruct</a>, a tool that 
automatically downloads, configures and builds KDE 3.4 for you.
</p>

<p>
Many more KDE applications are freely available from <a href="http://www.kde-apps.org/">KDE-Apps.org</a> and different look and feel improvents can be downloaded from <a href="http://www.kde-look.org/">KDE-Look.org</a>.
</p>

<h2>Supporting KDE</h2>

<p>
KDE is an open source project that exists and grows only because of the 
help of many volunteers that donate their time and effort. KDE 
is always looking for new volunteers and contributions, whether its 
help with coding, bug fixing or reporting, writing documentation, 
translations, promotion, money, etc. All contributions are gratefully 
appreciated and eagerly accepted. Please read through the <a href="http://www.kde.org/community/donations/">Supporting 
KDE page</a> for further information. <br />
We look forward to hearing from you soon!
</p>

<h2>About KDE</h2>

<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

Linux is a registered trademark of Linus Torvalds.

UNIX is a registered trademark of The Open Group in the United States and
other countries.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

</p>

<hr/>

<h4>Press Contacts</h4>
<table cellpadding="10" align="center"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#x6d;ai&#x6c;t&#x6f;&#00058;&#105;n&#102;&#x6f;&#45;&#x61;&#x66;r&#105;c&#0097;&#x40;k&#100;&#x65;&#x2e;o&#114;g">&#105;&#110;&#0102;&#00111;-&#x61;&#102;ri&#99;a&#x40;kde.o&#114;g</a><br />

</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="&#109;&#00097;&#105;l&#116;&#111;:info&#x2d;&#00097;&#115;ia&#64;&#x6b;&#100;&#0101;.o&#x72;g">&#x69;&#110;fo&#x2d;as&#105;a&#x40;&#107;&#x64;e&#x2e;&#111;rg</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="mai&#108;to&#x3a;in&#x66;o&#x2d;e&#0117;ro&#112;&#x65;&#00064;kde.o&#x72;g">&#x69;n&#102;&#111;-&#x65;u&#114;&#x6f;&#x70;&#101;&#0064;&#107;&#x64;e.&#x6f;&#x72;g</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="ma&#105;l&#116;&#x6f;:i&#x6e;&#x66;o&#x2d;no&#x72;&#0116;h&#00097;&#x6d;e&#114;i&#099;&#0097;&#64;k&#100;&#101;&#x2e;&#0111;&#114;&#x67;">&#105;&#110;&#102;o-&#00110;or&#116;h&#x61;me&#114;&#105;c&#097;&#x40;&#0107;de.org</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="m&#97;&#105;&#x6c;&#x74;&#111;&#00058;i&#x6e;f&#111;-&#x6f;&#x63;&#x65;&#x61;n&#x69;a&#x40;&#00107;&#x64;&#x65;.or&#0103;">&#105;&#110;fo&#045;oc&#x65;a&#110;&#x69;&#97;&#00064;&#x6b;de&#46;o&#x72;&#103;</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="&#109;a&#105;&#108;&#x74;&#x6f;&#058;&#105;&#110;f&#x6f;-s&#x6f;u&#x74;&#104;&#x61;m&#x65;&#0114;ica&#x40;k&#x64;e.or&#x67;">&#x69;&#110;&#x66;o-&#115;ou&#00116;ha&#x6d;e&#x72;ica&#064;&#x6b;d&#101;&#0046;&#111;&#114;&#x67;</a><br />
</td>

</tr></table>
