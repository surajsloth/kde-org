---
title: KDE Applications 4.8 Offer Faster, More Scalable File Management
date: "2012-01-25"
hidden: true
---

<p>
KDE is happy to announce the release of new versions of many popular applications. From games to education and entertainment, these applications are more powerful. But they are still easy to use as they mature. Below are highlights from some of the updated applications released today.
</p>

<div class="text-center">
	<a href="/announcements/announce-4.8/gwenview2.png">
	<img src="/announcements/announce-4.8/thumbs/gwenview2.png" class="img-fluid" alt="KDE Applications 4.8">
	</a> <br/>
	<em>KDE Applications 4.8</em>
</div>
<br/>

<p>
Enjoy new tricks from a faster and prettier <a href="http://dolphin.kde.org"><strong>Dolphin</strong></a>. The new display engine performs faster when dealing with big directories, slow disks and layout changes. File names no longer get shortened, and icon and text boundaries fit items better.
</p><p>
<div class="text-center">
	<a href="/announcements/announce-4.8/gwenview.png">
	<img src="/announcements/announce-4.8/thumbs/gwenview.png" class="img-fluid" alt="Gwenview 4.8 comes with nicer transitions between images">
	</a> <br/>
	<em>Gwenview 4.8 comes with nicer transitions between images</em>
</div>
<br/>

<a href="http://www.kde.org/applications/graphics/gwenview/"><strong>Gwenview</strong></a> finetunes its usability and image viewing abilities. A handy, translucent navigation area appears when an image is zoomed-in so scrolling is easier. A fade effect is applied in image transitions. <a href="http://okular.kde.org/"><strong>Okular</strong></a> 4.8, KDE's document viewer brings <a href="http://nightcrawlerinshadow.wordpress.com/2011/08/20/advanced-text-selection-in-okular/">advanced text selection features</a>.

<div class="text-center">
	<a href="/announcements/announce-4.8/kmail.png">
	<img src="/announcements/announce-4.8/thumbs/kmail.png" class="img-fluid" alt="KMail has seen many performance and stability improvements">
	</a> <br/>
	<em>KMail has seen many performance and stability improvements</em>
</div>
<br/>

</p><p>
<a href="https://www.kde.org/applications/internet/kmail/"><strong>KMail</strong></a> has seen many bug fixes and performance improvements, following its substantial architectural changes in the previous release. It also has a variety of new frontend features.

</p><p>
<strong>Kate</strong> has many <a href="http://kate-editor.org/2011/12/21/kate-in-kde-4-8/">new features and improvements</a>: a new Search &amp; Replace plugin, useful indicators when lines are changed, and a new editor that can be configured to use document variables (modelines). There are improvements to the heavily reworked code folding, the Vi mode and the Kate/Kwrite handbooks. As for bugs, about 190 issues were resolved after heavy work cleaning up the bug database.

<div class="text-center">
	<a href="/announcements/announce-4.8/kate.png">
	<img src="/announcements/announce-4.8/thumbs/kate.png" class="img-fluid" alt="Kate 4.8 shows changes you've made to files">
	</a> <br/>
	<em>Kate 4.8 shows changes you've made to files</em>
</div>
<br/>

</p><p>
<a href="http://edu.kde.org/cantor/"><strong>Cantor</strong></a> —KDE's frontend to popular mathematics packages—gains support for Qalculate and Scilab.

<a href="https://www.kde.org/applications/development/lokalize/"><strong>Lokalize</strong></a> —the KDE translation tool—has improved support for TBX (TermBase eXchange) glossaries and XLIFF (XML Localisation Interchange File Format) translation catalogs—two important industry standards. It also has a faster UI and 15 bug fixes.

</p><p>
<a href="http://edu.kde.org/marble/"><b>Marble</b></a> —the virtual globe and world atlas—now integrates with Plasma's KRunner. By allowing for coordinate and bookmark searches, Marble can be opened directly from the Plasma search bar. The new Elevation Profile shows the incline of routes, which can be edited interactively. Stargazers can view and track Earth satellites thanks to Marble participation in the European Space Agency (ESA) Summer of Code in Space. During Google Summer of Code, Marble gained support for display of .osm (OpenStreetMap) files in vector format. Owners of the Nokia N9/N950 are the first to receive the new mobile application Marble Touch. Further details can be found in the <a href="http://edu.kde.org/marble/current_1.3">visual changelog </a> at the Marble website.

<div class="youtube">
{{< youtube id="auxbMI4N6is" >}}
</div>

</p>
<h4>Installing KDE Applications</h4>

<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href="http://plasma-active.org">Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.8.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.8.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.8.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.8.0">4.8 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.8.0 may be <a href="/info/4.8.0">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.8.0
  are available from the <a href="/info/4.8.0#binary">4.8.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we recommend to use a recent version of Qt, either 4.7.4 or 4.8.0. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}

<h2>Also Announced Today:</h2>

<h3>
<a href="../plasma">
KDE Plasma Workspaces 4.8 Gain Adaptive Power Management
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/announce-4.8/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.8" />
</a>

Highlights for Plasma Workspaces include <a href="http://philipp.knechtges.com/?p=10">Kwin optimizations</a>, the redesign of power management, and integration with Activities. The first QtQuick-based Plasma widgets have entered the default installation of Plasma Desktop, with more to follow in future releases. Read the complete <a href="../plasma">Plasma Workspaces Announcement</a>.
<br />

</p>

<h3>
<a href="../platform">
KDE Platform 4.8 Enhances Interoperability, Introduces Touch-Friendly Components
</a>
</h3>

<p>
<a href="../platform">
<img src="/announcements/announce-4.8/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.8"/>
</a>

KDE Platform provides the foundation for KDE software. KDE software is more stable than ever before. In addition to stability improvements and bugfixes, Platform 4.8 provides better tools for building fluid and touch-friendly user interfaces, integrates with other systems' password saving mechanisms and lays the base for more powerful interaction with other people using the new KDE Telepathy framework. For full details, read the <a href="../platform">KDE Platform 4.8 release announcement</a>.
<br />

</p>
