---
title: "KDE Plasma 5.20.3, bugfix Release for November"
description: Today KDE releases a bugfix update to KDE Plasma 5
date: 2020-11-10
changelog: plasma-5.20.2-5.20.3-changelog
layout: plasma
---

{{< plasma-5-20-video >}}

Tuesday, 10 November 2020.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.20.3" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in October 2020 with many feature refinements and new modules to complete the desktop experience." "5.20" >}}

This release adds a fortnight's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

+ Plasma Disks: Actually erase devices. [Commit.](http://commits.kde.org/plasma-disks/916f0081b2334759b3d92ba2ca5c042d2df85535) Fixes bug [#428746](https://bugs.kde.org/428746)
+ Plasma Network Management: Do not show absurdedly high speeds on first update. [Commit.](http://commits.kde.org/plasma-nm/08efc47603e59ae702e4cf181eb26374d0af97af) 
+ Fix missing "Switch User" button on lockscreen with systemd 246. [Commit.](http://commits.kde.org/plasma-workspace/aaa7a59a6710a89f21ebd441616df13be5ba8fef) Fixes bug [#427777](https://bugs.kde.org/427777)
