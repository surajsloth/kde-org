---
title: "KDE Plasma 5.17 Beta: Thunderbolt, X11 Night Color and Redesigned Settings"
description: KDE Ships Plasma 5.16.90.
date: 2019-09-19
release: "plasma-5.16.90"
version: "5.16.90"
changelog: plasma-5.16.5-5.16.90-changelog
layout: plasma
---

{{<figure src="/announcements/plasma-5.17/plasma-5.17.png" alt="Plasma 5.17 Beta" class="text-center" width="600px" caption="KDE Plasma 5.17 Beta">}}

Thursday, 19 September 2019.

Today KDE launches the beta release of Plasma 5.17.

We've added a bunch of new features and improvements to KDE's lightweight yet full featured desktop environment.

<a href='https://kde.org/plasma-desktop'>Plasma's updated web page</a> gives more background on why you should use it on your computer.

{{<figure src="/announcements/plasma-5.17/guillermo.png" alt="Guillermo Amaral" caption="Guillermo Amaral" width="600px" >}}

System Settings has gained new features to help you manage your fancy Thunderbolt hardware, plus Night Color is now on X11 and a bunch of pages got redesigned to help you get your configuration done easier. Our notifications continue to improve with a new icon and automatic do-not-disturb mode for presentations. Our Breeze GTK theme now provides a better appearance for the Chromium/Chrome web browsers and applies your color scheme to GTK and GNOME apps. The window manager KWin has received many HiDPI and multi-screen improvements, and now supports fractional scaling on Wayland.

You can test the Plasma 5.17 beta for the next three weeks until the final release in mid-October. Give it a whirl with your favorite distribution!

The Plasma 5.17 series is dedicated to our friend Guillermo Amaral. Guillermo was an enthusiastic KDE developer who rightly self described as 'an incredibly handsome multidisciplinary self-taught engineer'. He brought cheer to anyone he met. He lost his battle with cancer last summer but will be remembered as a friend to all he met.

### Plasma

{{<figure src="/announcements/plasma-5.17/unsplash-pic-of-day.png" alt="<a href='https://unsplash.com/'>Unsplash</a> Picture of the Day" caption="<a href='https://unsplash.com/'>Unsplash</a> Pic of the Day" width="600px" >}}

{{<figure src="/announcements/plasma-5.17/inches-to-cm.png" alt="KRunner now converts fractional units" caption="KRunner now converts fractional units" width="600px" >}}

{{<figure src="/announcements/plasma-5.17/notification-widget.png" alt="Improved Notifications widget and widget editing UX" caption="Improved Notifications widget and widget editing UX" width="600px" >}}

- Do Not Disturb mode is automatically enabled when mirroring screens (e.g. when delivering a presentation)
- The Notifications widget now uses an improved icon instead of displaying the number of unread notifications
- Improved widget positioning UX, particularly for touch
- Improved the Task Manager's middle-click behavior: middle-clicking on an open app's task opens a new instance, while middle-clicking on its thumbnail will close that instance
- Slight RGB hinting is now the default font rendering mode
- Plasma now starts even faster!
- Conversion of fractional units into other units (e.g. 3/16" == 4.76 mm) in KRunner and Kickoff
- Wallpaper slideshows can now have user-chosen ordering rather than always being random
- New <a href='https://unsplash.com/'>Unsplash</a> picture of the day wallpaper source with categories
- Much better support for public WiFi login
- Added the ability to set a maximum volume that's lower than 100%
- Pasting text into a sticky note strips the formatting by default
- Kickoff's recent documents section now works with GNOME/GTK apps
- Fixed Kickoff tab appearance being broken with vertical panels

### System Settings: Thunderbolt, X11 Night Color and Overhauled Interfaces

{{<figure src="/announcements/plasma-5.17/night-color.png" alt="Night Color settings are now available on X11 too" caption="Night Color settings are now available on X11 too" width="600px" >}}

{{<figure src="/announcements/plasma-5.17/thunderbolt.png" alt="Thunderbolt device management" caption="Thunderbolt device management" width="600px" >}}

{{<figure src="/announcements/plasma-5.17/settings-consistency.png" alt="Reorganized Appearance settings, consistent sidebars and headers" caption="Reorganized Appearance settings, consistent sidebars and headers" width="600px" >}}

- New settings panel for managing and configuring Thunderbolt devices
- The Night Color settings are now available on X11 too. It gets a modernized and redesigned user interface, and the feature can be manually invoked in the settings or with a keyboard shortcut.
- Overhauled the user interface for the Displays, Energy, Activities, Boot Splash, Desktop Effects, Screen Locking, Screen Edges, Touch Screen, and Window Behavior settings pages and the SDDM advanced settings tab
- Reorganized and renamed some settings pages in the Appearance section
- Basic system information is now available through System Settings
- Added accessibility feature to move your cursor with the keyboard when using Libinput
- You can now apply a user's font, color scheme, icon theme, and other settings to the SDDM login screen to ensure visual continuity on single-user systems
- New 'sleep for a few hours and then hibernate' feature
- The Colors page now displays the color scheme's titlebar colors
- It is now possible to assign a global keyboard shortcut to turn off the screen
- Standardized appearance for list headers
- The 'Automatically switch all running streams when a new output becomes available' feature now works properly

### Breeze Theme

{{<figure src="/announcements/plasma-5.17/window-borders.png" alt="Window borders are now turned off by default" caption="Window borders are now turned off by default" width="600px" >}}

- The Breeze GTK theme now respects your chosen color scheme
- Active and inactive tabs in Google Chrome and Chromium now look visually distinct
- Window borders are now turned off by default
- Sidebars in settings windows now have a consistent modernized appearance

### System Monitor

{{<figure src="/announcements/plasma-5.17/ksysguard.png" alt="CGroups in System Monitor" caption="CGroups in System Monitor" width="600px" >}}

- System Monitor can now show CGroup details to look at container limits
- Each process can now report its network usage statistics
- It is now possible to see NVidia GPU stats

### Discover

{{<figure src="/announcements/plasma-5.17/discover.png" alt="Discover now has icons on the sidebar" caption="Discover now has icons on the sidebar" width="600px" >}}

- Real progress bars and spinners in various parts of the UI to better communicate progress information
- Better 'No connection' error messages
- Icons in the sidebar and icons for Snap apps

### KWin: Improved Display Management

- Fractional scaling added on Wayland
- It is now once again possible to close windows in the Present Windows effect with a middle-click
- Option to configure whether screen settings apply only for the current screen arrangement or to all screen arrangements
- Many multi-screen and HiDPI improvements
- On Wayland, it is now possible to resize GTK headerbar windows from window edges
- Scrolling with a wheel mouse on Wayland now always scrolls the correct number of lines
- On X11, it is now possible to use the Meta key as a modifier for the window switcher that's bound to Alt+Tab by default
