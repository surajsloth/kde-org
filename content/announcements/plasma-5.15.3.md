---
title: KDE Plasma 5.15.3, Bugfix Release for March
release: "plasma-5.15.3"
version: "5.15.3"
description: KDE Ships Plasma 5.15.3.
date: 2019-03-12
layout: plasma
changelog: plasma-5.15.2-5.15.3-changelog
---

{{<figure src="/announcements/plasma-5.15/plasma-5.15-apps.png" alt="Plasma 5.15" class="text-center" width="600px" caption="KDE Plasma 5.15">}}

Tuesday, 12 March 2019.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.15.3." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in February with many feature refinements and new modules to complete the desktop experience." "5.15" %}}

This release adds a fortnight's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Single-clicks correctly activate modules again when the system is in double-click mode. <a href="https://commits.kde.org/kinfocenter/dbaf5ba7e8335f3a9c4577f137a7b1a23c6de33b">Commit.</a> Fixes bug <a href="https://bugs.kde.org/405373">#405373</a>. Phabricator Code review <a href="https://phabricator.kde.org/D19703">D19703</a>
- [Task Manager] Fix sorting of tasks on last desktop in sort-by-desktop mode. <a href="https://commits.kde.org/plasma-workspace/53290043796c90207c5b7b6aad4a70f030514a97">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D19259">D19259</a>
- [OSD] Fix animation stutter. <a href="https://commits.kde.org/plasma-workspace/9a7de4e02399880a0e649bad32a40ad820fc87eb">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D19566">D19566</a>
