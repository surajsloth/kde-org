---
title: "KDE Plasma 5.20.4, bugfix Release for December"
description: Today KDE releases a bugfix update to KDE Plasma 5
date: 2020-12-01
changelog: plasma-5.20.3-5.20.4-changelog
layout: plasma
draft: false
---

{{< plasma-5-20-video >}}

Tuesday, 1 December 2020.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s" "5.20.4" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in October 2020 with many feature refinements and new modules to complete the desktop experience." "5.20" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

+ Use plasma theme icons in kickoff leave view. [Commit.](http://commits.kde.org/plasma-desktop/2da94367a93d53dd9ede58cc2e38a1b376c963a5) See bug [#429280](https://bugs.kde.org/429280)
+ Weight main categories properly. [Commit.](http://commits.kde.org/kinfocenter/1b666ba9f39d0a1c99820e4350fd90b9bab04f21) Fixes bug [#429153](https://bugs.kde.org/429153)
+ Discover: Display title in application page. [Commit.](http://commits.kde.org/discover/d32292b9808baf57d04f37feb8c90cf5f11bafdd) 
