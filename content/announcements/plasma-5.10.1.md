---
title: KDE Plasma 5.10.1, Bugfix Release for June
release: plasma-5.10.1
version: 5.10.1
description: KDE Ships 5.10.1
date: 2017-06-06
layout: plasma
changelog: plasma-5.10.0-5.10.1-changelog
---

{{%youtube id="VtdTC2Mh070"%}}

{{<figure src="/announcements/plasma-5.10/plasma-5.10.png" alt="KDE Plasma 5.10 " class="text-center" width="600px" caption="KDE Plasma 5.10">}}

Tuesday, 6 June 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.10.1." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.10" "May" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "a week's" %}}

- Fix screen locker incorrect behaviour after pressing enter key with empty password. <a href="https://commits.kde.org/kscreenlocker/23fa33cedfa55cbac83bbdcc514b988d721552dc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380491">#380491</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6091">D6091</a>
- Make QuickShare plasmoid compatible with Purpose 1.1. <a href="https://commits.kde.org/kdeplasma-addons/27efc1c2abc9c56b7161b77fc558fb77c591d4fe">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380883">#380883</a>
- Fixed crash when dropping files on desktop with KDeclarative from KDE Frameworks 5.35. <a href="https://commits.kde.org/plasma-desktop/77f1e675178ac995f7eb74c0410b5028ca1d74de">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380806">#380806</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6088">D6088</a>

