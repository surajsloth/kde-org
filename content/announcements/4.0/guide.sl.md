---
title: Slikovni vodič po KDE 4.0
hidden: true
---

<p>
Namizje in programi v KDE 4.0 si zaslužijo podrobnejši ogled. Sledeče strani vsebujejo
ogled KDE 4.0 in primerov programov. Dodanih je mnogo zaslonskih posnetkov komponent.
Pomnite, da je predstavljen le del tega, kar vam ponuja KDE 4.0
</p>
<div align="left">
<table width="400" border="0" cellspacing="20" cellpadding="8">
<tr>
        <td width="32">
		<a href="../desktop"><img src="/announcements/announce-4.0/images/desktop-32.png" /></a>
        </td>
        <td>
                <a href="../desktop"><strong>Namizje</strong>: Plasma, KRunner, KickOff and KWin</a>
        </td>
</tr>
<tr>
        <td>
		<a href="../applications"><img src="/announcements/announce-4.0/images/applications-32.png" /></a>
        </td>
        <td>
		<a href="../applications"><strong>Programi</strong>: Dolphin, Okular, Gwenview, System Settings and Konsole</a>
        </td>
</tr>
<tr>
        <td>
		<a href="../education"><img src="/announcements/announce-4.0/images/education-32.png" /></a>
        </td>
        <td>
                <a href="../education"><strong>Izobraževalni programi:</strong> Kalzium, Parley, Marble, Blinken, KStars and KTouch</a>
        </td>
</tr>
<tr>
        <td>
		<a href="../games"><img src="/announcements/announce-4.0/images/games-32.png" /></a>
        </td>
        <td>
		<a href="../games"><strong>Igre</strong>: KGoldrunner, KFourInLine, LSkat, KJumpingCube, KSudoku and Konquest</a>
        </td>
</tr>
</table>
</div>
<p>
<br /><br /><br />
<em>Slikovni vodič sta sestavila Sebastian K&uuml;gler in Jos Poortvliet.</em><br />
<em>V slovenščino ga je prevedel Jure Repinc</em>
</p>
