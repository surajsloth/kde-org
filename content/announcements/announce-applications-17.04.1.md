---
title: KDE Ships KDE Applications 17.04.1
description: KDE Ships KDE Applications 17.04.1
date: 2017-05-11
version: 17.04.1
changelog: fulllog_applications-17.04.1
layout: application
---

{{% i18n_var "May 11, 2017. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-17.04.0" %}}

More than 20 recorded bugfixes include improvements to kdepim, dolphin, gwenview, kate, kdenlive, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.32" %}}
