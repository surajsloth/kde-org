---
version: "14.12.3"
title: "KDE Applications 14.12.3 Info Page"
announcement: /announcements/announce-applications-14.12.3
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
