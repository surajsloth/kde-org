---
version: "5.0.0"
title: "KDE Frameworks 5.0 Info Page"
type: info-framework5
bugs:
  - KAuth has no functional backend, Polkit-Qt5 is not yet functional.
---
