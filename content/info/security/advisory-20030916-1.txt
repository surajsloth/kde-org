-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1



KDE Security Advisory: KDM vulnerabilities
Original Release Date: 2003-09-16
URL: http://www.kde.org/info/security/advisory-20030916-1.txt

0. References
        http://cert.uni-stuttgart.de/archive/suse/security/2002/12/msg00101.html
        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2003-0690
        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2003-0692


1. Systems affected:

        All versions of KDM as distributed with KDE up to and including 
        KDE 3.1.3.


2. Overview:

        Two issues have been discovered in KDM:

            a) CAN-2003-0690:
               Privilege escalation with specific PAM modules
            b) CAN-2003-0692:
               Session cookies generated by KDM are potentially insecure

        KDM does not check for successful completion of the pam_setcred()
        call. In case of error conditions in the installed PAM modules, KDM
        might grant local root access to any user with valid login 
        credentials.

        It has been reported that a certain configuration of the MIT pam_krb5
        module can result in a failing pam_setcred() call leaving the 
        session alive and providing root access to a regular user.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2003-0690 to this issue.

        Additionally the session cookie generation algorithm used by KDM was
        considered too weak to supply full 128 bits of entropy. This enables
        non-authorized users to brute-force the session cookie.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2003-0692 to this issue.


3. Impact:

        If KDM is used in combination with the MIT pam_krb5 module and given
        a valid username and password of an existing user, the login attempt
        succeeds and establishes a session with excessive privileges. This
        may enable a local root compromise of the system. 

        It is possible that the same vulnerability exists if KDM is used 
        with other PAM modules. At the date of this advisory we are however
        not aware of any other PAM module being affected by this
        vulnerability.

        The weak cookie generation may allow non-authorized users to guess
        the session cookie by a brute force attack, which allows, assuming
        hostname / IP restrictions can be bypassed, to authorize to the running
        session and gain full access to it. 


4. Solution:

        a) Privilege escalation with specific PAM modules:

        The patch listed in section 5 adds error checking to KDM and
        aborts the login attempt if an error occurs during the 
        pam_setcred() call.

        There is no intermediate workaround known. Users who do not use
        PAM with KDM and users who use PAM with regular Unix crypt/MD5 based
        authentication are not affected. 

        b) Weak cookie generation:

        The patch listed in section 5 adds a new cookie generation algorithm,
        which uses /dev/urandom as non-predictable source of entropy. 

        Users of KDE 2.2.2 are advised to upgrade to KDE 3.1.4. A patch for
        KDE 2.2.2 is available for users who are unable to upgrade to 
        KDE 3.1.

        Users of KDE 3.0.x are advised to upgrade to KDE 3.1.4. A patch for
        KDE 3.0.5b is available for users who are unable to upgrade to 
        KDE 3.1.

        Users of KDE 3.1.x are advised to upgrade to KDE 3.1.4.


5. Patch:

        A patch for KDE 2.2.2 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        4672868343b26e0c0eae91fffeff1f7e  post-2.2.2-kdebase-kdm.patch

        A patch for KDE 3.0.5b is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        fde237203fc7b325c34d2f90a463db3f  post-3.0.5-kdebase-kdm.patch

        A patch for KDE 3.1.3 is available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

        8553c20798b321e333d8c516636f2297  post-3.1.3-kdebase-kdm.patch


6. Time line and credits:

        12/06/2002 Posting on suse-security mailing list describing the 
                   PAM vulnerability.
        08/06/2003 Notification of KDE Security and the KDM maintainer 
                   about the PAM vulnerability by Stephan Kulow.
        08/09/2003 Patches for the PAM vulnerability applied to KDE CVS.
        08/20/2003 George Lebl notifies Oswald Buddenhagen about weak
                   session cookie generation in KDM.
        08/26/2003 Impact analysis and advisory finished.
        09/04/2003 Patches for the weak cookie vulnerability applied to CVS.
        09/16/2003 Public advisory.


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.2-rc1-SuSE (GNU/Linux)

iD8DBQE/Z1fBvsXr+iuy1UoRAi02AJ90TqHxCeqzqGJrN3jS7mRSd9u5xQCg6/Do
LB3tubiwfy8TUy5rL7B8UFY=
=2tbX
-----END PGP SIGNATURE-----
