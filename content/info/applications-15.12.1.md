---
version: "15.12.1"
title: "KDE Applications 15.12.1 Info Page"
announcement: /announcements/announce-applications-15.12-1
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---

