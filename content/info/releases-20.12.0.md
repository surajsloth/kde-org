---
version: "20.12.0"
date: 2020-12-10
title: "20.12.0 Releases Source Info Page"
type: info/release
build_instructions: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
---
