---
version: "15.03.80"
title: "KDE Applications 15.03.80 Info Page"
announcement: /announcements/announce-applications-15.04-beta1
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
